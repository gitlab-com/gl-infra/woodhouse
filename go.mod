module gitlab.com/gitlab-com/gl-infra/woodhouse

go 1.24.0

require (
	github.com/PagerDuty/go-pagerduty v1.8.0
	github.com/alecthomas/kingpin/v2 v2.4.0
	github.com/avast/retry-go v3.0.0+incompatible
	github.com/fatih/semgroup v1.3.0
	github.com/go-kit/log v0.2.1
	github.com/golang/mock v1.6.0
	github.com/gorilla/mux v1.8.1
	github.com/hashicorp/go-retryablehttp v0.7.7
	github.com/iancoleman/strcase v0.3.0
	github.com/oklog/run v1.1.0
	github.com/prometheus/client_golang v1.21.0
	github.com/prometheus/common v0.62.0
	github.com/rs/xid v1.6.0
	github.com/sethvargo/go-retry v0.3.0
	github.com/slack-go/slack v0.16.0
	github.com/statusio/statusio-go v0.0.0-20231122162342-17b4bae9f0d5
	github.com/stretchr/testify v1.10.0
	github.com/xanzy/go-gitlab v0.115.0
	github.com/zekroTJA/timedmap/v2 v2.0.0
	gitlab.com/gitlab-org/labkit v1.22.0
	golang.org/x/sync v0.11.0
	golang.org/x/time v0.10.0
	gopkg.in/yaml.v3 v3.0.1
)

require (
	cloud.google.com/go v0.92.2 // indirect
	cloud.google.com/go/profiler v0.1.0 // indirect
	github.com/alecthomas/units v0.0.0-20211218093645-b94a6e3cc137 // indirect
	github.com/beorn7/perks v1.0.1 // indirect
	github.com/cespare/xxhash/v2 v2.3.0 // indirect
	github.com/davecgh/go-spew v1.1.1 // indirect
	github.com/go-logfmt/logfmt v0.5.1 // indirect
	github.com/golang/groupcache v0.0.0-20210331224755-41bb18bfe9da // indirect
	github.com/golang/protobuf v1.5.3 // indirect
	github.com/google/go-cmp v0.6.0 // indirect
	github.com/google/go-querystring v1.1.0 // indirect
	github.com/google/pprof v0.0.0-20210804190019-f964ff605595 // indirect
	github.com/googleapis/gax-go/v2 v2.0.5 // indirect
	github.com/gorilla/websocket v1.4.2 // indirect
	github.com/hashicorp/go-cleanhttp v0.5.2 // indirect
	github.com/klauspost/compress v1.17.11 // indirect
	github.com/kr/text v0.2.0 // indirect
	github.com/kylelemons/godebug v1.1.0 // indirect
	github.com/munnerz/goautoneg v0.0.0-20191010083416-a7dc8b61c822 // indirect
	github.com/oklog/ulid/v2 v2.0.2 // indirect
	github.com/pmezard/go-difflib v1.0.0 // indirect
	github.com/prometheus/client_model v0.6.1 // indirect
	github.com/prometheus/procfs v0.15.1 // indirect
	github.com/sebest/xff v0.0.0-20210106013422-671bd2870b3a // indirect
	github.com/sirupsen/logrus v1.9.3 // indirect
	github.com/stretchr/objx v0.5.2 // indirect
	github.com/xhit/go-str2duration/v2 v2.1.0 // indirect
	gitlab.com/gitlab-org/go/reopen v1.0.0 // indirect
	go.opencensus.io v0.23.0 // indirect
	golang.org/x/net v0.33.0 // indirect
	golang.org/x/oauth2 v0.24.0 // indirect
	golang.org/x/sys v0.28.0 // indirect
	golang.org/x/text v0.21.0 // indirect
	google.golang.org/api v0.54.0 // indirect
	google.golang.org/appengine v1.6.8 // indirect
	google.golang.org/genproto v0.0.0-20210917145530-b395a37504d4 // indirect
	google.golang.org/grpc v1.40.0 // indirect
	google.golang.org/protobuf v1.36.1 // indirect
)
