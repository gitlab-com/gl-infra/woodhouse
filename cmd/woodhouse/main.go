package main

import (
	"context"
	"fmt"
	"io"
	"net/http"
	"os"
	"os/signal"
	"strings"
	"syscall"
	"time"

	"github.com/alecthomas/kingpin/v2"
	"github.com/go-kit/log"
	"github.com/go-kit/log/level"
	retryablehttp "github.com/hashicorp/go-retryablehttp"
	gogitlab "github.com/xanzy/go-gitlab"
)

var logger log.Logger

func main() {
	cmd := kingpin.MustParse(app.Parse(os.Args[1:]))

	ctx, cancel := context.WithCancel(context.Background())
	defer cancel()

	sigs := make(chan os.Signal, 1)
	signal.Notify(sigs, syscall.SIGINT, syscall.SIGTERM)

	go func() {
		<-sigs
		cancel()
	}()

	logger = createLogger(log.NewSyncWriter(os.Stdout))
	logger = level.NewInjector(logger, level.InfoValue())
	logger = log.With(logger, "ts", log.DefaultTimestampUTC, "caller", log.DefaultCaller)

	switch cmd {
	case serveCmd.FullCommand():
		fatal(serve())
	case slackArchiveIncidentChannelsCmd.FullCommand():
		fatal(archiveIncidentChannels())
	case slackUpdateOncallUsergroupsCmd.FullCommand():
		fatal(updateOncallUsergroups())
	case gitlabNotifyMirroredMrCmd.FullCommand():
		fatal(notifyMirroredMr())
	case gitlabFollowRemotePipelineCmd.FullCommand():
		fatal(followRemotePipeline())
	case gitlabListPrivateEmails.FullCommand():
		fatal(listPrivateEmails())
	case alertmanagerNotifyExpiringSilencesCmd.FullCommand():
		fatal(notifyExpiringSilences())
	case handoverCmd.FullCommand():
		fatal(handoverGenerateIssue())
	case gitalyCtlStorageDrainCmd.FullCommand():
		fatal(gitalyStorageDrainCmd(ctx))
	case versionCmd.FullCommand():
		fatal(printVersion())
	default:
		fatal(fmt.Errorf("cmd %s unrecognized", cmd))
	}
}

func newGitLabClient(token, baseURL string, httpClient *http.Client) *gogitlab.Client {
	client, err := gogitlab.NewClient(
		token, gogitlab.WithBaseURL(baseURL),
		gogitlab.WithoutRetries(), // Use external retries, for better observability into how many failures occur
		gogitlab.WithHTTPClient(httpClient),
	)
	fatal(err)
	return client
}

func newRetryableHTTPClient() *retryablehttp.Client {
	retryClient := retryablehttp.NewClient()
	retryClient.RetryMax = 10
	retryClient.HTTPClient.Timeout = time.Second * 30
	return retryClient
}

func newHTTPClient() *http.Client {
	return &http.Client{Timeout: time.Second * 30}
}

func createLogger(w io.Writer) log.Logger {
	switch *logFormat {
	case "logfmt":
		return log.NewLogfmtLogger(w)
	case "json":
		return log.NewJSONLogger(w)
	default:
		// We can't call fatal without a logger!
		fmt.Printf("unsupported log format: %s\n", *logFormat)
		os.Exit(1)
		return nil
	}
}

func fatal(err error) {
	if err != nil {
		if strings.Contains(err.Error(), "context canceled") {
			level.Info(logger).Log("event", "shutdown", "reason", "terminated", "msg", "context canceled")
			os.Exit(0)
		}

		level.Error(logger).Log("event", "shutdown", "error", err)
		os.Exit(1)
	}
}
