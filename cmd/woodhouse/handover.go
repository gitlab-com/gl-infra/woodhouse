package main

import (
	"context"
	"fmt"
	"time"

	gopagerduty "github.com/PagerDuty/go-pagerduty"
	"github.com/go-kit/log/level"
	"github.com/xanzy/go-gitlab"
	"gitlab.com/gitlab-com/gl-infra/woodhouse/pkg/handover"
	"gitlab.com/gitlab-com/gl-infra/woodhouse/pkg/pagerduty"
)

func handoverGenerateIssue() error {
	ctx, cancel := context.WithTimeout(context.Background(), time.Minute*15)
	defer cancel()

	httpClient := newHTTPClient()

	pdClient := gopagerduty.NewClient(*handoverPagerdutyAPIToken)
	pdClient.HTTPClient = httpClient
	pager := &pagerduty.Pager{HTTPClient: httpClient, PagerdutyClient: pdClient}
	gitlabClient := newGitLabClient(*handoverGitlabAPIToken, *handoverGitlabAPIBaseURL, httpClient)

	now := time.Now().UTC()
	if *handoverNow != "" {
		var err error
		now, err = time.Parse(time.RFC3339, *handoverNow)
		if err != nil {
			fatal(fmt.Errorf("could not parse --now as RFC3339 timestamp: %v", err))
		}
	}

	g := &handover.Generator{
		Pager:        pager,
		GitlabClient: gitlabClient,
		Logger:       logger,

		PagerdutyEscalationPolicyEOC:  *handoverPagerdutyEscalationPolicyEOC,
		PagerdutyEscalationPolicyIMOC: *handoverPagerdutyEscalationPolicyIMOC,
		PagerdutyEscalationPolicyCMOC: *handoverPagerdutyEscalationPolicyCMOC,
		PagerdutyServiceIDs:           *handoverPagerdutyServiceIDs,
		ProductionProjectPath:         *handoverGitlabProductionProjectPath,
		HandoverProjectPath:           *handoverGitlabHandoverProjectPath,
	}
	body, title, err := g.Generate(ctx, now)
	if err != nil {
		return err
	}

	if *handoverDryRun {
		fmt.Println(body)
		return nil
	}

	issue, _, err := gitlabClient.Issues.CreateIssue(
		*handoverGitlabHandoverProjectPath,
		&gitlab.CreateIssueOptions{
			Title:       gitlab.String(title),
			Description: gitlab.String(body),
		},
	)
	if err != nil {
		return err
	}

	level.Warn(logger).Log("action", "handover", "msg", "created handover issue", "url", issue.WebURL)

	return nil
}
