package main

import (
	"context"
	"errors"
	"fmt"
	"net/http"
	"os"
	"time"

	"github.com/prometheus/client_golang/prometheus"
	"github.com/prometheus/client_golang/prometheus/collectors"
	"gitlab.com/gitlab-com/gl-infra/woodhouse/pkg/gitaly/storage"
	"gitlab.com/gitlab-org/labkit/monitoring"
	"golang.org/x/sync/errgroup"
	"gopkg.in/yaml.v3"
)

var (
	gitalyCtlCmd                   = app.Command("gitalyctl", "Manage a Gitaly fleet")
	gitalyCtlStorageCmd            = gitalyCtlCmd.Command("storage", "Manage storages in a Gitaly fleet")
	gitalyCtlStorageDrainCmd       = gitalyCtlStorageCmd.Command("drain", "Drain storage nodes")
	gitalyCtlStorageDrainCmdConfig = gitalyCtlStorageDrainCmd.Flag("config", "Path to config file").
					Default("./configs/gitalyctl-storage-drain-config.yml").Envar("GITALYCTL_STORAGE_DRAIN_CONFIG").String()
	gitalyCtlStorageDrainCmdGitlabAPIToken = gitalyCtlStorageDrainCmd.Flag("gitalyctl-api-token", "Admin API token").
						Envar("GITALYCTL_API_TOKEN").Required().String()
)

const (
	metricSrvAddr    = ":2112"
	readinessSrvAddr = ":2113"
)

func gitalyStorageDrainCmd(ctx context.Context) error {
	data, err := os.ReadFile(*gitalyCtlStorageDrainCmdConfig)
	if err != nil {
		return fmt.Errorf("read config file %q: %w", *gitalyCtlStorageDrainCmdConfig, err)
	}

	cfg, err := loadConfig(data)
	if err != nil {
		return fmt.Errorf("loadConfig: %w", err)
	}

	return storageDrain(ctx, cfg)
}

func storageDrain(ctx context.Context, cfg *storage.DrainCfg) error {
	metricsReg := prometheus.NewRegistry()
	metricsReg.MustRegister(collectors.NewGoCollector())

	ctx, cancel := context.WithCancel(ctx)

	metricsSrv := &http.Server{}
	g, ctx := errgroup.WithContext(ctx)

	httpRouter := http.NewServeMux()
	httpRouter.HandleFunc("/ready", func(w http.ResponseWriter, req *http.Request) {
		fmt.Fprintln(w, "OK")
	})
	readinessSrv := &http.Server{
		Addr:    readinessSrvAddr,
		Handler: httpRouter,
	}

	g.Go(func() error {
		err := monitoring.Start(
			monitoring.WithServer(metricsSrv),
			monitoring.WithListenerAddress(metricSrvAddr),
			monitoring.WithPrometheusRegisterer(metricsReg),
			monitoring.WithPrometheusGatherer(metricsReg),
		)
		if !errors.Is(err, http.ErrServerClosed) {
			return err
		}

		return nil
	})

	g.Go(func() error {
		defer cancel()

		return storage.Drain(ctx, cfg, logger, metricsReg, *gitalyCtlStorageDrainCmdGitlabAPIToken)
	})

	g.Go(func() error {
		err := readinessSrv.ListenAndServe()
		if !errors.Is(err, http.ErrServerClosed) {
			return err
		}

		return nil
	})

	g.Go(func() error {
		<-ctx.Done()

		srvShutdownCtx, cancelTimeout := context.WithTimeout(context.Background(), 5*time.Second)
		defer cancelTimeout()

		_ = metricsSrv.Shutdown(srvShutdownCtx)
		_ = readinessSrv.Shutdown(srvShutdownCtx)

		return nil
	})

	return g.Wait()
}

func loadConfig(data []byte) (*storage.DrainCfg, error) {
	cfg := &storage.DrainCfg{
		Storage: storage.StorageCfg{Concurrency: 2},
		Project: storage.RepositoryCfg{
			Concurrency:      2,
			MoveTimeout:      time.Minute,
			MoveStatusUpdate: 3 * time.Second,
		},
	}

	err := yaml.Unmarshal(data, &cfg)
	if err != nil {
		return &storage.DrainCfg{}, fmt.Errorf("unmarshal config file %q: %w", *gitalyCtlStorageDrainCmdConfig, err)
	}

	return cfg, nil
}
