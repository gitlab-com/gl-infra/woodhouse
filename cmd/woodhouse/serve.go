package main

import (
	"fmt"
	"net"
	"net/http"
	"os"
	"os/signal"
	"regexp"
	"runtime"
	"sync"
	"syscall"
	"time"

	gopagerduty "github.com/PagerDuty/go-pagerduty"
	gostatusio "github.com/statusio/statusio-go"

	"github.com/oklog/run"
	"github.com/prometheus/client_golang/prometheus"
	"github.com/prometheus/client_golang/prometheus/collectors/version"
	"github.com/prometheus/client_golang/prometheus/promhttp"
	"github.com/slack-go/slack"
	"gitlab.com/gitlab-com/gl-infra/woodhouse/pkg/changes"
	"gitlab.com/gitlab-com/gl-infra/woodhouse/pkg/gitlabutil"
	"gitlab.com/gitlab-com/gl-infra/woodhouse/pkg/incidents"
	"gitlab.com/gitlab-com/gl-infra/woodhouse/pkg/instrumentation"
	"gitlab.com/gitlab-com/gl-infra/woodhouse/pkg/pagerduty"
	"gitlab.com/gitlab-com/gl-infra/woodhouse/pkg/slackapps"
	"gitlab.com/gitlab-com/gl-infra/woodhouse/pkg/statusio"
	"gitlab.com/gitlab-com/gl-infra/woodhouse/pkg/web"
	"gitlab.com/gitlab-com/gl-infra/woodhouse/pkg/woodhouse"
)

func serve() error {
	logger.Log("event", "server_start")

	checkIncidentType()

	slackHandler := func(handler http.Handler) http.Handler {
		return slackapps.NewSlackHTTPMiddleware(*slackSigningSecret, handler)
	}

	reqTimeout := time.Duration(*reqMaxDuration) * time.Second
	reqGauge := new(sync.WaitGroup)
	jobRunner := woodhouse.NewAsyncJobRunner(
		logger, prometheus.DefaultRegisterer,
		time.Duration(*asyncJobTimeoutSeconds)*time.Second,
		reqGauge,
	)

	retryClient := newRetryableHTTPClient()
	httpClient := newHTTPClient()
	slackClient := slack.New(*slackBotAccessToken, slack.OptionHTTPClient(httpClient))

	pdClient := gopagerduty.NewClient(*pagerdutyAPIToken)
	pdClient.HTTPClient = httpClient
	pager := &pagerduty.Pager{HTTPClient: httpClient, PagerdutyClient: pdClient}

	gitlabClient := newGitLabClient(*gitlabAPIToken, *gitlabAPIBaseURL, httpClient)
	gitlabGraphQLClient := gitlabutil.NewGraphQLClient(
		retryClient,
		*gitlabAPIToken,
		apiBaseURLToGraphQL(*gitlabAPIBaseURL),
	)

	gitlabOpsClient := newGitLabClient(*gitlabOpsAPIToken, *gitlabOpsAPIBaseURL, httpClient)
	gitlabOpsGraphQLClient := gitlabutil.NewGraphQLClient(
		retryClient,
		*gitlabOpsAPIToken,
		apiBaseURLToGraphQL(*gitlabOpsAPIBaseURL),
	)

	var statusioClient *statusio.Client
	if *statusioApiID != "" && *statusioApiKey != "" && *statusioPageID != "" {
		statusioApi := gostatusio.NewStatusioApi(*statusioApiID, *statusioApiKey)
		statusioApi.HttpClient = httpClient

		statusioClient = statusio.NewClient(statusioApi, *statusioPageID, *statusioPageBaseURL)
	}

	httpRouter := web.NewRouter()
	httpRouter.HandleFunc("/ready", func(w http.ResponseWriter, req *http.Request) {
		fmt.Fprintln(w, "OK")
	}).Methods(http.MethodGet)

	eventsHandler := slackapps.NewEventsHandler(slackClient)
	// Any Slack message with a memo or pencil reaction will be processed
	// by the incident reaction highlight method
	reactionHighlights := incidents.NewReactionHighlights(gitlabClient, *gitlabProductionProjectPath)
	eventsHandler.AddReactionHandler("memo", reactionHighlights)
	eventsHandler.AddReactionHandler("pencil", reactionHighlights)

	slashCmdHandler := slackapps.NewSlashCommandHandler(*globalSlashCommand)
	slashCmdHandler.HandleSlashCommand("echo", &slackapps.EchoSlashCommand{})
	slashCmdHandler.HandleSlashCommand(*incidentSlashCommand, &incidents.SlashCommand{
		SlackClient:                 slackClient,
		JobRunner:                   jobRunner,
		IncidentChannelID:           *incidentSlackChannelID,
		GitlabProductionProjectPath: *gitlabProductionProjectPath,
		GitlabClient:                gitlabClient,
		StatusioClient:              statusioClient,
	})
	slashCmdHandler.HandleSlashCommand(*changeSlashCommand, &changes.SlashCommand{
		SlackClient:                 slackClient,
		JobRunner:                   jobRunner,
		ChangeChannelID:             *changeSlackChannelID,
		GitlabProductionProjectPath: *gitlabProductionProjectPath,
		GitlabGlInfraGroup:          *gitlabGlInfraGroup,
		GitlabClient:                gitlabClient,
		GitlabOpsClient:             gitlabOpsClient,
	})

	httpRouter.Handle("/slack/slash", slackHandler(slashCmdHandler)).Methods(http.MethodPost)
	httpRouter.Handle("/slack/events", slackHandler(eventsHandler)).Methods(http.MethodPost)

	interactivityHandler := slackapps.NewInteractivityHandler()
	interactivityHandler.HandleInteraction(incidents.DeclareModalCallbackID, &incidents.DeclareModalHandler{
		JobRunner:                   jobRunner,
		SlackClient:                 slackClient,
		GitlabClient:                gitlabClient,
		GitlabOpsClient:             gitlabOpsClient,
		GitlabGraphQLClient:         gitlabGraphQLClient,
		GitlabOpsGraphQLClient:      gitlabOpsGraphQLClient,
		GitlabProductionProjectPath: *gitlabProductionProjectPath,
		GitLabProfileSlackFieldID:   *gitlabProfileSlackFieldID,
		GitLabIssueType:             *gitlabIssueType,

		IncidentChannelNamePrefix: *incidentChannelNamePrefix,
		IncidentCallURL:           *incidentCallURL,

		Pager:                         pager,
		PagerdutyIntegrationKeyEOC:    *pagerdutyIntegrationKeyEOC,
		PagerdutyIntegrationKeyIMOC:   *pagerdutyIntegrationKeyIMOC,
		PagerdutyIntegrationKeyCMOC:   *pagerdutyIntegrationKeyCMOC,
		PagerdutyIntegrationKeyGitaly: *pagerdutyIntegrationKeyGitaly,

		PagerdutyEscalationPolicyEOC:  *pagerdutyEscalationPolicyEOC,
		PagerdutyEscalationPolicyIMOC: *pagerdutyEscalationPolicyIMOC,

		InviteMemberIDs: *memberIDsToAddToIncidentChannel,
	})

	interactivityHandler.HandleInteraction(incidents.PostStatuspageCallbackID, &incidents.PostStatuspageModalHandler{
		JobRunner:      jobRunner,
		SlackClient:    slackClient,
		StatusioClient: statusioClient,
	})

	interactivityHandler.HandleInteraction(changes.ModalCallbackID, &changes.ChangeModalHandler{
		JobRunner:                   jobRunner,
		SlackClient:                 slackClient,
		GitlabClient:                gitlabClient,
		GitlabOpsClient:             gitlabOpsClient,
		GitlabProductionProjectPath: *gitlabProductionProjectPath,
		GitLabProfileSlackFieldID:   *gitlabProfileSlackFieldID,
	})

	httpRouter.Handle("/slack/interactivity", slackHandler(interactivityHandler)).Methods(http.MethodPost)

	gitlabIncidentWebhookHandler := &incidents.IssueWebhookHandler{
		GitlabGraphQLClient:    gitlabGraphQLClient,
		GitlabOpsGraphQLClient: gitlabOpsGraphQLClient,
		UpdateIncidentSeverity: updateIncidentSeverity(),
		SlackClient:            slackClient,
		ChannelID:              *incidentSlackChannelID,
	}
	httpRouter.Handle("/gitlab/incident-issues", gitlabutil.NewGitLabWebhookMiddleware(
		*gitlabWebhookToken, gitlabIncidentWebhookHandler,
	)).Methods(http.MethodPost)

	httpHandler := web.NewRequestCountingMiddleware(
		reqGauge, instrumentation.NewInstrumentedHTTPMiddleware(
			logger, prometheus.DefaultRegisterer, []string{"/ready"}, web.NewPanicRecoverMiddleware(
				handleWebPanic, web.NewLimitsMiddleware(
					reqTimeout, *reqBodyMaxSize, *maxReqsPerSecond, httpRouter,
				),
			),
		),
	)

	prometheus.MustRegister(version.NewCollector("woodhouse"))

	httpSocket, err := net.Listen("tcp", *listenAddr)
	fatal(err)
	httpServer := &http.Server{
		Handler:      httpHandler,
		ReadTimeout:  reqTimeout,
		WriteTimeout: reqTimeout,
	}

	metricsSocket, err := net.Listen("tcp", *metricsListenAddr)
	fatal(err)
	metricsServer := &http.Server{
		Handler:      promhttp.Handler(),
		ReadTimeout:  reqTimeout,
		WriteTimeout: reqTimeout,
	}

	signals := make(chan os.Signal, 1)
	signal.Notify(signals, syscall.SIGTERM, syscall.SIGINT)

	var threads run.Group
	threads.Add(func() error {
		return httpServer.Serve(httpSocket)
	}, func(error) {
		httpServer.Close()
	})
	threads.Add(func() error {
		return metricsServer.Serve(metricsSocket)
	}, func(error) {
		metricsServer.Close()
	})
	threads.Add(func() error {
		<-signals
		logger.Log("event", "signal_received")
		time.Sleep(time.Duration(*shutdownSleepSeconds) * time.Second)
		reqGauge.Wait()
		return nil
	}, func(error) {
	})
	return threads.Run()
}

func handleWebPanic(panicked interface{}, req *http.Request) {
	stackBuff := make([]byte, 8*1024)
	stackBytesWritten := runtime.Stack(stackBuff, false)
	stackBuff = stackBuff[0:stackBytesWritten]

	event := req.Context().Value(instrumentation.EventContextKey{}).(*instrumentation.Event)
	event.With("error", panicked, "stack", string(stackBuff), "message", "caught panic")
}

func apiBaseURLToGraphQL(url string) string {
	re := regexp.MustCompile(`\/api\/.*$`)
	if !re.MatchString(url) {
		fatal(fmt.Errorf("Unable generate GraphQL URL from API URL: %s", url))
	}

	baseURL := re.ReplaceAllString(url, "")
	return baseURL + "/api/graphql"
}

func checkIncidentType() {
	if !(*gitlabIssueType == issueTypeIncident || *gitlabIssueType == issueTypeIssue) {
		fatal(fmt.Errorf("Invalid issue type %s", *gitlabIssueType))
	}
}

func updateIncidentSeverity() bool {
	return *gitlabIssueType == issueTypeIncident
}
