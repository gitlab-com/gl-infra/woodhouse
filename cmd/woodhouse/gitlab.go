package main

import (
	"context"
	"fmt"
	"os"
	"sort"
	"time"

	gopagerduty "github.com/PagerDuty/go-pagerduty"
	"gitlab.com/gitlab-com/gl-infra/woodhouse/pkg/gitlabutil"
)

func notifyMirroredMr() error {
	ctx, cancel := context.WithTimeout(context.Background(), time.Minute*2)
	defer cancel()
	retryableClient := newRetryableHTTPClient()
	client := newGitLabClient(*gitlabNotifyMirroredMrToken, *gitlabNotifyMirroredMrBaseURL, retryableClient.StandardClient())
	return gitlabutil.NotifyMergeRequestFromMirrorSource(ctx, client, logger, *gitlabNotifyMirroredMrProjectPath, *gitlabNotifyMirroredMrPipelineStatus, *gitlabNotifyMirroredMrPipelineWaitDuration, *gitlabNotifyMirroredMrDryRun)
}

func followRemotePipeline() error {
	followRemoteTimeout := time.Duration(*gitlabFollowRemotePipelineTimeoutSeconds) * time.Second
	ctx, cancel := context.WithTimeout(context.Background(), followRemoteTimeout)
	defer cancel()
	retryableClient := newRetryableHTTPClient()
	client := newGitLabClient(*gitlabFollowRemotePipelineToken, *gitlabFollowRemotePipelineBaseURL, retryableClient.StandardClient())

	// Assume that this will only be used on mirrors with identical project paths.
	// If this assumption doesn't hold, we can always make it configurable.
	return gitlabutil.FollowRemotePipeline(ctx, logger, client, os.Getenv("CI_PROJECT_PATH"), os.Getenv("CI_COMMIT_SHA"))
}

func listPrivateEmails() error {
	ctx, cancel := context.WithTimeout(context.Background(), time.Minute*15)
	defer cancel()

	httpClient := newRetryableHTTPClient().StandardClient()

	pdClient := gopagerduty.NewClient(*gitlabListPrivateEmailsPagerdutyAPIToken)
	pdClient.HTTPClient = httpClient
	gitlabClient := newGitLabClient(*gitlabListPrivateEmailsGitlabAPIToken, *gitlabListPrivateEmailsGitlabAPIBaseURL, httpClient)

	privateEmails, err := gitlabutil.ListPrivateEmails(ctx, pdClient, gitlabClient, *gitlabListPrivateEmailsPagerdutyEscalationPolicy)
	if err != nil {
		return err
	}

	sort.Strings(privateEmails)

	for _, email := range privateEmails {
		fmt.Println(email)
	}

	return nil
}
