package main

import (
	"context"
	"fmt"
	"io"
	"net"
	"net/http"
	"net/http/httptest"
	"syscall"
	"testing"
	"time"

	"github.com/go-kit/log"
	"github.com/stretchr/testify/assert"
	"github.com/stretchr/testify/require"
	"gitlab.com/gitlab-com/gl-infra/woodhouse/pkg/gitaly/storage"
)

func TestLoadConfig_ConfirmValuesLoadedFromYaml(t *testing.T) {
	cfgYaml := []byte(`
gitlab:
  api_url: https://gdk.test:3000/api/v4

storage:
  concurrency: 1
  move:
    - nfs-file01
    - nfs-file02

project:
  concurrency: 10
  move_timeout: 1h
  move_status_update: 5s
  skip_repositories:
    - 3
    - 55

dry_run: false
`)

	cfg, err := loadConfig(cfgYaml)
	require.NoError(t, err)
	assert.Equal(t, "https://gdk.test:3000/api/v4", cfg.GitLab.ApiURL)
	assert.Equal(t, "nfs-file01", cfg.Storage.Move[0])
	assert.Equal(t, "nfs-file02", cfg.Storage.Move[1])
	assert.Equal(t, int64(10), cfg.Project.Concurrency)
	assert.Equal(t, int64(1), cfg.Storage.Concurrency)
	assert.Equal(t, 3, cfg.Project.SkipRepositories[0])
	assert.Equal(t, 55, cfg.Project.SkipRepositories[1])
	assert.Equal(t, false, cfg.DryRun)
	assert.Equal(t, time.Hour, cfg.Project.MoveTimeout)
	assert.Equal(t, 5*time.Second, cfg.Project.MoveStatusUpdate)
}

func TestLoadConfig_ConfirmDefaultValues(t *testing.T) {
	cfgYaml := []byte(`
gitlab:
  api_url: https://gdk.test:3000/api/v4

storage:
  move:
    - nfs-file01
    - nfs-file02

project:
  

dry_run: false
`)

	cfg, err := loadConfig(cfgYaml)
	require.NoError(t, err)
	assert.Equal(t, int64(2), cfg.Project.Concurrency)
	assert.Equal(t, int64(2), cfg.Storage.Concurrency)
	assert.Equal(t, time.Minute, cfg.Project.MoveTimeout)
	assert.Equal(t, 3*time.Second, cfg.Project.MoveStatusUpdate)
}

func TestLoadConfig_ReturnsErrorWhenUnmarshalFails(t *testing.T) {
	cfgYaml := []byte(`
gitlab:
  api_url: https://gdk.test:3000/api/v4

storage:
  move:
    - nfs-file01
    nfs-file02

project:
  skip_repositories:
    - 3
    - 55

dry_run: false
`)

	_, err := loadConfig(cfgYaml)
	assert.ErrorContains(t, err, "unmarshal config file")
}

func TestErrorStartingMetricServer(t *testing.T) {
	l, err := net.Listen("tcp", metricSrvAddr)
	require.NoError(t, err)
	defer l.Close()

	gitlabAPICount := 0
	gitlabAPI := httptest.NewServer(http.HandlerFunc(func(w http.ResponseWriter, r *http.Request) {
		time.Sleep(5 * time.Second) // We are dealing with concurrent code so we might send a request before metric server starts.
		gitlabAPICount++
	}))
	defer gitlabAPI.Close()

	cfg := &storage.DrainCfg{}
	cfg.GitLab.ApiURL = fmt.Sprintf("http://%s", gitlabAPI.Listener.Addr().String())

	err = storageDrain(context.Background(), cfg)
	require.Zero(t, gitlabAPICount)
	require.ErrorIs(t, err, syscall.EADDRINUSE)

	var opError *net.OpError
	require.ErrorAs(t, err, &opError)
	require.Equal(t, opError.Addr.String(), metricSrvAddr)
}

func TestErrorStartingReadinessServer(t *testing.T) {
	l, err := net.Listen("tcp", readinessSrvAddr)
	require.NoError(t, err)
	defer l.Close()

	cfg := &storage.DrainCfg{}

	err = storageDrain(context.Background(), cfg)
	require.ErrorIs(t, err, syscall.EADDRINUSE)

	var opError *net.OpError
	require.ErrorAs(t, err, &opError)
	require.Equal(t, opError.Addr.String(), readinessSrvAddr)
}

func TestReadinessServer_Returns200(t *testing.T) {
	logger = log.NewJSONLogger(io.Discard) // logger is set by woodhouse main, so we need to set it here otherwise we panic inside storageDrain

	gitlabAPI := httptest.NewServer(http.HandlerFunc(func(w http.ResponseWriter, r *http.Request) {
		var resp *http.Response
		var err error

		for i := 0; i < 100; i++ {
			resp, err = http.Get("http://localhost" + readinessSrvAddr + "/ready")
			if err != nil || resp.StatusCode != http.StatusOK {
				time.Sleep(100 * time.Millisecond)
				continue
			}

			break
		}
		require.NoError(t, err)
		require.Equal(t, resp.StatusCode, http.StatusOK)

		fmt.Fprintln(w, "OK")
	}))
	defer gitlabAPI.Close()

	cfg := &storage.DrainCfg{}
	cfg.GitLab.ApiURL = fmt.Sprintf("http://%s", gitlabAPI.Listener.Addr().String())
	cfg.Storage = storage.StorageCfg{Move: []string{"shard-1"}, Concurrency: 1}

	storageDrain(context.Background(), cfg)
}

func TestExitOnDrain(t *testing.T) {
	ctx, cancel := context.WithTimeout(context.Background(), 5*time.Second)
	defer cancel()

	go func() {
		defer cancel()

		cfg := &storage.DrainCfg{}
		err := storageDrain(context.Background(), cfg)
		require.NoError(t, err)
	}()

	<-ctx.Done()
	require.ErrorIs(t, context.Cause(ctx), context.Canceled, "storageDrain did not exit, and reached timeout.")
}
