# Woodhouse

A monolithic project for GitLab infrastructure team tools. Slack apps, CLI
subcommands, and other assorted things.

[[_TOC_]]

## Functionality

- Slack apps
  - Incident management (`/woodhouse incident`)
  - On-call handover (coming soon)
- Cronjobs (CI-scheduled commands)
  - Archive old incident Slack channels
  - Alertmanager silence issue updates (Helicopter) (coming soon)
  - On-call report generator (coming soon)
- Commands
  - notify-mirrored-mr

## Configuration

<!--
Please keep these tables aligned.
Example, if using vim: https://github.com/junegunn/vim-easy-align
-->

### Global flags

| Flag            | Environment variable   | Description                                                                     | Required | Default   |
| ------          | :--------------------: | :-----------:                                                                   | :----:   | -----:    |
| log-format      | LOG_FORMAT             | Format in which to print structured logs. Valid values: json, logfmt (default). | n        | `logfmt`  |
| pushgateway-url | PUSHGATEWAY_URL        | Prometheus pushgateway to push metrics to from cronjobs.                        | n        |           |

### `serve` subcommand

Start HTTP server, for Slack apps and other HTTP things.

| Flag                              | Environment variable              | Description                                                                                                                                                  | Required | Default                         |
| --------------------------------- | :-------------------------------- | :----------------------------------------------------------------------------------------------------------------------------------------------------------- | :------: | :------------------------       |
| listen-address                    | LISTEN_ADDRESS                    | Address to listen on.                                                                                                                                        | n        | `:8080`                         |
| metrics-listen-address            | METRICS_LISTEN_ADDRESS            | Address to listen on to provide Prometheus metrics.                                                                                                          | n        | `:8081`                         |
| slack-signing-secret              | SLACK_SIGNING_SECRET              | Signing secret used to validate that requests originate from Slack.                                                                                          | y        |                                 |
| slack-bot-access-token            | SLACK_BOT_ACCESS_TOKEN            | Access token for the app's bot user to access the Slack API.                                                                                                 | y        |                                 |
| http-request-body-max-size-bytes  | HTTP_REQUEST_BODY_MAX_SIZE_BYTES  | Maximum HTTP request body size in bytes.                                                                                                                     | n        | `1048576`                       |
| max-requests-per-second-per-ip    | MAX_REQUESTS_PER_SECOND_PER_IP    | Request rate limit per IP.                                                                                                                                   | n        | `10`                            |
| gitlab-api-token                  | GITLAB_API_TOKEN                  | GitLab API token to use for creating issues.                                                                                                                 | y        |                                 |
| gitlab-ops-api-token              | GITLAB_OPS_API_TOKEN              | GitLab API for ops.gitlab.net token to use for creating issues.                                                                                              | y        |                                 |
| gitlab-api-base-url               | GITLAB_API_BASE_URL               | GitLab API to use for creating issues.                                                                                                                       | n        | `https://gitlab.com/api/v4`     |
| gitlab-ops-api-base-url           | GITLAB_OPS_API_BASE_URL           | GitLab API for ops.gitlab.net to use for creating issues.                                                                                                    | n        | `https://ops.gitlab.net/api/v4` |
| gitlab-gl-infra-group             | GITLAB_GL_INFRA_GROUP             | GitLab group for finding all SREs by membership.                                                                                                             | n        | `gitlab-com/gl-infra`           |
| gitlab-ops-check-trigger-token    | GITLAB_OPS_CHECK_TRIGGER_TOKEN    | GitLab Ops trigger token for triggering production checks.                                                                                                   | n        |                                 |
| gitlab-issue-type                 | GITLAB_ISSUE_TYPE                 | Issue types for opening incidents, valid values are "incident" or "issue"                                                                                    | n        | `incident`                      |
| gitlab-production-project-path    | GITLAB_PRODUCTION_PROJECT_PATH    | GitLab project path for incident and change issues.                                                                                                          | y        |                                 |
| incident-channel-name-prefix      | INCIDENT_CHANNEL_NAME_PREFIX      | Name prefix of incident channels. Defaults to project name.                                                                                                  | n        |                                 |
| incident-call-url                 | INCIDENT_CALL_URL                 | URL to a call to use for incidents. This is a secret, unless you like being zoombombed!                                                                      | y        |                                 |
| gitlab-webhook-token              | GITLAB_WEBHOOK_TOKEN              | GitLab webhook secret token, to validate requests.                                                                                                           | y        |                                 |
| global-slash-command              | GLOBAL_SLASH_COMMAND              | Name of the global slash command.                                                                                                                            | n        | `woodhouse`                     |
| incident-slash-command            | INCIDENT_SLASH_COMMAND            | Name of the incident slash command.                                                                                                                          | n        | `incident`                      |
| change-slash-command              | CHANGE_SLASH_COMMAND              | Name of the change slash command.                                                                                                                            | n        | `change`                        |
| async-job-timeout-seconds         | ASYNC_JOB_TIMEOUT_SECONDS         | Timeout limit for async jobs.                                                                                                                                | y        | `120`                           |
| incident-slack-channel            | INCIDENT_SLACK_CHANNEL            | Slack channel in which to post incident updates. Obtain the ID by copying a link to the channel, and taking the last part of the path.                       | y        |                                 |
| change-slack-channel              | CHANGE_SLACK_CHANNEL              | Slack channel in which to post newly created change management issues. Obtain the ID by copying a link to the channel, and taking the last part of the path. | y        |                                 |
| gitlab-profile-slack-field-id     | GITLAB_PROFILE_SLACK_FIELD_ID     | Slack custom field ID for users' GitLab profile links.                                                                                                       | n        | `Xf494JUKE0`                    |
| pagerduty-integration-key-eoc     | PAGERDUTY_INTEGRATION_KEY_EOC     | Pagerduty events V2 API integration key for the Production service (pages the EOC).                                                                          | n        |                                 |
| pagerduty-integration-key-imoc    | PAGERDUTY_INTEGRATION_KEY_IMOC    | Pagerduty events V2 API integration key for the IMOC service.                                                                                                | n        |                                 |
| pagerduty-integration-key-cmoc    | PAGERDUTY_INTEGRATION_KEY_CMOC    | Pagerduty events V2 API integration key for the CMOC service.                                                                                                | n        |                                 |
| pagerduty-integration-key-gitaly  | PAGERDUTY_INTEGRATION_KEY_GITALY  | Pagerduty events V2 API integration key for the GITALY service.                                                                                                | n        |                                 |
| pagerduty-api-token               | PAGERDUTY_API_TOKEN               | Pagerduty API token.                                                                                                                                         | y        |                                 |
| pagerduty-escalation-policy-eoc   | PAGERDUTY_ESCALATION_POLICY_EOC   | Escalation policy ID for the EOC.                                                                                                                            | n        |                                 |
| pagerduty-escalation-policy-imoc  | PAGERDUTY_ESCALATION_POLICY_IMOC  | Escalation policy ID for the IMOC.                                                                                                                           | n        |                                 |
| pagerduty-escalation-policy-cmoc  | PAGERDUTY_ESCALATION_POLICY_CMOC  | Escalation policy ID for the CMOC.                                                                                                                           | n        |                                 |
| http-request-max-duration-seconds | HTTP_REQUEST_MAX_DURATION_SECONDS | Maximum HTTP request duration in seconds.                                                                                                                    | n        | `30`                            |
| shutdown-sleep-seconds            | SHUTDOWN_SLEEP_SECONDS            | Time to sleep on receipt of a signal before waiting for all requests to complete, then shutting down.                                                        | n        | `0`                             |
| statusio-api-id                   | STATUSIO_API_ID                   | Status.io API ID used for communicating with the status.io API for statuspage management                                                                     | n        |                                 |
| statusio-api-key                  | STATUSIO_API_KEY                  | Status.io API key used for communicating with the status.io API for statuspage management                                                                    | n        |                                 |
| statusio-page-id                  | STATUSIO_PAGE_ID                  | Status.io page ID defines what statuspage to act on                                                                                                          | n        |                                 |
| statusio-page-base-url            | STATUSIO_PAGE_BASE_URL            | Status.io page base URL                                                                                                                                      | n        |                                 |

### `slack archive-incident-channels` subcommand

Archive old incident channels.

| Flag                           | Environment variable           | Description                                                  | Required | Default                     |
| ------------------------------ | :----------------------------- | :----------------------------------------------------------- | :------: | :------------------------   |
| access-token                   | SLACK_BOT_ACCESS_TOKEN         | Access token for the app's bot user to access the Slack API. | y        |                             |
| gitlab-api-token               | GITLAB_API_TOKEN               | GitLab API token to use for fetching issues.                 | y        |                             |
| gitlab-api-base-url            | GITLAB_API_BASE_URL            | GitLab API to use for fetching issues.                       | y        | `https://gitlab.com/api/v4` |
| gitlab-production-project-path | GITLAB_PRODUCTION_PROJECT_PATH | GitLab project path for incident and change issues.          | y        |                             |
| name-prefix                    | INCIDENT_CHANNEL_NAME_PREFIX   | Name prefix for incident slack channels.                     | y        |                             |
| dry-run                        | DRY_RUN                        | Don't actually archive any channels.                         | n        | `false`                     |

### `gitlab notify-mirrored-mr` subcommand

When run inside a mirror's CI job, writes a note on any corresponding merge requests on the mirror source.

| Flag                | Environment variable          | Description                                                                | Required | Default                     |
| ------------------- | :---------------------------- | :------------------------------------------------------------------------- | :------: | :------------------------   |
| gitlab-api-token    | GITLAB_API_TOKEN              | GitLab API token to use for creating notes.                                | y        |                             |
| gitlab-api-base-url | GITLAB_API_BASE_URL           | GitLab API to use for creating issues.                                     | n        | `https://gitlab.com/api/v4` |
| project-path        | GITLAB_MIRROR_PATH            | Project path of mirror source, if different from the mirror target's path. | n        |                             |
| pipeline-status     | GITLAB_MIRROR_PIPELINE_STATUS | Status of the mirror's pipeline.                                           | n        | `starting`                  |
| dry-run             | DRY_RUN                       | Don't actually make any comments.                                          | n        | `false`                     |

### `gitlab follow-remote-pipeline` subcommand

When run inside a GitLab CI job, blocks on pipelines for the same commit running
on a mirror target/source.

| Flag                | Environment variable | Description                                      | Required | Default                     |
| ------------------- | :------------------- | :----------------------------------------------- | :------: | :------------------------   |
| gitlab-api-token    | GITLAB_API_TOKEN     | GitLab API token to use for following pipelines. | y        |                             |
| gitlab-api-base-url | GITLAB_API_BASE_URL  | GitLab API to use for following pipelines.       | n        | `https://gitlab.com/api/v4` |

### `alertmanager notify-expiring-silences` subcommand

Find silences that are expiring soon and notify the on-call via slack.

| Flag                           | Environment variable           | Description                                                  | Required | Default                     |
| ------------------------------ | :----------------------------- | :----------------------------------------------------------- | :------: | :------------------------   |
| access-token                   | SLACK_BOT_ACCESS_TOKEN         | Access token for the app's bot user to access the Slack API. | y        |                             |
| slack-channel                  | ALERTMANATER_SLACK_CHANNEL     | Slack channel in which to post expiring alert warnings.      | y        |                             |
| alertmanager-base-url          | ALERTMANAGER_BASE_URL          | Alertmanager API base URL.                                   | y        | `http://localhost:9093`     |
| alertmanager-external-base-url | ALERTMANAGER_EXTERNAL_BASE_URL | Alertmanager base URL, used to link to silences.             | n        | `https://alerts.gitlab.net` |
| silence-end-cutoff             | SILENCE_END_CUTOFF             | Cutoff for when silences expire.                             | y        | `8h`                        |
| dry-run                        | DRY_RUN                        | Don't actually post to slack.                                | n        |                             |

### `gitalyctl storage drain` subcommand

This subcommand is used to move repositories from configured Gitaly VMs to different Gitaly storages so that the drained storages can be decommissioned. More details on how to use it can be found in our [runbooks](https://gitlab.com/gitlab-com/runbooks/-/blob/master/docs/gitaly/gitalyctl.md?ref_type=heads).

## Developing

Build woodhouse:

```sh
make
```

You can use [ngrok](https://ngrok.com/) to expose a locally-running web server
on the public internet, e.g. to test Slack integration in a test workspace. This
won't always be necessary: see below for instructions on how to make a staging
deploy from a branch.

```sh
ngrok http --region <region> 8080
```

See the available configuration in the tables above, or run `./woodhouse --help`,
or look at the flags in [`config.go`](cmd/woodhouse/config.go).

Follow the "Installing Slack App" instructions below to install a Slack app on a
Slack workspace you control. When configuring the callback URLs, use the ngrok
URL given to you by `ngrok http` as a base.

We might eventually create a staging deployment to test out branch builds on the
GitLab slack, but at the moment this is the only way to test changes to
Woodhouse.

You can use [`direnv`](https://direnv.net/) to automate the sourcing of a
`.envrc` file if you want.

Start woodhouse:

```sh
./woodhouse serve
```

You can now run Slack slash commands like `/woodhouse incident declare`, or
`/woodhouse echo`.

### Testing integrations

Woodhouse is an integration-heavy codebase, and as such it can be difficult to
gain confidence in changes to these integrations from unit testing alone. We
rely on manual testing of some integrations using Woodhouse's shared staging
environment.

1. Check in the `#woodhouse-staging` Slack channel to see if anyone else has
   borrowed staging without returning it.
1. If you suspect that someone forgot to announce they were done with staging,
   message them.
1. Announce that you are borrowing staging,
1. Create your branch and merge request.
1. Trigger the manual docker_image step on the gitlab.com CI pipeline for your
   MR. Wait for the Docker image to be built and pushed for your commit SHA.
1. If your MR introduces any new mandatory configuration parameters, add them to
   staging's tanka configuration, or if they are secret, to the Kubernetes
   Secret in staging. Instructions:
   <https://gitlab.com/gitlab-com/gl-infra/k8s-workloads/tanka-deployments/-/tree/master/environments/woodhouse>.
1. Trigger the manual deploy_staging step on the ops.gitlab.net CI pipeline for
   your MR. This will trigger a deploy to Woodhouse's staging environment in
   <https://ops.gitlab.net/gitlab-com/gl-infra/k8s-workloads/tanka-deployments>.
1. Try out your new feature.
1. Announce that you're done with staging in the Slack channel.
1. When your merge request is merged, this will trigger an automatic deployment
   to production.

## Operating

### Deployment and configuration

We (GitLab) deploy Woodhouse to our Kubernetes clusters using
[Tanka](https://tanka.dev). Our deployment config is found
[here](https://gitlab.com/gitlab-com/gl-infra/k8s-workloads/tanka-deployments/-/tree/master/environments/woodhouse).

The non-secret config is found in [`environments.jsonnet`](https://gitlab.com/gitlab-com/gl-infra/k8s-workloads/tanka-deployments/-/blob/master/environments/woodhouse/environments.libsonnet), while the secret config is provisioned [from Vault](https://vault.gitlab.net/ui/vault/secrets/k8s/kv/list/ops-gitlab-gke/woodhouse/) in [`secrets.libsonnet`](https://gitlab.com/gitlab-com/gl-infra/k8s-workloads/tanka-deployments/-/blob/master/environments/woodhouse/secrets.libsonnet).

The GitLab API tokens are Group Access Tokens managed and rotated automatically by [`infra-mgmt`](https://gitlab.com/gitlab-com/gl-infra/infra-mgmt):

- [`gitlab.com`](https://gitlab.com/gitlab-com/gl-infra/infra-mgmt/-/blob/19889fc8754861d7c45d265db55cb7ade8917fbb/environments/gitlab-com/group_gl-infra.tf#L91-108)
- [`ops.gitlab.net`](https://gitlab.com/gitlab-com/gl-infra/infra-mgmt/-/blob/19889fc8754861d7c45d265db55cb7ade8917fbb/environments/ops-gitlab-net/group_gl-infra.tf#L91-108)

We auto-deploy master to our `ops` cluster, and can make staging deploys from
branches (see above).

See [`.gitlab-ci.yml`](.gitlab-ci.yml) for the source of truth on how Tanka
deploys are triggered from this repository.

#### Cronjobs

Previously, archiving a slack channel was initiated via a scheduled pipeline in [the gitlab-com/gl-infra/woodhouse ops mirror](https://ops.gitlab.net/gitlab-com/gl-infra/woodhouse/-/pipeline_schedules), however, this functionality has been migrated to [RunwayJob](https://docs.runway.gitlab.com/workloads/jobs/).

Jobs [migrated to Runway](https://gitlab.com/gitlab-com/gl-infra/production-engineering/-/issues/25758):

- `woodhouse-channels` (to archive incident Slack channels)
- `woodhouse-handover` (to create GitLab handover issues)

Tanka-managed Kubernetes deployments jobs:

- `woodhouse-notify-expiring-silences`

The Kubernetes jobs have not been migrated to `RunwayJob` simply because Runway has no access to internal endpoints, which is required by the expiring silences job as it needs to talk to Alertmanager.

Here's a few commands that can help debugging if this functionality stopped working:

```sh
$ glsh kube use-cluster ops
$ kubectl get cronjobs -n woodhouse
NAME                                  SCHEDULE          SUSPEND   ACTIVE   LAST SCHEDULE   AGE
woodhouse-notify-expiring-silences    1 7,15,23 * * *   False     0        5h18m           304d

$ kubectl get jobs -n woodhouse
# if you see no jobs there  with the prefix `woodhouse-notify-expiring-silences`, attempt creating one.

$ kubectl create job --from=cronjob/woodhouse-notify-expiring-silences woodhouse-notify-expiring-silences-test-0 -n woodhouse
job.batch/woodhouse-notify-expiring-silences-test-0 created

$ kubectl logs jobs/woodhouse-notify-expiring-silences-test-0 -n woodhouse
```

RunwayJobs execution and the logs can be viewed in the Cloud Run GCP console in projects `gitlab-runway-production` and `gitlab-runway-staging`. Navigate to a required job and check the history of executions with attached logs. Additionally, failed executions notifications from the runs will be posted to [#f_runway_alerts](https://gitlab.enterprise.slack.com/archives/C07MYSYQZ0U) channel.

You can also view the [dashboard](https://dashboards.gitlab.net/d/runway-job/runway3a-runway-job-metrics?orgId=1) for Runway Jobs.

### Installing Slack App

1. Visit <https://api.slack.com/apps>.
1. Click "Create an App"
1. Select "From an app manifest"
1. Select your development workspace
1. Copy/paste this manifest and update accordingly (request URL + any other changes you're making):

    ```yaml
    display_information:
      name: Woodhouse Dev
    settings:
      event_subscriptions:
        request_url: <your request URL>/slack/events
        bot_events:
          - reaction_added
      org_deploy_enabled: false
      socket_mode_enabled: false
      token_rotation_enabled: false
      interactivity:
        is_enabled: true
        request_url: <your request URL>/slack/interactivity
    features:
      app_home:
        messages_tab_enabled: true
        messages_tab_read_only_enabled: false
      bot_user:
        display_name: woodhouse
        always_online: true
      slash_commands:
        - command: /woodhouse
          url: <your request URL>/slack/slash
          usage_hint: help|incident|change
          description: Ask Woodhouse to do things
        - command: /incident
          url: <your request URL>/slack/slash
          usage_hint: declare|spin-the-wheel|post-statuspage|help
          description: Incident management commands
        - command: /change
          url: <your request URL>/slack/slash
          usage_hint: declare|help
          description: Create a change issue
    oauth_config:
      scopes:
        bot:
          - chat:write
          - chat:write.customize
          - channels:manage
          - channels:read
          - users.profile:read
          - users:read
          - commands
          - reactions:read
          - channels:history
    ```

1. Click "Install to Workspace" and then "Allow"
1. In the "App Credentials" section:
    - Grab the _Signing Secret_ for this app -- this value goes in `--slack-signing-secret`
1. On the left-hand sidebar menu, in the "Features" section click on "OAuth & Permissions"
    - Grab the _Bot User OAuth Token_ -- this value goes in `--slack-bot-access-token`
1. If all goes well, you should be able to do `/woodhouse help` and see a response from _Woodhouse Dev_

#### A note about slash commands

Slash commands are all globally namespaced per Slack workspace, and Slack will
route slash commands to the most recently installed app that declares that
command.

Woodhouse supports 2 forms of Slack slash commands: namespaced (e.g., `/woodhouse incident declare`) and
non-namespaced (e.g., `/incident declare`).

The `/woodhouse` global command is configurable. It has to be, otherwise we
wouldn't be able to install a staging deployment of Woodhouse to the same slack
workspace as production.

All slash commands (except `echo`) have configurable names too, which affect
both the non-namespaced (`/name`) and namespaced (`/woodhouse name`) versions.
These names are configurable for 2 reasons:

- Support staging and production versions of non-namespaced commands in the same
  Slack workspace.
- Allow testing of command replacements without immediately replacing the
  non-namespaced command in Slack.

### GitLab webhook integration

Woodhouse can receive webhooks from GitLab. Currently, Woodhouse can receive
webhooks on its `/gitlab/incident-issue` endpoint as part of its incident
management functionality, and there may be more use cases for this in the
future.

1. Configure a webhook to Woodhouse's domain and endpoint in a GitLab project's
   hooks section, e.g.
   <https://gitlab.com/gitlab-com/gl-infra/woodhouse-integration-test/hooks>. Only
   check the boxes relevant to events Woodhouse can handle - currently issue
   events and confidential issue events.
1. Generate and configure a webhook secret, e.g. `pwgen -s 40 1`.
1. Add this webhook secret to Woodhouse's secret
   [config](cmd/woodhouse/config.go).

### Metrics

We collect metrics using Prometheus. All of them:
[here](https://thanos-query.ops.gitlab.net/graph?g0.range_input=1h&g0.max_source_resolution=0s&g0.expr=%7Bjob%3D%22woodhouse%22%7D&g0.tab=1).

### Logs / Events

Woodhouse is sparse with logging, and in general emits one structured message
per event, where an event is a HTTP request or async job.

You can view our production Woodhouse logs in
[stackdriver](https://console.cloud.google.com/logs/viewer?project=gitlab-ops&minLogLevel=0&expandAll=false&timestamp=2020-10-14T08:29:26.315000000Z&customFacets=&limitCustomFacetWidth=true&dateRangeEnd=2020-10-14T08:29:26.566Z&interval=P1D&resource=k8s_container%2Fcluster_name%2Fops-gitlab-gke%2Fnamespace_name%2Fwoodhouse%2Fcontainer_name%2Fwoodhouse&dateRangeStart=2020-10-13T08:29:26.566Z&scrollTimestamp=2020-10-13T15:24:32.904889638Z).

## Maintainers

- [Igor W](https://gitlab.com/igorwwwwwwwwwwwwwwwwwwww)
- [Jarv](https://gitlab.com/jarv)

## Troubleshooting

Is the slack command not working? Here's some 101 checks that you can perform.

1. Does `/woodhouse echo` work?
    - If yes, and other commands aren't working, then it's likely application logic issue.
    - If no, then it's likely infrastructure/hosting issue.
2. Is woodhouse reachable?

   ```sh
   curl https://woodhouse.ops.gitlab.net
   curl https://woodhouse.gstg.gitlab.net
   ```

3. Is slack having issues? Check the [Slack Status page](https://status.slack.com/).
4. Check [the logs](#logs-events).

## Why put unrelated things into the same codebase / executable?

The main benefits are:

- One language (Go). This benefit is a double-edged sword, but:
  - Go is a language many on the GitLab infrastructure team have at least some
    familiarity with.
  - Code can be shared between the different components easily.
  - We're unlikely to make frequent changes to many of these components, and if
    some were written in different languages, the mean time between working in
    that language might be quite long, making it difficult to quickly jump in
    and make a change.
- New components don't have to reinvent the "project core" wheel: event logging,
  metrics, HTTP middleware (e.g. Slack authorization). If the codebase already
  contains a Slack app, or a CLI subcommand, slotting in new functionality
  should be simple.
- No need to set up build, test, and deployment pipelines for many small tools.
- One place to keep dependencies up to date.

The main drawbacks are:

- When making a change to a component, you and/or a CI job must build/index
  code, and run tests, for code you're not using.
- The built artifacts are larger than they would be for any one component
  extracted into its own codebase.

These drawbacks are most noticeable at large scale, which this project is
unlikely to reach.

Most of the benefits could be realized in a monorepo codebase that builds
several binaries, but the build and deployment pipelines are maximally simple
with the monolithic binary approach.


