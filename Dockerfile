# Build stage
FROM golang:1.24 as build

WORKDIR /app
COPY . /app
RUN make

# Final image
FROM alpine:3.21

COPY --from=build /app/woodhouse /bin/woodhouse
RUN apk add --no-cache git
CMD ["/bin/woodhouse"]
