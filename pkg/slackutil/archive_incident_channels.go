package slackutil

import (
	"context"
	"fmt"
	"strconv"
	"strings"
	"time"

	"github.com/go-kit/log"
	"github.com/sethvargo/go-retry"
	"github.com/slack-go/slack"
	"github.com/xanzy/go-gitlab"
)

//nolint:staticcheck // SA1019 https://github.com/slack-go/slack/issues/876
func ArchiveIncidentChannels(
	ctx context.Context, logger log.Logger, client *slack.Client,
	gitlabClient *gitlab.Client,
	gitlabProductionProjectPath string,
	namePrefix string, dryRun bool,
) error {
	self, err := client.AuthTestContext(ctx)
	if err != nil {
		return fmt.Errorf("error getting own identity: %s", err)
	}

	var channels []slack.Channel
	var nextCursor string

	logger.Log("msg", "finding channels", "username", self.User, "user_id", self.UserID)
	listParams := &slack.GetConversationsParameters{ExcludeArchived: true}
	for {
		err = retry.Exponential(ctx, 10*time.Second, func(ctx context.Context) error {
			var err error
			channels, nextCursor, err = client.GetConversationsContext(ctx, listParams)
			if _, ok := err.(*slack.RateLimitedError); ok {
				logger.Log("msg", "slack rate limit, backing off")
				return retry.RetryableError(err)
			}
			return err
		})
		if err != nil {
			return err
		}

		for _, channel := range channels {
			if strings.HasPrefix(channel.Name, namePrefix+"-") {
				createdAt := channel.Created.Time().UTC()
				logger.Log("msg", "found channel", "channel", channel.Name, "creator", channel.Creator, "created_at", createdAt.String())

				suffix := channel.Name[len(namePrefix+"-"):]
				incidentID, err := strconv.Atoi(suffix)
				if err != nil {
					logger.Log("msg", "channel name did not have numeric incident id suffix", "channel", channel.Name)
					continue
				}

				issue, _, err := gitlabClient.Issues.GetIssue(gitlabProductionProjectPath, incidentID, gitlab.WithContext(ctx))
				if err != nil {
					logger.Log("msg", "could not find issue for incident channel", "channel", channel.Name, "err", err)
					continue
				}

				if channel.Creator == self.UserID && issue.State == "closed" {
					if dryRun {
						logger.Log("msg", "did not archive channel because this is a dry run", "channel", channel.Name)
					} else {
						logger.Log("msg", "archiving channel", "channel", channel.Name)
						if err := client.ArchiveConversationContext(ctx, channel.ID); err != nil {
							logger.Log("msg", "could not archive incident channel", "channel", channel.Name, "err", err)
							continue
						}
					}
				}
			}
		}
		if nextCursor == "" {
			return nil
		}
		listParams.Cursor = nextCursor
	}
}
