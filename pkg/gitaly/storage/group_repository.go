package storage

import (
	"context"
	"fmt"
	"strings"
	"time"

	"github.com/fatih/semgroup"
	"github.com/go-kit/log"
	"github.com/go-kit/log/level"
	"github.com/prometheus/client_golang/prometheus"
	"github.com/xanzy/go-gitlab"
)

type groupsClient struct {
	listGroups                  func(opt *gitlab.ListGroupsOptions, options ...gitlab.RequestOptionFunc) ([]*gitlab.Group, *gitlab.Response, error)
	scheduleStorageMoveForGroup func(group int, opts gitlab.ScheduleStorageMoveForGroupOptions, options ...gitlab.RequestOptionFunc) (*gitlab.GroupRepositoryStorageMove, *gitlab.Response, error)
	getStorageMoveForGroup      func(repositoryStorage int, options ...gitlab.RequestOptionFunc) (*gitlab.GroupRepositoryStorageMove, *gitlab.Response, error)
}

type groupRepository struct {
	metrics *repositoryMetrics
}

const (
	groupNamespace = gitalyctlNamespace + "_groups"
)

func (g *groupRepository) drain(ctx context.Context, c *gitlab.Client, cfg *DrainCfg, logger log.Logger, storage string) error {
	gitlabClient := &groupsClient{
		listGroups:                  c.Groups.ListGroups,
		scheduleStorageMoveForGroup: c.GroupRepositoryStorageMove.ScheduleStorageMoveForGroup,
		getStorageMoveForGroup:      c.GroupRepositoryStorageMove.GetStorageMove,
	}

	err := retryWithBackOff(ctx, func() error {
		return g.moveGroups(ctx, gitlabClient, cfg, logger, storage)
	})
	if err != nil {
		return fmt.Errorf("drain group storage: %w", err)
	}

	return nil
}

func (g *groupRepository) moveGroups(ctx context.Context, gitlabClient *groupsClient, cfg *DrainCfg, logger log.Logger, storage string) error {
	sg := semgroup.NewGroup(ctx, cfg.Group.Concurrency)
	g.metrics.currentPageGauge.WithLabelValues(storage).Set(1)

	opts := &gitlab.ListGroupsOptions{
		OrderBy:      gitlab.Ptr("id"),
		Sort:         gitlab.Ptr("asc"),
		Statistics:   gitlab.Ptr(true),
		AllAvailable: gitlab.Ptr(true),
		ListOptions: gitlab.ListOptions{
			Page:    1,
			PerPage: apiResultsPerPage,
		},
		SkipGroups:        &cfg.Group.SkipRepositories,
		RepositoryStorage: gitlab.Ptr(storage),
	}

	for {
		groups, resp, err := gitlabClient.listGroups(opts, gitlab.WithContext(ctx))
		if err != nil {
			return fmt.Errorf("list groups: %w", err)
		}

		for _, gp := range groups {
			group := gp

			sg.Go(func() error {
				select {
				case <-ctx.Done():
					return ctx.Err()
				default:
					err = g.moveGroupRepository(ctx, gitlabClient, group, cfg, logger)
					if err != nil {
						return fmt.Errorf("move repository: %w", err)
					}
				}
				return nil
			})
		}

		if resp == nil || resp.NextPage == 0 {
			g.metrics.currentPageGauge.WithLabelValues(storage).Set(0)
			break
		}
		g.metrics.currentPageGauge.WithLabelValues(storage).Set(float64(resp.NextPage))
		opts.Page = resp.NextPage
	}

	if err := sg.Wait(); err != nil {
		if strings.Contains(err.Error(), "context canceled") {
			level.Info(logger).Log("event", "context cancelled")
			return ctx.Err()
		}

		return fmt.Errorf("move group: %w", err)
	}

	return nil
}

func (g *groupRepository) moveGroupRepository(ctx context.Context, gitlabClient *groupsClient, group *gitlab.Group, cfg *DrainCfg, logger log.Logger) error {
	logger = log.With(logger,
		"group", group.WebURL,
		"repository_size", group.Statistics.RepositorySize,
		"timeout_duration", cfg.Group.MoveTimeout,
	)
	var repositoryMoveOpts gitlab.ScheduleStorageMoveForGroupOptions

	if cfg.DryRun {
		level.Info(logger).Log("dry_run", "true", "event", "move repository")
		return nil
	}

	level.Info(logger).Log("event", "move repository")

	if dstStorage := cfg.Group.DestinationStorage; dstStorage != "" {
		repositoryMoveOpts.DestinationStorageName = gitlab.Ptr(dstStorage)
	}

	repositoryMove, _, err := gitlabClient.scheduleStorageMoveForGroup(group.ID, repositoryMoveOpts, gitlab.WithContext(ctx))
	if err != nil {
		level.Error(logger).Log("event", "move repository failure", "err", err)
		g.metrics.failedRepositoryMovesCounter.WithLabelValues(group.RepositoryStorage).Inc()
		return fmt.Errorf("schedule group repository move: %w", err)
	}

	logger = log.With(logger,
		"group_repository_move_id", repositoryMove.ID,
		"destination_storage", repositoryMove.DestinationStorageName,
	)

	level.Info(logger).Log("state", repositoryMove.State, "event", "move repository state")
	err = g.waitForGroupRepositoryMove(gitlabClient, repositoryMove, group, cfg, logger)
	if err != nil {
		level.Error(logger).Log("event", "move repository state failure", "err", err)
		return fmt.Errorf("wait for group repository move: %w", err)
	}

	return nil
}

func (g *groupRepository) waitForGroupRepositoryMove(gitlabClient *groupsClient, scheduledRepositoryMove *gitlab.GroupRepositoryStorageMove, group *gitlab.Group, cfg *DrainCfg, logger log.Logger) error {
	startTime := time.Now()

	g.metrics.activeRepositoryMovesGauge.WithLabelValues(scheduledRepositoryMove.SourceStorageName).Inc()
	defer g.metrics.activeRepositoryMovesGauge.WithLabelValues(scheduledRepositoryMove.SourceStorageName).Dec()

	for {
		repositoryMove, _, err := gitlabClient.getStorageMoveForGroup(scheduledRepositoryMove.ID)
		if err != nil {
			level.Error(logger).Log("event", "get storage move for group error", "err", err)
			g.metrics.failedRepositoryMovesCounter.WithLabelValues(scheduledRepositoryMove.SourceStorageName).Inc()
			return fmt.Errorf("retrieve group repository move: %w", err)
		}

		repoMoveLogger := log.With(logger, "state", repositoryMove.State)

		if time.Since(startTime) >= cfg.Group.MoveTimeout {
			level.Error(repoMoveLogger).Log("event", "move repository timeout")
			g.metrics.timedOutRepositoryMovesCounter.WithLabelValues(repositoryMove.SourceStorageName).Inc()
			break
		}

		if repositoryMove.State == repositoryMoveFinished {
			level.Info(repoMoveLogger).Log("event", "move repository state success")
			g.metrics.successfulRepositoryMovesCounter.WithLabelValues(repositoryMove.SourceStorageName).Inc()
			break
		} else if repositoryMove.State == repositoryMoveFailed || repositoryMove.State == repositoryMoveCleanupFailed {
			level.Error(repoMoveLogger).Log("event", "move repository state failure")
			g.metrics.failedRepositoryMovesCounter.WithLabelValues(repositoryMove.SourceStorageName).Inc()
			break
		}

		level.Info(repoMoveLogger).Log("event", "move repository state")
		// Prevents spamming the endpoint between checks
		time.Sleep(cfg.Group.MoveStatusUpdate)
	}

	return nil
}

func newGroupRepository(cfg *DrainCfg, registry prometheus.Registerer) *groupRepository {
	g := groupRepository{}
	m := registerMetrics(groupNamespace)

	g.metrics = &m
	g.metrics.itemsPerPageGauge.Set(apiResultsPerPage)
	g.metrics.storageConcurrencyGauge.Set(float64(cfg.Storage.Concurrency))
	g.metrics.repositoryConcurrencyGauge.Set(float64(cfg.Group.Concurrency))

	if registry != nil {
		registry.MustRegister(
			g.metrics.currentPageGauge,
			g.metrics.skippedRepositoryCounter,
			g.metrics.activeRepositoryMovesGauge,
			g.metrics.failedRepositoryMovesCounter,
			g.metrics.timedOutRepositoryMovesCounter,
			g.metrics.readOnlyRepositoriesCounter,
			g.metrics.successfulRepositoryMovesCounter,
			g.metrics.itemsPerPageGauge,
			g.metrics.storageConcurrencyGauge,
			g.metrics.repositoryConcurrencyGauge,
		)
	}

	return &g
}

// Describe is used to describe Prometheus metrics.
func (g *groupRepository) Describe(descs chan<- *prometheus.Desc) {
	prometheus.DescribeByCollect(g, descs)
}

// Collect is used to collect Prometheus metrics.
func (g *groupRepository) Collect(metrics chan<- prometheus.Metric) {
	g.metrics.currentPageGauge.Collect(metrics)
	g.metrics.skippedRepositoryCounter.Collect(metrics)
	g.metrics.activeRepositoryMovesGauge.Collect(metrics)
	g.metrics.failedRepositoryMovesCounter.Collect(metrics)
	g.metrics.timedOutRepositoryMovesCounter.Collect(metrics)
	g.metrics.readOnlyRepositoriesCounter.Collect(metrics)
	g.metrics.successfulRepositoryMovesCounter.Collect(metrics)
}
