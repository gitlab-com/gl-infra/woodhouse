package storage

import (
	"context"
	"fmt"
	"strings"
	"time"

	"github.com/fatih/semgroup"
	"github.com/go-kit/log"
	"github.com/go-kit/log/level"
	"github.com/prometheus/client_golang/prometheus"
	"github.com/xanzy/go-gitlab"
)

type snippetClient struct {
	listAllSnippets               func(opt *gitlab.ListAllSnippetsOptions, options ...gitlab.RequestOptionFunc) ([]*gitlab.Snippet, *gitlab.Response, error)
	scheduleStorageMoveForSnippet func(snippet int, opts gitlab.ScheduleStorageMoveForSnippetOptions, options ...gitlab.RequestOptionFunc) (*gitlab.SnippetRepositoryStorageMove, *gitlab.Response, error)
	getStorageMoveForSnippet      func(repositoryStorage int, options ...gitlab.RequestOptionFunc) (*gitlab.SnippetRepositoryStorageMove, *gitlab.Response, error)
}

type snippetRepository struct {
	metrics *repositoryMetrics
}

const (
	snippetNamespace = gitalyctlNamespace + "_snippets"
)

func (s *snippetRepository) drain(ctx context.Context, c *gitlab.Client, cfg *DrainCfg, logger log.Logger, storage string) error {
	gitLabClient := &snippetClient{
		listAllSnippets:               c.Snippets.ListAllSnippets,
		scheduleStorageMoveForSnippet: c.SnippetRepositoryStorageMove.ScheduleStorageMoveForSnippet,
		getStorageMoveForSnippet:      c.SnippetRepositoryStorageMove.GetStorageMove,
	}

	err := retryWithBackOff(ctx, func() error {
		return s.moveSnippets(ctx, gitLabClient, cfg, logger, storage)
	})
	if err != nil {
		return fmt.Errorf("drain snippet storage: %w", err)
	}

	return nil
}

func (s *snippetRepository) moveSnippets(ctx context.Context, gitlabClient *snippetClient, cfg *DrainCfg, logger log.Logger, storage string) error {
	sg := semgroup.NewGroup(ctx, cfg.Snippet.Concurrency)
	s.metrics.currentPageGauge.WithLabelValues(storage).Set(1)

	opts := &gitlab.ListAllSnippetsOptions{
		ListOptions: gitlab.ListOptions{
			Page:    1,
			PerPage: apiResultsPerPage,
		},
		RepositoryStorage: gitlab.Ptr(storage),
	}

	for {
		snippets, resp, err := gitlabClient.listAllSnippets(opts, gitlab.WithContext(ctx))
		if err != nil {
			return fmt.Errorf("list snippets: %w", err)
		}

		for _, snip := range snippets {
			snippet := snip
			if s.isSkippable(snippet, cfg, logger) {
				continue
			}

			sg.Go(func() error {
				select {
				case <-ctx.Done():
					return ctx.Err()
				default:
					err = s.moveSnippetRepository(ctx, gitlabClient, snippet, cfg, logger)
					if err != nil {
						return fmt.Errorf("move repository: %w", err)
					}
				}
				return nil
			})
		}

		if resp == nil || resp.NextPage == 0 {
			s.metrics.currentPageGauge.WithLabelValues(storage).Set(0)
			break
		}
		s.metrics.currentPageGauge.WithLabelValues(storage).Set(float64(resp.NextPage))
		opts.ListOptions.Page = resp.NextPage
	}

	if err := sg.Wait(); err != nil {
		if strings.Contains(err.Error(), "context canceled") {
			level.Info(logger).Log("event", "context cancelled")
			return ctx.Err()
		}

		return fmt.Errorf("move snippet: %w", err)
	}

	return nil
}

func (s *snippetRepository) isSkippable(snippet *gitlab.Snippet, cfg *DrainCfg, logger log.Logger) bool {
	logger = log.With(logger,
		"event", "skip snippet",
		"snippet", snippet.WebURL,
		"snippet_id", snippet.ID,
	)

	for _, snippetID := range cfg.Snippet.SkipRepositories {
		if snippetID == snippet.ID {
			s.metrics.skippedRepositoryCounter.WithLabelValues("marked_skip").Inc()
			level.Info(logger).Log("reason", "snippet marked for skip")
			return true
		}
	}

	return false
}

func (s *snippetRepository) moveSnippetRepository(ctx context.Context, gitlabClient *snippetClient, snippet *gitlab.Snippet, cfg *DrainCfg, logger log.Logger) error {
	logger = log.With(logger,
		"snippet", snippet.WebURL,
		"timeout_duration", cfg.Snippet.MoveTimeout,
	)
	var repositoryMoveOpts gitlab.ScheduleStorageMoveForSnippetOptions

	if cfg.DryRun {
		level.Info(logger).Log("dry_run", "true", "event", "move repository")
		return nil
	}

	level.Info(logger).Log("event", "move repository")

	if dstStorage := cfg.Snippet.DestinationStorage; dstStorage != "" {
		repositoryMoveOpts.DestinationStorageName = gitlab.Ptr(dstStorage)
	}

	repositoryMove, _, err := gitlabClient.scheduleStorageMoveForSnippet(snippet.ID, repositoryMoveOpts, gitlab.WithContext(ctx))
	if err != nil {
		level.Error(logger).Log("event", "move repository failure", "err", err)
		s.metrics.failedRepositoryMovesCounter.WithLabelValues(snippet.RepositoryStorage).Inc()
		return fmt.Errorf("schedule snippet repository move: %w", err)
	}

	logger = log.With(logger,
		"snippet_repository_move_id", repositoryMove.ID,
		"destination_storage", repositoryMove.DestinationStorageName,
	)

	level.Info(logger).Log("state", repositoryMove.State, "event", "move repository state")

	err = s.waitForSnippetRepositoryMove(gitlabClient, repositoryMove, snippet, cfg, logger)
	if err != nil {
		return fmt.Errorf("wait for snippet repository move: %w", err)
	}

	return nil
}

func (s *snippetRepository) waitForSnippetRepositoryMove(gitlabClient *snippetClient, scheduledRepositoryMove *gitlab.SnippetRepositoryStorageMove, snippet *gitlab.Snippet, cfg *DrainCfg, logger log.Logger) error {
	startTime := time.Now()

	s.metrics.activeRepositoryMovesGauge.WithLabelValues(scheduledRepositoryMove.SourceStorageName).Inc()
	defer s.metrics.activeRepositoryMovesGauge.WithLabelValues(scheduledRepositoryMove.SourceStorageName).Dec()

	for {
		repositoryMove, _, err := gitlabClient.getStorageMoveForSnippet(scheduledRepositoryMove.ID)
		if err != nil {
			level.Error(logger).Log("event", "retrieve snippet repository move", "err", err)
			s.metrics.failedRepositoryMovesCounter.WithLabelValues(scheduledRepositoryMove.SourceStorageName).Inc()
			return fmt.Errorf("retrieve snippet repository move: %w", err)
		}
		repoMoveLogger := log.With(logger, "state", repositoryMove.State)

		if time.Since(startTime) >= cfg.Snippet.MoveTimeout {
			level.Error(repoMoveLogger).Log("event", "move repository timeout")
			s.metrics.timedOutRepositoryMovesCounter.WithLabelValues(repositoryMove.SourceStorageName).Inc()
			break
		}

		if repositoryMove.State == repositoryMoveFinished {
			level.Info(repoMoveLogger).Log("event", "move repository state success")
			s.metrics.successfulRepositoryMovesCounter.WithLabelValues(repositoryMove.SourceStorageName).Inc()
			break
		} else if repositoryMove.State == repositoryMoveFailed || repositoryMove.State == repositoryMoveCleanupFailed {
			level.Error(repoMoveLogger).Log("event", "move repository state failed")
			s.metrics.failedRepositoryMovesCounter.WithLabelValues(repositoryMove.SourceStorageName).Inc()
			break
		}

		level.Info(repoMoveLogger).Log("event", "move repository state")
		// Prevents spamming the endpoint between checks
		time.Sleep(cfg.Snippet.MoveStatusUpdate)
	}

	return nil
}

func newSnippetRepository(cfg *DrainCfg, registry prometheus.Registerer) *snippetRepository {
	s := snippetRepository{}
	m := registerMetrics(snippetNamespace)

	s.metrics = &m
	s.metrics.itemsPerPageGauge.Set(apiResultsPerPage)
	s.metrics.storageConcurrencyGauge.Set(float64(cfg.Storage.Concurrency))
	s.metrics.repositoryConcurrencyGauge.Set(float64(cfg.Snippet.Concurrency))

	if registry != nil {
		registry.MustRegister(
			s.metrics.currentPageGauge,
			s.metrics.skippedRepositoryCounter,
			s.metrics.activeRepositoryMovesGauge,
			s.metrics.failedRepositoryMovesCounter,
			s.metrics.timedOutRepositoryMovesCounter,
			s.metrics.readOnlyRepositoriesCounter,
			s.metrics.successfulRepositoryMovesCounter,
			s.metrics.itemsPerPageGauge,
			s.metrics.storageConcurrencyGauge,
			s.metrics.repositoryConcurrencyGauge,
		)
	}

	return &s
}

// Describe is used to describe Prometheus metrics.
func (s *snippetRepository) Describe(descs chan<- *prometheus.Desc) {
	prometheus.DescribeByCollect(s, descs)
}

// Collect is used to collect Prometheus metrics.
func (s *snippetRepository) Collect(metrics chan<- prometheus.Metric) {
	s.metrics.currentPageGauge.Collect(metrics)
	s.metrics.skippedRepositoryCounter.Collect(metrics)
	s.metrics.activeRepositoryMovesGauge.Collect(metrics)
	s.metrics.failedRepositoryMovesCounter.Collect(metrics)
	s.metrics.timedOutRepositoryMovesCounter.Collect(metrics)
	s.metrics.readOnlyRepositoriesCounter.Collect(metrics)
	s.metrics.successfulRepositoryMovesCounter.Collect(metrics)
}
