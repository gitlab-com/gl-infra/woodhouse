package storage

import (
	"bytes"
	"context"
	"fmt"
	"net/http"
	"testing"
	"time"

	"github.com/go-kit/log"
	"github.com/prometheus/client_golang/prometheus"
	"github.com/prometheus/client_golang/prometheus/testutil"
	"github.com/stretchr/testify/assert"
	"github.com/stretchr/testify/require"
	"github.com/xanzy/go-gitlab"
)

type groupTestArgs struct {
	group          *gitlab.Group
	repositoryMove *gitlab.GroupRepositoryStorageMove
	gitlabClient   *groupsClient
	cfg            *DrainCfg
	logger         log.Logger
	stdout         *bytes.Buffer
	err            error
}

type groupTest struct {
	name             string
	args             groupTestArgs
	assertCustomFunc func(t *testing.T, args groupTestArgs)
	assertCustom     interface{}
	assertError      string
	assertMetrics    string
}

var scheduleGroupRepositoryStorageMoveCalledCount = 0

func TestFetchGroups(t *testing.T) {
	tests := []groupTest{
		{
			name: "ReturnsNilErrorOnSuccess",
			args: groupTestArgs{
				gitlabClient: &groupsClient{
					listGroups:                  listGroupsReturnsGroup,
					scheduleStorageMoveForGroup: scheduleStorageMoveForGroupReturnsSuccess,
					getStorageMoveForGroup:      getGroupStorageMoveWithStateFinished,
				},
				cfg: &DrainCfg{
					Group: RepositoryCfg{
						Concurrency: 1,
						MoveTimeout: 3 * time.Second,
					},
				},
			},
			assertError: "",
			assertMetrics: fmt.Sprintf(`
				# HELP gitalyctl_groups_active_repository_moves Number of repository moves in progress.
        	    # TYPE gitalyctl_groups_active_repository_moves gauge
        	    gitalyctl_groups_active_repository_moves{storage="%v"} 0
				# HELP gitalyctl_groups_current_page Current page number for the API results.
				# TYPE gitalyctl_groups_current_page gauge
				gitalyctl_groups_current_page{storage="%[1]v"} 0
				# HELP gitalyctl_groups_successful_repository_moves_total Number of successful repository moves.
        	    # TYPE gitalyctl_groups_successful_repository_moves_total counter
        	    gitalyctl_groups_successful_repository_moves_total{storage="TestSourceStorage"} 1
			`, testGitalySourceStorageName),
		},
		{
			name: "ErrorsWhenListGroupsReturnsError",
			args: groupTestArgs{
				gitlabClient: &groupsClient{
					listGroups: listGroupsReturnsError,
				},
				cfg: &DrainCfg{
					Group: RepositoryCfg{
						Concurrency: 1,
						MoveTimeout: 3 * time.Second,
					},
				},
			},
			assertError: "list groups: test error",
			assertMetrics: fmt.Sprintf(`
				# HELP gitalyctl_groups_current_page Current page number for the API results.
        	    # TYPE gitalyctl_groups_current_page gauge
        	    gitalyctl_groups_current_page{storage="%v"} 1
			`, testGitalySourceStorageName),
		},
	}
	ctx := context.Background()

	for _, tt := range tests {
		tt.args.stdout = &bytes.Buffer{}
		tt.args.logger = setupLogger(tt.args.stdout)

		r := prometheus.NewRegistry()
		g := newGroupRepository(tt.args.cfg, r)

		t.Run(tt.name, func(t *testing.T) {
			res := g.moveGroups(ctx, tt.args.gitlabClient, tt.args.cfg, tt.args.logger, testGitalySourceStorageName)

			g.assertTests(t, tt, res)
			if tt.assertCustom != nil {
				assert.Equal(t, tt.assertCustom, res)
			}
		})
	}
}

func TestFetchGroups_ContextCanceledPreventsCallingMoveRepository(t *testing.T) {
	stdout := &bytes.Buffer{}
	logger := setupLogger(stdout)

	c := &groupsClient{listGroups: listGroupsReturnsGroup}
	cfg := &DrainCfg{
		Group: RepositoryCfg{
			Concurrency: 1,
			MoveTimeout: 3 * time.Second,
		},
	}

	r := prometheus.NewRegistry()
	g := newGroupRepository(cfg, r)

	ctx, cancel := context.WithCancel(context.Background())
	cancel()

	err := g.moveGroups(ctx, c, cfg, logger, testGitalySourceStorageName)
	assert.EqualError(t, err, "context canceled")
	assert.Contains(t, stdout.String(), "event=\"context cancelled\"")
	assert.NotContains(t, stdout.String(), "event=\"move repository\"")
}

func TestMoveGroupRepository(t *testing.T) {
	testGroup := &gitlab.Group{
		WebURL: "https://gitlab.example.com/test-group",
		Statistics: &gitlab.Statistics{
			RepositorySize: 100,
		},
		RepositoryStorage: "default",
	}
	testCfg := &DrainCfg{Group: RepositoryCfg{MoveTimeout: time.Second}}

	tests := []groupTest{
		{
			name: "LogsDryRun",
			args: groupTestArgs{
				gitlabClient: &groupsClient{scheduleStorageMoveForGroup: scheduleStorageMoveForGroupIsCalled},
				group:        testGroup,
				cfg:          &DrainCfg{DryRun: true},
			},
			assertError:   "",
			assertMetrics: "",
			assertCustomFunc: func(t *testing.T, args groupTestArgs) {
				assert.Equal(t, 0, scheduleGroupRepositoryStorageMoveCalledCount)
			},
		},
		{
			name: "ReturnsNilErrorOnSuccess",
			args: groupTestArgs{
				gitlabClient: &groupsClient{
					scheduleStorageMoveForGroup: scheduleStorageMoveForGroupReturnsSuccess,
					getStorageMoveForGroup:      getGroupStorageMoveWithStateFinished,
				},
				group: testGroup,
				cfg:   testCfg,
			},
			assertError: "",
			assertMetrics: fmt.Sprintf(`
				# HELP gitalyctl_groups_active_repository_moves Number of repository moves in progress.
				# TYPE gitalyctl_groups_active_repository_moves gauge
				gitalyctl_groups_active_repository_moves{storage="%v"} 0
				# HELP gitalyctl_groups_successful_repository_moves_total Number of successful repository moves.
        	    # TYPE gitalyctl_groups_successful_repository_moves_total counter
        	    gitalyctl_groups_successful_repository_moves_total{storage="%[1]v"} 1
			`, testGitalySourceStorageName),
		},
		{
			name: "ReturnsErrorOnScheduleMoveFailure",
			args: groupTestArgs{
				gitlabClient: &groupsClient{scheduleStorageMoveForGroup: scheduleStorageMoveForGroupReturnsError},
				group:        testGroup,
				cfg:          testCfg,
			},
			assertError: "schedule group repository move: test error",
			// TODO: Update once repository storage label added
			assertMetrics: fmt.Sprintf(`
				# HELP gitalyctl_groups_failed_repository_moves_total Number of failed repository moves.
        	    # TYPE gitalyctl_groups_failed_repository_moves_total counter
        	    gitalyctl_groups_failed_repository_moves_total{storage="%v"} 1
			`, "default"),
		},
		{
			name: "ReturnsErrorWhenWaitForRepositoryMoveFails",
			args: groupTestArgs{
				gitlabClient: &groupsClient{
					scheduleStorageMoveForGroup: scheduleStorageMoveForGroupReturnsSuccess,
					getStorageMoveForGroup:      getGroupStorageMoveReturnsError,
				},
				group: testGroup,
				cfg:   testCfg,
			},
			assertError: "wait for group repository move: retrieve group repository move: test error",
			assertMetrics: fmt.Sprintf(`
				# HELP gitalyctl_groups_active_repository_moves Number of repository moves in progress.
				# TYPE gitalyctl_groups_active_repository_moves gauge
				gitalyctl_groups_active_repository_moves{storage="%v"} 0
				# HELP gitalyctl_groups_failed_repository_moves_total Number of failed repository moves.
        	    # TYPE gitalyctl_groups_failed_repository_moves_total counter
        	    gitalyctl_groups_failed_repository_moves_total{storage="%[1]v"} 1
			`, testGitalySourceStorageName),
		},
		{
			name: "ReturnsErrorOnGetGroupRepositoryStorageMoveFailure",
			args: groupTestArgs{
				gitlabClient: &groupsClient{
					scheduleStorageMoveForGroup: scheduleStorageMoveForGroupReturnsSuccess,
					getStorageMoveForGroup:      getGroupStorageMoveReturnsError,
				},
				group: testGroup,
				cfg:   testCfg,
			},
			assertError: "wait for group repository move: retrieve group repository move: test error",
			assertMetrics: fmt.Sprintf(`
				# HELP gitalyctl_groups_active_repository_moves Number of repository moves in progress.
				# TYPE gitalyctl_groups_active_repository_moves gauge
				gitalyctl_groups_active_repository_moves{storage="%v"} 0
				# HELP gitalyctl_groups_failed_repository_moves_total Number of failed repository moves.
        	    # TYPE gitalyctl_groups_failed_repository_moves_total counter
        	    gitalyctl_groups_failed_repository_moves_total{storage="%[1]v"} 1
			`, testGitalySourceStorageName),
		},
	}

	for _, tt := range tests {
		ctx := context.Background()

		tt.args.stdout = &bytes.Buffer{}
		tt.args.logger = setupLogger(tt.args.stdout)

		r := prometheus.NewRegistry()
		g := newGroupRepository(tt.args.cfg, r)

		t.Run(tt.name, func(t *testing.T) {
			err := g.moveGroupRepository(ctx, tt.args.gitlabClient, tt.args.group, tt.args.cfg, tt.args.logger)
			g.assertTests(t, tt, err)
		})
	}
}

func TestWaitForGroupRepositoryMove(t *testing.T) {
	testGroup := &gitlab.Group{
		WebURL: "https://gitlab.example.com/test-group",
		Statistics: &gitlab.Statistics{
			RepositorySize: 1,
		},
	}
	testCfg := &DrainCfg{Group: RepositoryCfg{MoveTimeout: 10 * time.Second}}

	tests := []groupTest{
		{
			name: "ReturnsNilErrorForFinishedMove",
			args: groupTestArgs{
				gitlabClient:   &groupsClient{getStorageMoveForGroup: getGroupStorageMoveWithStateFinished},
				group:          testGroup,
				repositoryMove: &gitlab.GroupRepositoryStorageMove{ID: 1, SourceStorageName: testGitalySourceStorageName},
				cfg:            testCfg,
			},
			assertError: "",
			assertMetrics: fmt.Sprintf(`
				# HELP gitalyctl_groups_active_repository_moves Number of repository moves in progress.
				# TYPE gitalyctl_groups_active_repository_moves gauge
				gitalyctl_groups_active_repository_moves{storage="%v"} 0
				# HELP gitalyctl_groups_successful_repository_moves_total Number of successful repository moves.
				# TYPE gitalyctl_groups_successful_repository_moves_total counter
				gitalyctl_groups_successful_repository_moves_total{storage="%[1]v"} 1
			`, testGitalySourceStorageName),
		},
		{
			name: "ReturnsNilErrorForFailedMove",
			args: groupTestArgs{
				gitlabClient:   &groupsClient{getStorageMoveForGroup: getGroupStorageMoveWithStateFailed},
				group:          testGroup,
				repositoryMove: &gitlab.GroupRepositoryStorageMove{ID: 1, SourceStorageName: testGitalySourceStorageName},
				cfg:            testCfg,
			},
			assertError: "",
			assertMetrics: fmt.Sprintf(`
				# HELP gitalyctl_groups_active_repository_moves Number of repository moves in progress.
				# TYPE gitalyctl_groups_active_repository_moves gauge
				gitalyctl_groups_active_repository_moves{storage="%v"} 0
				# HELP gitalyctl_groups_failed_repository_moves_total Number of failed repository moves.
        	    # TYPE gitalyctl_groups_failed_repository_moves_total counter
        	    gitalyctl_groups_failed_repository_moves_total{storage="%[1]v"} 1
			`, testGitalySourceStorageName),
		},
		{
			name: "ReturnsNilErrorForCleanupFailedMove",
			args: groupTestArgs{
				gitlabClient:   &groupsClient{getStorageMoveForGroup: getGroupStorageMoveWithStateCleanupFailed},
				group:          testGroup,
				repositoryMove: &gitlab.GroupRepositoryStorageMove{ID: 1, SourceStorageName: testGitalySourceStorageName},
				cfg:            testCfg,
			},
			assertError: "",
			assertMetrics: fmt.Sprintf(`
				# HELP gitalyctl_groups_active_repository_moves Number of repository moves in progress.
				# TYPE gitalyctl_groups_active_repository_moves gauge
				gitalyctl_groups_active_repository_moves{storage="%v"} 0
				# HELP gitalyctl_groups_failed_repository_moves_total Number of failed repository moves.
        	    # TYPE gitalyctl_groups_failed_repository_moves_total counter
        	    gitalyctl_groups_failed_repository_moves_total{storage="%[1]v"} 1
			`, testGitalySourceStorageName),
		},
		{
			name: "GetStorageMoveReturnsError",
			args: groupTestArgs{
				gitlabClient:   &groupsClient{getStorageMoveForGroup: getGroupStorageMoveReturnsError},
				group:          testGroup,
				repositoryMove: &gitlab.GroupRepositoryStorageMove{ID: 1, SourceStorageName: testGitalySourceStorageName},
				cfg:            testCfg,
			},
			assertError: "retrieve group repository move: test error",
			assertMetrics: fmt.Sprintf(`
				# HELP gitalyctl_groups_active_repository_moves Number of repository moves in progress.
				# TYPE gitalyctl_groups_active_repository_moves gauge
				gitalyctl_groups_active_repository_moves{storage="%v"} 0
				# HELP gitalyctl_groups_failed_repository_moves_total Number of failed repository moves.
        	    # TYPE gitalyctl_groups_failed_repository_moves_total counter
				gitalyctl_groups_failed_repository_moves_total{storage="%[1]v"} 1
			`, testGitalySourceStorageName),
		},
		{
			name: "LogsErrorOnTimeout",
			args: groupTestArgs{
				gitlabClient: &groupsClient{getStorageMoveForGroup: getGroupStorageMoveWithStateStarted},
				group:        testGroup,
				repositoryMove: &gitlab.GroupRepositoryStorageMove{
					ID:                     1,
					SourceStorageName:      testGitalySourceStorageName,
					DestinationStorageName: testGitalyDestinationStorageName,
				},
				cfg: &DrainCfg{Group: RepositoryCfg{MoveTimeout: time.Second}},
			},
			assertError: "",
			assertMetrics: fmt.Sprintf(`
				# HELP gitalyctl_groups_active_repository_moves Number of repository moves in progress.
				# TYPE gitalyctl_groups_active_repository_moves gauge
				gitalyctl_groups_active_repository_moves{storage="%v"} 0
				# HELP gitalyctl_groups_timed_out_repository_moves_total Number of timed out repository moves.
        	    # TYPE gitalyctl_groups_timed_out_repository_moves_total counter
        	    gitalyctl_groups_timed_out_repository_moves_total{storage="%[1]v"} 1
			`, testGitalySourceStorageName),
		},
	}

	for _, tt := range tests {
		tt.args.stdout = &bytes.Buffer{}
		tt.args.logger = setupLogger(tt.args.stdout)

		r := prometheus.NewRegistry()
		g := newGroupRepository(tt.args.cfg, r)

		t.Run(tt.name, func(t *testing.T) {
			err := g.waitForGroupRepositoryMove(tt.args.gitlabClient, tt.args.repositoryMove, tt.args.group, tt.args.cfg, tt.args.logger)
			g.assertTests(t, tt, err)
		})
	}
}

func TestGroupRepositoryPagination(t *testing.T) {
	var pagesVisited []int

	gitLabClient := &groupsClient{listGroups: func(opt *gitlab.ListGroupsOptions, options ...gitlab.RequestOptionFunc) ([]*gitlab.Group, *gitlab.Response, error) {
		require.Equal(t, *opt.RepositoryStorage, "storage1")
		require.Equal(t, *&opt.PerPage, apiResultsPerPage)

		pagesVisited = append(pagesVisited, opt.Page)

		nextPage := opt.Page + 1
		if nextPage == 5 { // We've reached the end.
			nextPage = 0
		}

		resp := gitlab.Response{
			Response: &http.Response{},
			NextPage: nextPage,
		}

		return nil, &resp, nil
	}}

	cfg := &DrainCfg{
		Storage: StorageCfg{Concurrency: 1},
		Group: RepositoryCfg{
			Concurrency:      1,
			MoveTimeout:      3 * time.Second,
			MoveStatusUpdate: time.Second,
		},
	}

	ctx := context.Background()
	g := newGroupRepository(cfg, nil)

	require.NoError(t, g.moveGroups(ctx, gitLabClient, cfg, nil, "storage1"))
	require.Equal(t, []int{1, 2, 3, 4}, pagesVisited)
}

func (g *groupRepository) assertTests(t *testing.T, tt groupTest, err error) {
	if tt.assertError == "" {
		require.NoError(t, err)
	} else {
		assert.EqualError(t, err, tt.assertError)
	}
	require.NoError(t, testutil.CollectAndCompare(g, bytes.NewBufferString(tt.assertMetrics)))
}

func listGroupsReturnsGroup(opt *gitlab.ListGroupsOptions, options ...gitlab.RequestOptionFunc) ([]*gitlab.Group, *gitlab.Response, error) {
	g := []*gitlab.Group{
		{
			ID:     1,
			WebURL: "https://gitlab.example.com/test-group/test-group-group-gofedo",
			Statistics: &gitlab.Statistics{
				RepositorySize: 1,
			},
		},
	}
	return g, nil, nil
}

func scheduleStorageMoveForGroupReturnsSuccess(group int, opts gitlab.ScheduleStorageMoveForGroupOptions, options ...gitlab.RequestOptionFunc) (*gitlab.GroupRepositoryStorageMove, *gitlab.Response, error) {
	return &gitlab.GroupRepositoryStorageMove{
		ID:                     1,
		CreatedAt:              gitlab.Time(time.Now()),
		State:                  repositoryMoveScheduled,
		SourceStorageName:      testGitalySourceStorageName,
		DestinationStorageName: testGitalyDestinationStorageName,
	}, nil, nil
}

func scheduleStorageMoveForGroupReturnsError(group int, opts gitlab.ScheduleStorageMoveForGroupOptions, options ...gitlab.RequestOptionFunc) (*gitlab.GroupRepositoryStorageMove, *gitlab.Response, error) {
	return nil, nil, fmt.Errorf("test error")
}

func getGroupStorageMoveWithStateFinished(repositoryStorage int, options ...gitlab.RequestOptionFunc) (*gitlab.GroupRepositoryStorageMove, *gitlab.Response, error) {
	return &gitlab.GroupRepositoryStorageMove{
		ID:                     1,
		CreatedAt:              gitlab.Time(time.Now()),
		State:                  repositoryMoveFinished,
		SourceStorageName:      testGitalySourceStorageName,
		DestinationStorageName: testGitalyDestinationStorageName,
	}, nil, nil
}

func getGroupStorageMoveWithStateFailed(repositoryStorage int, options ...gitlab.RequestOptionFunc) (*gitlab.GroupRepositoryStorageMove, *gitlab.Response, error) {
	return &gitlab.GroupRepositoryStorageMove{
		ID:                     1,
		CreatedAt:              gitlab.Time(time.Now()),
		State:                  repositoryMoveFailed,
		SourceStorageName:      testGitalySourceStorageName,
		DestinationStorageName: testGitalyDestinationStorageName,
	}, nil, nil
}

func getGroupStorageMoveWithStateCleanupFailed(repositoryStorage int, options ...gitlab.RequestOptionFunc) (*gitlab.GroupRepositoryStorageMove, *gitlab.Response, error) {
	return &gitlab.GroupRepositoryStorageMove{
		ID:                     1,
		CreatedAt:              gitlab.Time(time.Now()),
		State:                  repositoryMoveCleanupFailed,
		SourceStorageName:      testGitalySourceStorageName,
		DestinationStorageName: testGitalyDestinationStorageName,
	}, nil, nil
}

func getGroupStorageMoveWithStateStarted(repositoryStorage int, options ...gitlab.RequestOptionFunc) (*gitlab.GroupRepositoryStorageMove, *gitlab.Response, error) {
	return &gitlab.GroupRepositoryStorageMove{
		ID:                     1,
		CreatedAt:              gitlab.Time(time.Now()),
		State:                  repositoryMoveStarted,
		SourceStorageName:      testGitalySourceStorageName,
		DestinationStorageName: testGitalyDestinationStorageName,
	}, nil, nil
}

func listGroupsReturnsError(opt *gitlab.ListGroupsOptions, options ...gitlab.RequestOptionFunc) ([]*gitlab.Group, *gitlab.Response, error) {
	return []*gitlab.Group{}, nil, fmt.Errorf("test error")
}

func scheduleStorageMoveForGroupIsCalled(group int, opts gitlab.ScheduleStorageMoveForGroupOptions, options ...gitlab.RequestOptionFunc) (*gitlab.GroupRepositoryStorageMove, *gitlab.Response, error) {
	scheduleGroupRepositoryStorageMoveCalledCount++
	return nil, nil, nil
}

func getGroupStorageMoveReturnsError(repositoryStorage int, options ...gitlab.RequestOptionFunc) (*gitlab.GroupRepositoryStorageMove, *gitlab.Response, error) {
	return nil, nil, fmt.Errorf("test error")
}
