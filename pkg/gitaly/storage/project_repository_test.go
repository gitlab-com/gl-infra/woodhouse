package storage

import (
	"bytes"
	"context"
	"fmt"
	"net/http"
	"testing"
	"time"

	"github.com/go-kit/log"
	"github.com/prometheus/client_golang/prometheus"
	"github.com/prometheus/client_golang/prometheus/testutil"
	"github.com/stretchr/testify/assert"
	"github.com/stretchr/testify/require"
	"github.com/xanzy/go-gitlab"
)

type projectTestArgs struct {
	gitlabClient   *projectClient
	project        *gitlab.Project
	repositoryMove *gitlab.ProjectRepositoryStorageMove
	cfg            *DrainCfg
	logger         log.Logger
	stdout         *bytes.Buffer
	err            error
}
type projectTest struct {
	name             string
	args             projectTestArgs
	assertCustomFunc func(t *testing.T, args projectTestArgs)
	assertCustom     interface{}
	assertError      string
	assertMetrics    string
}

const (
	testParentProjectRepositoryStorage = "TestRootProjectStorage"
)

var scheduleProjectRepositoryStorageMoveCalledCount = 0

func TestFetchProjects(t *testing.T) {
	tests := []projectTest{
		{
			name: "ReturnsNilErrorOnSuccess",
			args: projectTestArgs{
				gitlabClient: &projectClient{
					listProjects:                  listProjectsReturnsProject,
					scheduleStorageMoveForProject: scheduleStorageMoveForProjectReturnsSuccess,
					getStorageMoveForProject:      getStorageMoveWithStateFinished,
				},
				cfg: &DrainCfg{
					Project: RepositoryCfg{
						Concurrency: 1,
						MoveTimeout: 3 * time.Second,
					},
				},
			},
			assertError: "",
			assertMetrics: fmt.Sprintf(`
				# HELP gitalyctl_projects_active_repository_moves Number of repository moves in progress.
				# TYPE gitalyctl_projects_active_repository_moves gauge
				gitalyctl_projects_active_repository_moves{storage="%v"} 0
				# HELP gitalyctl_projects_current_page Current page number for the API results.
				# TYPE gitalyctl_projects_current_page gauge
				gitalyctl_projects_current_page{storage="%[1]v"} 0
				# HELP gitalyctl_projects_successful_repository_moves_total Number of successful repository moves.
				# TYPE gitalyctl_projects_successful_repository_moves_total counter
				gitalyctl_projects_successful_repository_moves_total{storage="%[1]v"} 1
			`, testGitalySourceStorageName),
		},
		{
			name: "ErrorsWhenListProjectsReturnsError",
			args: projectTestArgs{
				gitlabClient: &projectClient{
					listProjects: listProjectsReturnsError,
				},
				cfg: &DrainCfg{
					Project: RepositoryCfg{
						Concurrency: 1,
						MoveTimeout: 3 * time.Second,
					},
				},
			},
			assertError: "list projects: test error",
			assertMetrics: fmt.Sprintf(`
				# HELP gitalyctl_projects_current_page Current page number for the API results.
        	    # TYPE gitalyctl_projects_current_page gauge
        	    gitalyctl_projects_current_page{storage="%v"} 1
			`, testGitalySourceStorageName),
		},
	}
	ctx := context.Background()

	for _, tt := range tests {
		tt.args.stdout = &bytes.Buffer{}
		tt.args.logger = setupLogger(tt.args.stdout)

		r := prometheus.NewRegistry()
		p := newProjectRepository(tt.args.cfg, r)

		t.Run(tt.name, func(t *testing.T) {
			res := p.moveProjects(ctx, tt.args.gitlabClient, tt.args.cfg, tt.args.logger, testGitalySourceStorageName)

			p.assertTests(t, tt, res)
			if tt.assertCustom != nil {
				assert.Equal(t, tt.assertCustom, res)
			}
		})
	}
}

func TestFetchProjects_ContextCanceledPreventsCallingMoveRepository(t *testing.T) {
	stdout := &bytes.Buffer{}
	logger := setupLogger(stdout)

	c := &projectClient{listProjects: listProjectsReturnsProject}
	cfg := &DrainCfg{
		Project: RepositoryCfg{
			Concurrency: 1,
			MoveTimeout: 3 * time.Second,
		},
	}

	r := prometheus.NewRegistry()
	p := newProjectRepository(cfg, r)

	ctx, cancel := context.WithCancel(context.Background())
	cancel()

	err := p.moveProjects(ctx, c, cfg, logger, testGitalySourceStorageName)
	assert.EqualError(t, err, "context canceled")
	assert.Contains(t, stdout.String(), "event=\"context cancelled\"")
	assert.NotContains(t, stdout.String(), "event=\"move repository\"")
}

func TestIsSkippable(t *testing.T) {
	const (
		testProjectWebURL       = "https://gitlab.example.com/test-group/test-group-project-gofedo"
		testSkippableProject    = true
		testNonSkippableProject = false
	)

	tests := []projectTest{
		{
			name: "CfgSkipProjectReturnsTrue",
			args: projectTestArgs{
				project: &gitlab.Project{
					ID:     100,
					WebURL: testProjectWebURL,
				},
				cfg: &DrainCfg{Project: RepositoryCfg{SkipRepositories: []int{100}}},
			},
			assertMetrics: `
				# HELP gitalyctl_projects_skipped_repositories_total Number of repositories that have been skipped.
        	    # TYPE gitalyctl_projects_skipped_repositories_total counter
        	    gitalyctl_projects_skipped_repositories_total{reason="marked_skip"} 1
			`,
			assertCustom: testSkippableProject,
		},
		{
			name: "CfgSkipPooledRepositoriesReturnsTrue_ProjectHasForks",
			args: projectTestArgs{
				project: &gitlab.Project{
					ForksCount: 2,
					WebURL:     testProjectWebURL,
				},
				cfg: &DrainCfg{Project: RepositoryCfg{SkipPooledRepositories: true}},
			},
			assertMetrics: `
				# HELP gitalyctl_projects_skipped_repositories_total Number of repositories that have been skipped.
        	    # TYPE gitalyctl_projects_skipped_repositories_total counter
        	    gitalyctl_projects_skipped_repositories_total{reason="has_pooled_repository"} 1
			`,
			assertCustom: testSkippableProject,
		},
		{
			name: "CfgSkipPooledRepositoriesReturnsTrue_ProjectIsForked",
			args: projectTestArgs{
				project: &gitlab.Project{
					ForkedFromProject: &gitlab.ForkParent{},
					WebURL:            testProjectWebURL,
				},
				cfg: &DrainCfg{Project: RepositoryCfg{SkipPooledRepositories: true}},
			},
			assertMetrics: `
				# HELP gitalyctl_projects_skipped_repositories_total Number of repositories that have been skipped.
        	    # TYPE gitalyctl_projects_skipped_repositories_total counter
        	    gitalyctl_projects_skipped_repositories_total{reason="has_pooled_repository"} 1
			`,
			assertCustom: testSkippableProject,
		},
		{
			name: "NonSkippableProjectReturnsFalse",
			args: projectTestArgs{
				project: &gitlab.Project{WebURL: "https://gitlab.example.com/test-group/test-group-project-gofedo"},
				cfg:     &DrainCfg{},
			},
			assertMetrics: "",
			assertCustom:  testNonSkippableProject,
		},
	}

	for _, tt := range tests {
		tt.args.stdout = &bytes.Buffer{}
		tt.args.logger = setupLogger(tt.args.stdout)

		r := prometheus.NewRegistry()
		p := newProjectRepository(tt.args.cfg, r)

		t.Run(tt.name, func(t *testing.T) {
			res := p.isSkippable(tt.args.project, tt.args.cfg, tt.args.logger)

			p.assertTests(t, tt, nil)
			if tt.assertCustom != nil {
				assert.Equal(t, tt.assertCustom, res)
			}
		})
	}
}

func TestMoveProjectRepository(t *testing.T) {
	testProject := &gitlab.Project{
		ID:                1,
		WebURL:            "https://gitlab.example.com/test-group/test-group-project-gofedo",
		RepositoryStorage: testGitalySourceStorageName,
		Statistics: &gitlab.Statistics{
			RepositorySize: 100,
		},
	}
	testForkedProject := &gitlab.Project{
		ID:                2,
		WebURL:            "https://gitlab.example.com/test-group/test-group-project-gofedo-forked",
		RepositoryStorage: testGitalySourceStorageName,
		Statistics: &gitlab.Statistics{
			RepositorySize: 100,
		},
		ForkedFromProject: &gitlab.ForkParent{
			ID:                1,
			RepositoryStorage: testParentProjectRepositoryStorage,
		},
	}
	testCfg := &DrainCfg{Project: RepositoryCfg{MoveTimeout: time.Second}}

	tests := []projectTest{
		{
			name: "LogsDryRun",
			args: projectTestArgs{
				gitlabClient: &projectClient{scheduleStorageMoveForProject: scheduleStorageMoveForProjectIsCalled},
				project:      testProject,
				cfg:          &DrainCfg{DryRun: true},
			},
			assertError:   "",
			assertMetrics: "",
			assertCustomFunc: func(t *testing.T, args projectTestArgs) {
				assert.Equal(t, 0, scheduleProjectRepositoryStorageMoveCalledCount)
			},
		},
		{
			name: "ReturnsNilErrorOnSuccess",
			args: projectTestArgs{
				gitlabClient: &projectClient{
					scheduleStorageMoveForProject: scheduleStorageMoveForProjectReturnsSuccess,
					getStorageMoveForProject:      getStorageMoveWithStateFinished,
				},
				project: testProject,
				cfg:     testCfg,
			},
			assertError: "",
			assertMetrics: fmt.Sprintf(`
				# HELP gitalyctl_projects_active_repository_moves Number of repository moves in progress.
				# TYPE gitalyctl_projects_active_repository_moves gauge
				gitalyctl_projects_active_repository_moves{storage="%v"} 0
				# HELP gitalyctl_projects_successful_repository_moves_total Number of successful repository moves.
				# TYPE gitalyctl_projects_successful_repository_moves_total counter
				gitalyctl_projects_successful_repository_moves_total{storage="%[1]v"} 1
			`, testGitalySourceStorageName),
		},
		{
			name: "ForkedProjectIsMovedToRootProjectRepositoryStorage",
			args: projectTestArgs{
				gitlabClient: &projectClient{
					scheduleStorageMoveForProject: func(project int, opts gitlab.ScheduleStorageMoveForProjectOptions, options ...gitlab.RequestOptionFunc) (*gitlab.ProjectRepositoryStorageMove, *gitlab.Response, error) {
						require.NotNil(t, opts.DestinationStorageName)
						require.Equal(t, testForkedProject.ForkedFromProject.RepositoryStorage, *opts.DestinationStorageName)

						return scheduleStorageMoveForProjectReturnsSuccess(project, opts, options...)
					},
					getStorageMoveForProject: getFinishedStorageMoveForForkedProject,
					getSettings: func(options ...gitlab.RequestOptionFunc) (*gitlab.Settings, *gitlab.Response, error) {
						return &gitlab.Settings{
							RepositoryStoragesWeighted: map[string]int{
								testForkedProject.ForkedFromProject.RepositoryStorage: 50,
							},
						}, nil, nil
					},
				},
				project: testForkedProject,
				cfg:     testCfg,
			},
			assertError: "",
			assertMetrics: fmt.Sprintf(`
				# HELP gitalyctl_projects_active_repository_moves Number of repository moves in progress.
				# TYPE gitalyctl_projects_active_repository_moves gauge
				gitalyctl_projects_active_repository_moves{storage="%v"} 0
				# HELP gitalyctl_projects_successful_repository_moves_total Number of successful repository moves.
				# TYPE gitalyctl_projects_successful_repository_moves_total counter
				gitalyctl_projects_successful_repository_moves_total{storage="%[1]v"} 1
			`, testGitalySourceStorageName),
		},
		{
			name: "ForkedProjectIsNotMovedToRootProjectRepositoryStorageWeightZero",
			args: projectTestArgs{
				gitlabClient: &projectClient{
					scheduleStorageMoveForProject: func(project int, opts gitlab.ScheduleStorageMoveForProjectOptions, options ...gitlab.RequestOptionFunc) (*gitlab.ProjectRepositoryStorageMove, *gitlab.Response, error) {
						require.Nil(t, opts.DestinationStorageName)

						return scheduleStorageMoveForProjectReturnsSuccess(project, opts, options...)
					},
					getStorageMoveForProject: getFinishedStorageMoveForForkedProject,
					getSettings: func(options ...gitlab.RequestOptionFunc) (*gitlab.Settings, *gitlab.Response, error) {
						return &gitlab.Settings{
							RepositoryStoragesWeighted: map[string]int{
								testForkedProject.ForkedFromProject.RepositoryStorage: 0,
							},
						}, nil, nil
					},
				},
				project: testForkedProject,
				cfg:     testCfg,
			},
			assertError: "",
			assertMetrics: fmt.Sprintf(`
				# HELP gitalyctl_projects_active_repository_moves Number of repository moves in progress.
				# TYPE gitalyctl_projects_active_repository_moves gauge
				gitalyctl_projects_active_repository_moves{storage="%v"} 0
				# HELP gitalyctl_projects_successful_repository_moves_total Number of successful repository moves.
        	    # TYPE gitalyctl_projects_successful_repository_moves_total counter
        	    gitalyctl_projects_successful_repository_moves_total{storage="%[1]v"} 1
			`, testGitalySourceStorageName),
		},
		{
			name: "ReturnsErrorOnScheduleMoveFailure",
			args: projectTestArgs{
				gitlabClient: &projectClient{scheduleStorageMoveForProject: scheduleStorageMoveForProjectReturnsError},
				project:      testProject,
				cfg:          testCfg,
			},
			assertError: "retrieve project repository move: test error",
			assertMetrics: fmt.Sprintf(`
				# HELP gitalyctl_projects_failed_repository_moves_total Number of failed repository moves.
        	    # TYPE gitalyctl_projects_failed_repository_moves_total counter
        	    gitalyctl_projects_failed_repository_moves_total{storage="%v"} 1
			`, testGitalySourceStorageName),
		},
		{
			name: "ReturnsErrorWhenWaitForRepositoryMoveFails",
			args: projectTestArgs{
				gitlabClient: &projectClient{
					scheduleStorageMoveForProject: scheduleStorageMoveForProjectReturnsSuccess,
					getStorageMoveForProject:      getStorageMoveReturnsError,
				},
				project: testProject,
				cfg:     testCfg,
			},
			assertError: "wait for project repository move: retrieve project repository move: test error",
			assertMetrics: fmt.Sprintf(`
				# HELP gitalyctl_projects_active_repository_moves Number of repository moves in progress.
				# TYPE gitalyctl_projects_active_repository_moves gauge
				gitalyctl_projects_active_repository_moves{storage="%v"} 0
				# HELP gitalyctl_projects_failed_repository_moves_total Number of failed repository moves.
        	    # TYPE gitalyctl_projects_failed_repository_moves_total counter
        	    gitalyctl_projects_failed_repository_moves_total{storage="%[1]v"} 1
			`, testGitalySourceStorageName),
		},
		{
			name: "ReturnsErrorOnGetProjectRepositoryStorageMoveFailure",
			args: projectTestArgs{
				gitlabClient: &projectClient{
					scheduleStorageMoveForProject: scheduleStorageMoveForProjectReturnsSuccess,
					getStorageMoveForProject:      getStorageMoveReturnsError,
				},
				project: testProject,
				cfg:     testCfg,
			},
			assertError: "wait for project repository move: retrieve project repository move: test error",
			assertMetrics: fmt.Sprintf(`
				# HELP gitalyctl_projects_active_repository_moves Number of repository moves in progress.
				# TYPE gitalyctl_projects_active_repository_moves gauge
				gitalyctl_projects_active_repository_moves{storage="%v"} 0
				# HELP gitalyctl_projects_failed_repository_moves_total Number of failed repository moves.
        	    # TYPE gitalyctl_projects_failed_repository_moves_total counter
        	    gitalyctl_projects_failed_repository_moves_total{storage="%[1]v"} 1
			`, testGitalySourceStorageName),
		},
		{
			name: "LogsReadOnlyWhenRepositoryIsReadOnly",
			args: projectTestArgs{
				gitlabClient: &projectClient{
					scheduleStorageMoveForProject:     scheduleStorageMoveForProjectReturnsReadOnlyError,
					retrieveAllStorageMovesForProject: retrieveAllStorageMovesForProjectReturnsNoActiveMoves,
				},
				project: testProject,
				cfg:     testCfg,
			},
			assertError: "",
			assertMetrics: fmt.Sprintf(`
				# HELP gitalyctl_projects_read_only_repositories_total Number of read-only repositories.
        	    # TYPE gitalyctl_projects_read_only_repositories_total counter
        	    gitalyctl_projects_read_only_repositories_total{storage="%v"} 1
			`, testGitalySourceStorageName),
		},
	}

	for _, tt := range tests {
		ctx := context.Background()

		tt.args.stdout = &bytes.Buffer{}
		tt.args.logger = setupLogger(tt.args.stdout)

		r := prometheus.NewRegistry()
		p := newProjectRepository(tt.args.cfg, r)

		t.Run(tt.name, func(t *testing.T) {
			err := p.moveProjectRepository(ctx, tt.args.gitlabClient, tt.args.project, tt.args.cfg, tt.args.logger)

			p.assertTests(t, tt, err)
			if tt.assertCustomFunc != nil {
				tt.assertCustomFunc(t, tt.args)
			}
		})
	}
}

func TestCheckProjectRepositoryMoveError(t *testing.T) {
	testProject := &gitlab.Project{
		WebURL: "https://gitlab.example.com/test-group/test-group-project-gofedo",
		Statistics: &gitlab.Statistics{
			RepositorySize: 1,
		},
	}
	testCfg := &DrainCfg{Project: RepositoryCfg{MoveTimeout: 3 * time.Second}}

	tests := []projectTest{
		{
			name: "ReturnsErrorWhenReadOnlyNotFoundInError",
			args: projectTestArgs{
				gitlabClient: &projectClient{},
				project:      &gitlab.Project{},
				cfg:          testCfg,
				err:          fmt.Errorf("not read-only"),
			},
			assertError:  "retrieve project repository move: not read-only",
			assertCustom: testFailedID,
		},
		{
			name: "ReturnsErrorWhenRetrievingMovesFails",
			args: projectTestArgs{
				gitlabClient: &projectClient{retrieveAllStorageMovesForProject: retrieveAllStorageMovesForProjectReturnsError},
				project:      testProject,
				cfg:          testCfg,
				err:          fmt.Errorf("is read-only"),
			},
			assertError:  "retrieve project repository move: check project repository move status: test error",
			assertCustom: testFailedID,
		},
		{
			name: "ReturnsEmptyMoveWhenNoActiveMovesFound",
			args: projectTestArgs{
				gitlabClient: &projectClient{retrieveAllStorageMovesForProject: retrieveAllStorageMovesForProjectReturnsNoActiveMoves},
				project:      testProject,
				cfg:          testCfg,
				err:          fmt.Errorf("is read-only"),
			},
			assertError:  "",
			assertCustom: testFailedID,
		},
		{
			name: "ReturnsMoveWhenActiveMoveFound",
			args: projectTestArgs{
				gitlabClient: &projectClient{retrieveAllStorageMovesForProject: retrieveAllStorageMovesForProjectReturnsScheduledMove},
				project:      testProject,
				cfg:          &DrainCfg{Project: RepositoryCfg{MoveTimeout: time.Second}},
				err:          fmt.Errorf("is read-only"),
			},
			assertError:  "",
			assertCustom: testSuccessfulID,
		},
	}

	for _, tt := range tests {
		tt.args.stdout = &bytes.Buffer{}
		tt.args.logger = setupLogger(tt.args.stdout)

		r := prometheus.NewRegistry()
		p := newProjectRepository(tt.args.cfg, r)

		t.Run(tt.name, func(t *testing.T) {
			psm, err := p.checkProjectRepositoryMoveError(tt.args.gitlabClient, tt.args.project, tt.args.logger, tt.args.err)

			p.assertTests(t, tt, err)
			if tt.assertCustom != nil {
				assert.Equal(t, psm.ID, tt.assertCustom)
			}
		})
	}
}

func TestWaitForProjectRepositoryMove(t *testing.T) {
	testProject := &gitlab.Project{
		WebURL: "https://gitlab.example.com/test-group/test-group-project-gofedo",
		Statistics: &gitlab.Statistics{
			RepositorySize: 1,
		},
	}
	testCfg := &DrainCfg{Project: RepositoryCfg{MoveTimeout: 10 * time.Second}}

	tests := []projectTest{
		{
			name: "ReturnsNilErrorForFinishedMove",
			args: projectTestArgs{
				gitlabClient:   &projectClient{getStorageMoveForProject: getStorageMoveWithStateFinished},
				project:        testProject,
				repositoryMove: &gitlab.ProjectRepositoryStorageMove{ID: 1, SourceStorageName: testGitalySourceStorageName},
				cfg:            testCfg,
			},
			assertError: "",
			assertMetrics: fmt.Sprintf(`
				# HELP gitalyctl_projects_active_repository_moves Number of repository moves in progress.
				# TYPE gitalyctl_projects_active_repository_moves gauge
				gitalyctl_projects_active_repository_moves{storage="%v"} 0
				# HELP gitalyctl_projects_successful_repository_moves_total Number of successful repository moves.
				# TYPE gitalyctl_projects_successful_repository_moves_total counter
				gitalyctl_projects_successful_repository_moves_total{storage="%[1]v"} 1
			`, testGitalySourceStorageName),
		},
		{
			name: "ReturnsNilErrorForFailedMove",
			args: projectTestArgs{
				gitlabClient:   &projectClient{getStorageMoveForProject: getStorageMoveWithStateFailed},
				project:        testProject,
				repositoryMove: &gitlab.ProjectRepositoryStorageMove{ID: 1, SourceStorageName: testGitalySourceStorageName},
				cfg:            testCfg,
			},
			assertError: "",
			assertMetrics: fmt.Sprintf(`
				# HELP gitalyctl_projects_active_repository_moves Number of repository moves in progress.
				# TYPE gitalyctl_projects_active_repository_moves gauge
				gitalyctl_projects_active_repository_moves{storage="%v"} 0
				# HELP gitalyctl_projects_failed_repository_moves_total Number of failed repository moves.
        	    # TYPE gitalyctl_projects_failed_repository_moves_total counter
        	    gitalyctl_projects_failed_repository_moves_total{storage="%[1]v"} 1
			`, testGitalySourceStorageName),
		},
		{
			name: "ReturnsNilErrorForCleanupFailedMove",
			args: projectTestArgs{
				gitlabClient:   &projectClient{getStorageMoveForProject: getStorageMoveWithStateCleanupFailed},
				project:        testProject,
				repositoryMove: &gitlab.ProjectRepositoryStorageMove{ID: 1, SourceStorageName: testGitalySourceStorageName},
				cfg:            testCfg,
			},
			assertError: "",
			assertMetrics: fmt.Sprintf(`
				# HELP gitalyctl_projects_active_repository_moves Number of repository moves in progress.
				# TYPE gitalyctl_projects_active_repository_moves gauge
				gitalyctl_projects_active_repository_moves{storage="%v"} 0
				# HELP gitalyctl_projects_failed_repository_moves_total Number of failed repository moves.
        	    # TYPE gitalyctl_projects_failed_repository_moves_total counter
        	    gitalyctl_projects_failed_repository_moves_total{storage="%[1]v"} 1
			`, testGitalySourceStorageName),
		},
		{
			name: "GetStorageMoveReturnsError",
			args: projectTestArgs{
				gitlabClient:   &projectClient{getStorageMoveForProject: getStorageMoveReturnsError},
				project:        testProject,
				repositoryMove: &gitlab.ProjectRepositoryStorageMove{ID: 1, SourceStorageName: testGitalySourceStorageName},
				cfg:            testCfg,
			},
			assertError: "retrieve project repository move: test error",
			assertMetrics: fmt.Sprintf(`
				# HELP gitalyctl_projects_active_repository_moves Number of repository moves in progress.
				# TYPE gitalyctl_projects_active_repository_moves gauge
				gitalyctl_projects_active_repository_moves{storage="%v"} 0
				# HELP gitalyctl_projects_failed_repository_moves_total Number of failed repository moves.
        	    # TYPE gitalyctl_projects_failed_repository_moves_total counter
				gitalyctl_projects_failed_repository_moves_total{storage="%[1]v"} 1
			`, testGitalySourceStorageName),
		},
		{
			name: "LogsErrorOnTimeout",
			args: projectTestArgs{
				gitlabClient: &projectClient{getStorageMoveForProject: getStorageMoveWithStateStarted},
				project:      testProject,
				repositoryMove: &gitlab.ProjectRepositoryStorageMove{
					ID:                     1,
					SourceStorageName:      testGitalySourceStorageName,
					DestinationStorageName: testGitalyDestinationStorageName,
				},
				cfg: &DrainCfg{Project: RepositoryCfg{MoveTimeout: time.Second}},
			},
			assertError: "",
			assertMetrics: fmt.Sprintf(`
				# HELP gitalyctl_projects_active_repository_moves Number of repository moves in progress.
				# TYPE gitalyctl_projects_active_repository_moves gauge
				gitalyctl_projects_active_repository_moves{storage="%v"} 0
				# HELP gitalyctl_projects_timed_out_repository_moves_total Number of timed out repository moves.
        	    # TYPE gitalyctl_projects_timed_out_repository_moves_total counter
        	    gitalyctl_projects_timed_out_repository_moves_total{storage="%[1]v"} 1
			`, testGitalySourceStorageName),
		},
	}

	for _, tt := range tests {
		tt.args.stdout = &bytes.Buffer{}
		tt.args.logger = setupLogger(tt.args.stdout)

		r := prometheus.NewRegistry()
		p := newProjectRepository(tt.args.cfg, r)

		t.Run(tt.name, func(t *testing.T) {
			err := p.waitForProjectRepositoryMove(tt.args.gitlabClient, tt.args.repositoryMove, tt.args.project, tt.args.cfg, tt.args.logger)
			p.assertTests(t, tt, err)
		})
	}
}

func TestGetProjectRepository(t *testing.T) {
	testProject := &gitlab.Project{
		WebURL: "https://gitlab.example.com/test-group/test-group-project-gofedo",
		Statistics: &gitlab.Statistics{
			RepositorySize: 1,
		},
	}
	testCfg := &DrainCfg{
		Storage: StorageCfg{Concurrency: 1},
		Project: RepositoryCfg{
			Concurrency:      1,
			MoveTimeout:      3 * time.Second,
			MoveStatusUpdate: time.Second,
		},
	}

	tests := []projectTest{
		{
			name: "ReturnsErrorOnFailure",
			args: projectTestArgs{
				gitlabClient: &projectClient{retrieveAllStorageMovesForProject: retrieveAllStorageMovesForProjectReturnsError},
				project:      testProject,
				cfg:          testCfg,
			},
			assertError:  "check project repository move status: test error",
			assertCustom: testFailedID,
		},
		{
			name: "ReturnsNothingWhenNoActiveMovesFound",
			args: projectTestArgs{
				gitlabClient: &projectClient{retrieveAllStorageMovesForProject: retrieveAllStorageMovesForProjectReturnsNoActiveMoves},
				project:      testProject,
				cfg:          testCfg,
			},
			assertError:  "",
			assertCustom: testFailedID,
		},
		{
			name: "ReturnsMoveWhenInitialMoveFound",
			args: projectTestArgs{
				gitlabClient: &projectClient{retrieveAllStorageMovesForProject: retrieveAllStorageMovesForProjectReturnsInitialMove},
				project:      testProject,
				cfg:          testCfg,
			},
			assertError:  "",
			assertCustom: testSuccessfulID,
		},
		{
			name: "ReturnsMoveWhenScheduledMoveFound",
			args: projectTestArgs{
				gitlabClient: &projectClient{retrieveAllStorageMovesForProject: retrieveAllStorageMovesForProjectReturnsScheduledMove},
				project:      testProject,
				cfg:          testCfg,
			},
			assertError:  "",
			assertCustom: testSuccessfulID,
		},
		{
			name: "ReturnsMoveWhenStartedMoveFound",
			args: projectTestArgs{
				gitlabClient: &projectClient{retrieveAllStorageMovesForProject: retrieveAllStorageMovesForProjectReturnsStartedMove},
				project:      testProject,
				cfg:          testCfg,
			},
			assertError:  "",
			assertCustom: testSuccessfulID,
		},
		{
			name: "ReturnsMoveWhenReplicatedMoveFound",
			args: projectTestArgs{
				gitlabClient: &projectClient{retrieveAllStorageMovesForProject: retrieveAllStorageMovesForProjectReturnsReplicatedMove},
				project:      testProject,
				cfg:          testCfg,
			},
			assertError:  "",
			assertCustom: testSuccessfulID,
		},
		{
			name: "ReturnsNothingWhenFailedMoveFound",
			args: projectTestArgs{
				gitlabClient: &projectClient{retrieveAllStorageMovesForProject: retrieveAllStorageMovesForProjectReturnsFailedMove},
				project:      testProject,
				cfg:          testCfg,
			},
			assertError:  "",
			assertCustom: testFailedID,
		},
		{
			name: "ReturnsNothingWhenFinishedMoveFound",
			args: projectTestArgs{
				gitlabClient: &projectClient{retrieveAllStorageMovesForProject: retrieveAllStorageMovesForProjectReturnsFinishedMove},
				project:      testProject,
				cfg:          testCfg,
			},
			assertError:  "",
			assertCustom: testFailedID,
		},
		{
			name: "ReturnsNothingWhenCleanupFailedMoveFound",
			args: projectTestArgs{
				gitlabClient: &projectClient{retrieveAllStorageMovesForProject: retrieveAllStorageMovesForProjectReturnsCleanupFailedMove},
				project:      testProject,
				cfg:          testCfg,
			},
			assertError:  "",
			assertCustom: testFailedID,
		},
	}

	for _, tt := range tests {
		tt.args.stdout = &bytes.Buffer{}
		tt.args.logger = setupLogger(tt.args.stdout)

		r := prometheus.NewRegistry()
		p := newProjectRepository(tt.args.cfg, r)

		t.Run(tt.name, func(t *testing.T) {
			res, err := p.getProjectRepositoryMoves(tt.args.gitlabClient, tt.args.project, tt.args.logger)

			p.assertTests(t, tt, err)
			if tt.assertCustom != nil {
				assert.Equal(t, res.ID, tt.assertCustom)
			}
		})
	}
}

func TestProjectRepositoryPagination(t *testing.T) {
	pagesCount := 0

	gitLabClient := &projectClient{listProjects: func(opt *gitlab.ListProjectsOptions, options ...gitlab.RequestOptionFunc) ([]*gitlab.Project, *gitlab.Response, error) {
		require.Equal(t, *opt.RepositoryStorage, "storage1")
		require.Equal(t, *&opt.PerPage, apiResultsPerPage)

		pagesCount++

		nextLink := fmt.Sprintf("https://example.com/api/v4/projects?id_after=%d", pagesCount)
		if pagesCount == 4 { // We've reached the end.
			nextLink = ""
		}

		resp := gitlab.Response{
			Response: &http.Response{},
			NextLink: nextLink,
		}

		return nil, &resp, nil
	}}

	cfg := &DrainCfg{
		Storage: StorageCfg{Concurrency: 1},
		Project: RepositoryCfg{
			Concurrency:      1,
			MoveTimeout:      3 * time.Second,
			MoveStatusUpdate: time.Second,
		},
	}

	ctx := context.Background()
	p := newProjectRepository(cfg, nil)

	require.NoError(t, p.moveProjects(ctx, gitLabClient, cfg, nil, "storage1"))
	require.Equal(t, pagesCount, 4)
}

func TestDestinationStorageName(t *testing.T) {
	tests := []struct {
		name           string
		project        gitlab.Project
		gitlabClient   projectClient
		cfg            DrainCfg
		wantDstStorage string
	}{
		{
			name: "single repository",
			project: gitlab.Project{
				ForkedFromProject: nil, // Not part of a fork network
			},
			wantDstStorage: "",
		},
		{
			name: "forked repository destination has weight",
			project: gitlab.Project{
				ForkedFromProject: &gitlab.ForkParent{
					RepositoryStorage: "fork-destination-storage",
				},
			},
			gitlabClient: projectClient{
				getSettings: func(options ...gitlab.RequestOptionFunc) (*gitlab.Settings, *gitlab.Response, error) {
					return &gitlab.Settings{
						RepositoryStoragesWeighted: map[string]int{
							"fork-destination-storage": 50,
						},
					}, nil, nil
				},
			},
			wantDstStorage: "fork-destination-storage",
		},
		{
			name: "forked repository destination has no weight",
			project: gitlab.Project{
				ForkedFromProject: &gitlab.ForkParent{
					RepositoryStorage: "fork-destination-storage",
				},
			},
			gitlabClient: projectClient{
				getSettings: func(options ...gitlab.RequestOptionFunc) (*gitlab.Settings, *gitlab.Response, error) {
					return &gitlab.Settings{
						RepositoryStoragesWeighted: map[string]int{
							"fork-destination-storage": 0,
						},
					}, nil, nil
				},
			},
			wantDstStorage: "",
		},
		{
			name: "forked repository destination does not exist in settings",
			project: gitlab.Project{
				ForkedFromProject: &gitlab.ForkParent{
					RepositoryStorage: "non-existant",
				},
			},
			gitlabClient: projectClient{
				getSettings: func(options ...gitlab.RequestOptionFunc) (*gitlab.Settings, *gitlab.Response, error) {
					return &gitlab.Settings{
						RepositoryStoragesWeighted: map[string]int{
							"fork-destination-storage": 100,
						},
					}, nil, nil
				},
			},
			wantDstStorage: "",
		},
		{
			name: "destination_storage is configured",
			cfg: DrainCfg{
				Project: RepositoryCfg{
					DestinationStorage: "new-gitaly-node",
				},
			},
			wantDstStorage: "new-gitaly-node",
		},
	}

	for _, tt := range tests {
		t.Run(tt.name, func(t *testing.T) {
			gotDestStorage, err := destinationStorageName(context.Background(), log.NewNopLogger(), &tt.cfg, &tt.gitlabClient, &tt.project)
			require.NoError(t, err)
			require.Equal(t, tt.wantDstStorage, gotDestStorage)
		})
	}
}

func (p *projectRepository) assertTests(t *testing.T, tt projectTest, err error) {
	if tt.assertError == "" {
		require.NoError(t, err)
	} else {
		assert.EqualError(t, err, tt.assertError)
	}
	require.NoError(t, testutil.CollectAndCompare(p, bytes.NewBufferString(tt.assertMetrics)))
}

func listProjectsReturnsProject(opt *gitlab.ListProjectsOptions, options ...gitlab.RequestOptionFunc) ([]*gitlab.Project, *gitlab.Response, error) {
	p := []*gitlab.Project{
		{
			ID:                1,
			WebURL:            "https://gitlab.example.com/test-group/test-group-project-gofedo",
			RepositoryStorage: testGitalySourceStorageName,
			Statistics: &gitlab.Statistics{
				RepositorySize: 1,
			},
		},
	}
	return p, nil, nil
}

func listProjectsReturnsError(opt *gitlab.ListProjectsOptions, options ...gitlab.RequestOptionFunc) ([]*gitlab.Project, *gitlab.Response, error) {
	return []*gitlab.Project{}, nil, fmt.Errorf("test error")
}

func scheduleStorageMoveForProjectIsCalled(project int, opts gitlab.ScheduleStorageMoveForProjectOptions, options ...gitlab.RequestOptionFunc) (*gitlab.ProjectRepositoryStorageMove, *gitlab.Response, error) {
	scheduleProjectRepositoryStorageMoveCalledCount++
	return nil, nil, nil
}

func scheduleStorageMoveForProjectReturnsSuccess(project int, opts gitlab.ScheduleStorageMoveForProjectOptions, options ...gitlab.RequestOptionFunc) (*gitlab.ProjectRepositoryStorageMove, *gitlab.Response, error) {
	return &gitlab.ProjectRepositoryStorageMove{
		ID:                     1,
		CreatedAt:              gitlab.Time(time.Now()),
		State:                  repositoryMoveScheduled,
		SourceStorageName:      testGitalySourceStorageName,
		DestinationStorageName: testGitalyDestinationStorageName,
	}, nil, nil
}

func scheduleStorageMoveForProjectReturnsReadOnlyError(project int, opts gitlab.ScheduleStorageMoveForProjectOptions, options ...gitlab.RequestOptionFunc) (*gitlab.ProjectRepositoryStorageMove, *gitlab.Response, error) {
	return &gitlab.ProjectRepositoryStorageMove{}, nil, fmt.Errorf("is read-only")
}

func scheduleStorageMoveForProjectReturnsError(project int, opts gitlab.ScheduleStorageMoveForProjectOptions, options ...gitlab.RequestOptionFunc) (*gitlab.ProjectRepositoryStorageMove, *gitlab.Response, error) {
	return nil, nil, fmt.Errorf("test error")
}

func getStorageMoveWithStateFinished(repositoryStorage int, options ...gitlab.RequestOptionFunc) (*gitlab.ProjectRepositoryStorageMove, *gitlab.Response, error) {
	return &gitlab.ProjectRepositoryStorageMove{
		ID:                     1,
		CreatedAt:              gitlab.Time(time.Now()),
		State:                  repositoryMoveFinished,
		SourceStorageName:      testGitalySourceStorageName,
		DestinationStorageName: testGitalyDestinationStorageName,
	}, nil, nil
}

func getFinishedStorageMoveForForkedProject(repositoryStorage int, options ...gitlab.RequestOptionFunc) (*gitlab.ProjectRepositoryStorageMove, *gitlab.Response, error) {
	return &gitlab.ProjectRepositoryStorageMove{
		ID:                     1,
		CreatedAt:              gitlab.Time(time.Now()),
		State:                  repositoryMoveFinished,
		SourceStorageName:      testGitalySourceStorageName,
		DestinationStorageName: testParentProjectRepositoryStorage,
	}, nil, nil
}

func getStorageMoveWithStateFailed(repositoryStorage int, options ...gitlab.RequestOptionFunc) (*gitlab.ProjectRepositoryStorageMove, *gitlab.Response, error) {
	return &gitlab.ProjectRepositoryStorageMove{
		ID:                     1,
		CreatedAt:              gitlab.Time(time.Now()),
		State:                  repositoryMoveFailed,
		SourceStorageName:      testGitalySourceStorageName,
		DestinationStorageName: testGitalyDestinationStorageName,
	}, nil, nil
}

func getStorageMoveWithStateCleanupFailed(repositoryStorage int, options ...gitlab.RequestOptionFunc) (*gitlab.ProjectRepositoryStorageMove, *gitlab.Response, error) {
	return &gitlab.ProjectRepositoryStorageMove{
		ID:                     1,
		CreatedAt:              gitlab.Time(time.Now()),
		State:                  repositoryMoveCleanupFailed,
		SourceStorageName:      testGitalySourceStorageName,
		DestinationStorageName: testGitalyDestinationStorageName,
	}, nil, nil
}

func getStorageMoveWithStateStarted(repositoryStorage int, options ...gitlab.RequestOptionFunc) (*gitlab.ProjectRepositoryStorageMove, *gitlab.Response, error) {
	return &gitlab.ProjectRepositoryStorageMove{
		ID:                     1,
		CreatedAt:              gitlab.Time(time.Now()),
		State:                  repositoryMoveStarted,
		SourceStorageName:      testGitalySourceStorageName,
		DestinationStorageName: testGitalyDestinationStorageName,
	}, nil, nil
}

func getStorageMoveReturnsError(repositoryStorage int, options ...gitlab.RequestOptionFunc) (*gitlab.ProjectRepositoryStorageMove, *gitlab.Response, error) {
	return nil, nil, fmt.Errorf("test error")
}

func retrieveAllStorageMovesForProjectReturnsError(project int, opts gitlab.RetrieveAllProjectStorageMovesOptions, options ...gitlab.RequestOptionFunc) ([]*gitlab.ProjectRepositoryStorageMove, *gitlab.Response, error) {
	return nil, nil, fmt.Errorf("test error")
}

func retrieveAllStorageMovesForProjectReturnsNoActiveMoves(project int, opts gitlab.RetrieveAllProjectStorageMovesOptions, options ...gitlab.RequestOptionFunc) ([]*gitlab.ProjectRepositoryStorageMove, *gitlab.Response, error) {
	return []*gitlab.ProjectRepositoryStorageMove{}, nil, nil
}

func retrieveAllStorageMovesForProjectReturnsInitialMove(project int, opts gitlab.RetrieveAllProjectStorageMovesOptions, options ...gitlab.RequestOptionFunc) ([]*gitlab.ProjectRepositoryStorageMove, *gitlab.Response, error) {
	return []*gitlab.ProjectRepositoryStorageMove{
		{
			ID:                     1,
			CreatedAt:              gitlab.Time(time.Now()),
			State:                  repositoryMoveInitial,
			SourceStorageName:      testGitalySourceStorageName,
			DestinationStorageName: testGitalyDestinationStorageName,
		},
	}, nil, nil
}

func retrieveAllStorageMovesForProjectReturnsScheduledMove(project int, opts gitlab.RetrieveAllProjectStorageMovesOptions, options ...gitlab.RequestOptionFunc) ([]*gitlab.ProjectRepositoryStorageMove, *gitlab.Response, error) {
	return []*gitlab.ProjectRepositoryStorageMove{
		{
			ID:                     1,
			CreatedAt:              gitlab.Time(time.Now()),
			State:                  repositoryMoveScheduled,
			SourceStorageName:      testGitalySourceStorageName,
			DestinationStorageName: testGitalyDestinationStorageName,
		},
	}, nil, nil
}

func retrieveAllStorageMovesForProjectReturnsStartedMove(project int, opts gitlab.RetrieveAllProjectStorageMovesOptions, options ...gitlab.RequestOptionFunc) ([]*gitlab.ProjectRepositoryStorageMove, *gitlab.Response, error) {
	return []*gitlab.ProjectRepositoryStorageMove{
		{
			ID:                     1,
			CreatedAt:              gitlab.Time(time.Now()),
			State:                  repositoryMoveStarted,
			SourceStorageName:      testGitalySourceStorageName,
			DestinationStorageName: testGitalyDestinationStorageName,
		},
	}, nil, nil
}

func retrieveAllStorageMovesForProjectReturnsReplicatedMove(project int, opts gitlab.RetrieveAllProjectStorageMovesOptions, options ...gitlab.RequestOptionFunc) ([]*gitlab.ProjectRepositoryStorageMove, *gitlab.Response, error) {
	return []*gitlab.ProjectRepositoryStorageMove{
		{
			ID:                     1,
			CreatedAt:              gitlab.Time(time.Now()),
			State:                  repositoryMoveReplicated,
			SourceStorageName:      testGitalySourceStorageName,
			DestinationStorageName: testGitalyDestinationStorageName,
		},
	}, nil, nil
}

func retrieveAllStorageMovesForProjectReturnsFailedMove(project int, opts gitlab.RetrieveAllProjectStorageMovesOptions, options ...gitlab.RequestOptionFunc) ([]*gitlab.ProjectRepositoryStorageMove, *gitlab.Response, error) {
	return []*gitlab.ProjectRepositoryStorageMove{
		{
			ID:                     1,
			CreatedAt:              gitlab.Time(time.Now()),
			State:                  repositoryMoveFailed,
			SourceStorageName:      testGitalySourceStorageName,
			DestinationStorageName: testGitalyDestinationStorageName,
		},
	}, nil, nil
}

func retrieveAllStorageMovesForProjectReturnsFinishedMove(project int, opts gitlab.RetrieveAllProjectStorageMovesOptions, options ...gitlab.RequestOptionFunc) ([]*gitlab.ProjectRepositoryStorageMove, *gitlab.Response, error) {
	return []*gitlab.ProjectRepositoryStorageMove{
		{
			ID:                     1,
			CreatedAt:              gitlab.Time(time.Now()),
			State:                  repositoryMoveFinished,
			SourceStorageName:      testGitalySourceStorageName,
			DestinationStorageName: testGitalyDestinationStorageName,
		},
	}, nil, nil
}

func retrieveAllStorageMovesForProjectReturnsCleanupFailedMove(project int, opts gitlab.RetrieveAllProjectStorageMovesOptions, options ...gitlab.RequestOptionFunc) ([]*gitlab.ProjectRepositoryStorageMove, *gitlab.Response, error) {
	return []*gitlab.ProjectRepositoryStorageMove{
		{
			ID:                     1,
			CreatedAt:              gitlab.Time(time.Now()),
			State:                  repositoryMoveCleanupFailed,
			SourceStorageName:      testGitalySourceStorageName,
			DestinationStorageName: testGitalyDestinationStorageName,
		},
	}, nil, nil
}
