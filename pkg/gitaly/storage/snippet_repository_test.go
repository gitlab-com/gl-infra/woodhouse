package storage

import (
	"bytes"
	"context"
	"fmt"
	"net/http"
	"testing"
	"time"

	"github.com/go-kit/log"
	"github.com/prometheus/client_golang/prometheus"
	"github.com/prometheus/client_golang/prometheus/testutil"
	"github.com/stretchr/testify/assert"
	"github.com/stretchr/testify/require"
	"github.com/xanzy/go-gitlab"
)

type snippetTestArgs struct {
	gitlabClient   *snippetClient
	snippet        *gitlab.Snippet
	repositoryMove *gitlab.SnippetRepositoryStorageMove
	cfg            *DrainCfg
	logger         log.Logger
	stdout         *bytes.Buffer
	err            error
}
type snippetTest struct {
	name             string
	args             snippetTestArgs
	assertCustomFunc func(t *testing.T, args snippetTestArgs)
	assertCustom     interface{}
	assertError      string
	assertMetrics    string
}

var scheduleSnippetRepositoryStorageMoveCalledCount = 0

func TestMoveSnippets(t *testing.T) {
	tests := []snippetTest{
		{
			name: "ReturnsNilErrorOnSuccess",
			args: snippetTestArgs{
				gitlabClient: &snippetClient{
					listAllSnippets:               listAllSnippetsReturnsSnippet,
					scheduleStorageMoveForSnippet: scheduleStorageMoveForSnippetReturnsSuccess,
					getStorageMoveForSnippet:      getSnippetStorageMoveWithStateFinished,
				},
				cfg: &DrainCfg{
					Snippet: RepositoryCfg{
						Concurrency: 1,
						MoveTimeout: 3 * time.Second,
					},
				},
			},
			assertError: "",
			assertMetrics: fmt.Sprintf(`
				# HELP gitalyctl_snippets_active_repository_moves Number of repository moves in progress.
				# TYPE gitalyctl_snippets_active_repository_moves gauge
				gitalyctl_snippets_active_repository_moves{storage="%v"} 0
				# HELP gitalyctl_snippets_current_page Current page number for the API results.
				# TYPE gitalyctl_snippets_current_page gauge
				gitalyctl_snippets_current_page{storage="%[1]v"} 0
				# HELP gitalyctl_snippets_successful_repository_moves_total Number of successful repository moves.
				# TYPE gitalyctl_snippets_successful_repository_moves_total counter
				gitalyctl_snippets_successful_repository_moves_total{storage="%[1]v"} 1
			`, testGitalySourceStorageName),
		},
		{
			name: "ErrorsWhenListSnippetsReturnsError",
			args: snippetTestArgs{
				gitlabClient: &snippetClient{
					listAllSnippets: listAllSnippetsReturnsError,
				},
				cfg: &DrainCfg{
					Snippet: RepositoryCfg{
						Concurrency: 1,
						MoveTimeout: 3 * time.Second,
					},
				},
			},
			assertError: "list snippets: test error",
			assertMetrics: fmt.Sprintf(`
				# HELP gitalyctl_snippets_current_page Current page number for the API results.
        	    # TYPE gitalyctl_snippets_current_page gauge
        	    gitalyctl_snippets_current_page{storage="%v"} 1
			`, testGitalySourceStorageName),
		},
	}
	ctx := context.Background()

	for _, tt := range tests {
		tt.args.stdout = &bytes.Buffer{}
		tt.args.logger = setupLogger(tt.args.stdout)

		r := prometheus.NewRegistry()
		s := newSnippetRepository(tt.args.cfg, r)

		t.Run(tt.name, func(t *testing.T) {
			res := s.moveSnippets(ctx, tt.args.gitlabClient, tt.args.cfg, tt.args.logger, testGitalySourceStorageName)

			s.assertTests(t, tt, res)
			if tt.assertCustom != nil {
				assert.Equal(t, tt.assertCustom, res)
			}
		})
	}
}

func TestFetchSnippets_ContextCanceledPreventsCallingMoveRepository(t *testing.T) {
	stdout := &bytes.Buffer{}
	logger := setupLogger(stdout)

	c := &snippetClient{listAllSnippets: listAllSnippetsReturnsSnippet}
	cfg := &DrainCfg{
		Snippet: RepositoryCfg{
			Concurrency: 1,
			MoveTimeout: 3 * time.Second,
		},
	}

	r := prometheus.NewRegistry()
	s := newSnippetRepository(cfg, r)

	ctx, cancel := context.WithCancel(context.Background())
	cancel()

	err := s.moveSnippets(ctx, c, cfg, logger, testGitalySourceStorageName)
	assert.EqualError(t, err, "context canceled")
	assert.Contains(t, stdout.String(), "event=\"context cancelled\"")
	assert.NotContains(t, stdout.String(), "event=\"move repository\"")
}

func TestSnippetIsSkippable(t *testing.T) {
	const (
		testSnippetWebURL       = "https://gitlab.example.com/test-group/test-group-snippet-gofedo"
		testSkippableSnippet    = true
		testNonSkippableSnippet = false
	)

	tests := []snippetTest{
		{
			name: "CfgSkipSnippetReturnsTrue",
			args: snippetTestArgs{
				snippet: &gitlab.Snippet{
					ID:     100,
					WebURL: testSnippetWebURL,
				},
				cfg: &DrainCfg{Snippet: RepositoryCfg{SkipRepositories: []int{100}}},
			},
			assertMetrics: `
				# HELP gitalyctl_snippets_skipped_repositories_total Number of repositories that have been skipped.
        	    # TYPE gitalyctl_snippets_skipped_repositories_total counter
        	    gitalyctl_snippets_skipped_repositories_total{reason="marked_skip"} 1
			`,
			assertCustom: testSkippableSnippet,
		},
		{
			name: "NonSkippableSnippetReturnsFalse",
			args: snippetTestArgs{
				snippet: &gitlab.Snippet{WebURL: "https://gitlab.example.com/test-group/test-group-snippet-gofedo"},
				cfg:     &DrainCfg{},
			},
			assertMetrics: "",
			assertCustom:  testNonSkippableSnippet,
		},
	}

	for _, tt := range tests {
		tt.args.stdout = &bytes.Buffer{}
		tt.args.logger = setupLogger(tt.args.stdout)

		r := prometheus.NewRegistry()
		s := newSnippetRepository(tt.args.cfg, r)

		t.Run(tt.name, func(t *testing.T) {
			res := s.isSkippable(tt.args.snippet, tt.args.cfg, tt.args.logger)

			s.assertTests(t, tt, nil)
			if tt.assertCustom != nil {
				assert.Equal(t, tt.assertCustom, res)
			}
		})
	}
}

func TestMoveSnippetRepository(t *testing.T) {
	testSnippet := &gitlab.Snippet{
		WebURL:            "https://gitlab.example.com/test-group/test-group-snippet-gofedo",
		RepositoryStorage: testGitalySourceStorageName,
	}
	testCfg := &DrainCfg{Snippet: RepositoryCfg{MoveTimeout: time.Second}}

	tests := []snippetTest{
		{
			name: "LogsDryRun",
			args: snippetTestArgs{
				gitlabClient: &snippetClient{scheduleStorageMoveForSnippet: scheduleStorageMoveForSnippetIsCalled},
				snippet:      testSnippet,
				cfg:          &DrainCfg{DryRun: true},
			},
			assertError:   "",
			assertMetrics: "",
			assertCustomFunc: func(t *testing.T, args snippetTestArgs) {
				assert.Equal(t, 0, scheduleSnippetRepositoryStorageMoveCalledCount)
			},
		},
		{
			name: "ReturnsNilErrorOnSuccess",
			args: snippetTestArgs{
				gitlabClient: &snippetClient{
					scheduleStorageMoveForSnippet: scheduleStorageMoveForSnippetReturnsSuccess,
					getStorageMoveForSnippet:      getSnippetStorageMoveWithStateFinished,
				},
				snippet: testSnippet,
				cfg:     testCfg,
			},
			assertError: "",
			assertMetrics: fmt.Sprintf(`
				# HELP gitalyctl_snippets_active_repository_moves Number of repository moves in progress.
				# TYPE gitalyctl_snippets_active_repository_moves gauge
				gitalyctl_snippets_active_repository_moves{storage="%v"} 0
				# HELP gitalyctl_snippets_successful_repository_moves_total Number of successful repository moves.
        	    # TYPE gitalyctl_snippets_successful_repository_moves_total counter
        	    gitalyctl_snippets_successful_repository_moves_total{storage="%[1]v"} 1
			`, testGitalySourceStorageName),
		},
		{
			name: "ReturnsErrorOnScheduleMoveFailure",
			args: snippetTestArgs{
				gitlabClient: &snippetClient{scheduleStorageMoveForSnippet: scheduleStorageMoveForSnippetReturnsError},
				snippet:      testSnippet,
				cfg:          testCfg,
			},
			assertError: "schedule snippet repository move: test error",
			assertMetrics: fmt.Sprintf(`
				# HELP gitalyctl_snippets_failed_repository_moves_total Number of failed repository moves.
        	    # TYPE gitalyctl_snippets_failed_repository_moves_total counter
        	    gitalyctl_snippets_failed_repository_moves_total{storage="%v"} 1
			`, testGitalySourceStorageName),
		},
		{
			name: "ReturnsErrorWhenWaitForRepositoryMoveFails",
			args: snippetTestArgs{
				gitlabClient: &snippetClient{
					scheduleStorageMoveForSnippet: scheduleStorageMoveForSnippetReturnsSuccess,
					getStorageMoveForSnippet:      getSnippetStorageMoveReturnsError,
				},
				snippet: testSnippet,
				cfg:     testCfg,
			},
			assertError: "wait for snippet repository move: retrieve snippet repository move: test error",
			assertMetrics: fmt.Sprintf(`
				# HELP gitalyctl_snippets_active_repository_moves Number of repository moves in progress.
				# TYPE gitalyctl_snippets_active_repository_moves gauge
				gitalyctl_snippets_active_repository_moves{storage="%v"} 0
				# HELP gitalyctl_snippets_failed_repository_moves_total Number of failed repository moves.
        	    # TYPE gitalyctl_snippets_failed_repository_moves_total counter
        	    gitalyctl_snippets_failed_repository_moves_total{storage="%[1]v"} 1
			`, testGitalySourceStorageName),
		},
		{
			name: "ReturnsErrorOnGetSnippetRepositoryStorageMoveFailure",
			args: snippetTestArgs{
				gitlabClient: &snippetClient{
					scheduleStorageMoveForSnippet: scheduleStorageMoveForSnippetReturnsSuccess,
					getStorageMoveForSnippet:      getSnippetStorageMoveReturnsError,
				},
				snippet: testSnippet,
				cfg:     testCfg,
			},
			assertError: "wait for snippet repository move: retrieve snippet repository move: test error",
			assertMetrics: fmt.Sprintf(`
				# HELP gitalyctl_snippets_active_repository_moves Number of repository moves in progress.
				# TYPE gitalyctl_snippets_active_repository_moves gauge
				gitalyctl_snippets_active_repository_moves{storage="%v"} 0
				# HELP gitalyctl_snippets_failed_repository_moves_total Number of failed repository moves.
        	    # TYPE gitalyctl_snippets_failed_repository_moves_total counter
        	    gitalyctl_snippets_failed_repository_moves_total{storage="%[1]v"} 1
			`, testGitalySourceStorageName),
		},
	}

	for _, tt := range tests {
		ctx := context.Background()

		tt.args.stdout = &bytes.Buffer{}
		tt.args.logger = setupLogger(tt.args.stdout)

		r := prometheus.NewRegistry()
		s := newSnippetRepository(tt.args.cfg, r)

		t.Run(tt.name, func(t *testing.T) {
			err := s.moveSnippetRepository(ctx, tt.args.gitlabClient, tt.args.snippet, tt.args.cfg, tt.args.logger)

			s.assertTests(t, tt, err)
			if tt.assertCustomFunc != nil {
				tt.assertCustomFunc(t, tt.args)
			}
		})
	}
}

func TestWaitForSnippetRepositoryMove(t *testing.T) {
	testSnippet := &gitlab.Snippet{
		WebURL: "https://gitlab.example.com/test-group/test-group-snippet-gofedo",
	}
	testCfg := &DrainCfg{Snippet: RepositoryCfg{MoveTimeout: 10 * time.Second}}

	tests := []snippetTest{
		{
			name: "ReturnsNilErrorForFinishedMove",
			args: snippetTestArgs{
				gitlabClient:   &snippetClient{getStorageMoveForSnippet: getSnippetStorageMoveWithStateFinished},
				snippet:        testSnippet,
				repositoryMove: &gitlab.SnippetRepositoryStorageMove{ID: 1, SourceStorageName: testGitalySourceStorageName},
				cfg:            testCfg,
			},
			assertError: "",
			assertMetrics: fmt.Sprintf(`
				# HELP gitalyctl_snippets_active_repository_moves Number of repository moves in progress.
				# TYPE gitalyctl_snippets_active_repository_moves gauge
				gitalyctl_snippets_active_repository_moves{storage="%v"} 0
				# HELP gitalyctl_snippets_successful_repository_moves_total Number of successful repository moves.
				# TYPE gitalyctl_snippets_successful_repository_moves_total counter
				gitalyctl_snippets_successful_repository_moves_total{storage="%[1]v"} 1
			`, testGitalySourceStorageName),
		},
		{
			name: "ReturnsNilErrorForFailedMove",
			args: snippetTestArgs{
				gitlabClient:   &snippetClient{getStorageMoveForSnippet: getSnippetStorageMoveWithStateFailed},
				snippet:        testSnippet,
				repositoryMove: &gitlab.SnippetRepositoryStorageMove{ID: 1, SourceStorageName: testGitalySourceStorageName},
				cfg:            testCfg,
			},
			assertError: "",
			assertMetrics: fmt.Sprintf(`
				# HELP gitalyctl_snippets_active_repository_moves Number of repository moves in progress.
				# TYPE gitalyctl_snippets_active_repository_moves gauge
				gitalyctl_snippets_active_repository_moves{storage="%v"} 0
				# HELP gitalyctl_snippets_failed_repository_moves_total Number of failed repository moves.
        	    # TYPE gitalyctl_snippets_failed_repository_moves_total counter
        	    gitalyctl_snippets_failed_repository_moves_total{storage="%[1]v"} 1
			`, testGitalySourceStorageName),
		},
		{
			name: "ReturnsNilErrorForCleanupFailedMove",
			args: snippetTestArgs{
				gitlabClient:   &snippetClient{getStorageMoveForSnippet: getSnippetStorageMoveWithStateCleanupFailed},
				snippet:        testSnippet,
				repositoryMove: &gitlab.SnippetRepositoryStorageMove{ID: 1, SourceStorageName: testGitalySourceStorageName},
				cfg:            testCfg,
			},
			assertError: "",
			assertMetrics: fmt.Sprintf(`
				# HELP gitalyctl_snippets_active_repository_moves Number of repository moves in progress.
				# TYPE gitalyctl_snippets_active_repository_moves gauge
				gitalyctl_snippets_active_repository_moves{storage="%v"} 0
				# HELP gitalyctl_snippets_failed_repository_moves_total Number of failed repository moves.
        	    # TYPE gitalyctl_snippets_failed_repository_moves_total counter
        	    gitalyctl_snippets_failed_repository_moves_total{storage="%[1]v"} 1
			`, testGitalySourceStorageName),
		},
		{
			name: "GetStorageMoveReturnsError",
			args: snippetTestArgs{
				gitlabClient:   &snippetClient{getStorageMoveForSnippet: getSnippetStorageMoveReturnsError},
				snippet:        testSnippet,
				repositoryMove: &gitlab.SnippetRepositoryStorageMove{ID: 1, SourceStorageName: testGitalySourceStorageName},
				cfg:            testCfg,
			},
			assertError: "retrieve snippet repository move: test error",
			assertMetrics: fmt.Sprintf(`
				# HELP gitalyctl_snippets_active_repository_moves Number of repository moves in progress.
				# TYPE gitalyctl_snippets_active_repository_moves gauge
				gitalyctl_snippets_active_repository_moves{storage="%v"} 0
				# HELP gitalyctl_snippets_failed_repository_moves_total Number of failed repository moves.
        	    # TYPE gitalyctl_snippets_failed_repository_moves_total counter
				gitalyctl_snippets_failed_repository_moves_total{storage="%[1]v"} 1
			`, testGitalySourceStorageName),
		},
		{
			name: "LogsErrorOnTimeout",
			args: snippetTestArgs{
				gitlabClient: &snippetClient{getStorageMoveForSnippet: getSnippetStorageMoveWithStateStarted},
				snippet:      testSnippet,
				repositoryMove: &gitlab.SnippetRepositoryStorageMove{
					ID:                     1,
					SourceStorageName:      testGitalySourceStorageName,
					DestinationStorageName: testGitalyDestinationStorageName,
				},
				cfg: &DrainCfg{Snippet: RepositoryCfg{MoveTimeout: time.Second}},
			},
			assertError: "",
			assertMetrics: fmt.Sprintf(`
				# HELP gitalyctl_snippets_active_repository_moves Number of repository moves in progress.
				# TYPE gitalyctl_snippets_active_repository_moves gauge
				gitalyctl_snippets_active_repository_moves{storage="%v"} 0
				# HELP gitalyctl_snippets_timed_out_repository_moves_total Number of timed out repository moves.
        	    # TYPE gitalyctl_snippets_timed_out_repository_moves_total counter
        	    gitalyctl_snippets_timed_out_repository_moves_total{storage="%[1]v"} 1
			`, testGitalySourceStorageName),
		},
	}

	for _, tt := range tests {
		tt.args.stdout = &bytes.Buffer{}
		tt.args.logger = setupLogger(tt.args.stdout)

		r := prometheus.NewRegistry()
		s := newSnippetRepository(tt.args.cfg, r)

		t.Run(tt.name, func(t *testing.T) {
			err := s.waitForSnippetRepositoryMove(tt.args.gitlabClient, tt.args.repositoryMove, tt.args.snippet, tt.args.cfg, tt.args.logger)
			s.assertTests(t, tt, err)
		})
	}
}

func TestSnippetRepositoryPagination(t *testing.T) {
	var pagesVisited []int

	gitLabClient := &snippetClient{listAllSnippets: func(opt *gitlab.ListAllSnippetsOptions, options ...gitlab.RequestOptionFunc) ([]*gitlab.Snippet, *gitlab.Response, error) {
		require.Equal(t, *opt.RepositoryStorage, "storage1")
		require.Equal(t, *&opt.PerPage, apiResultsPerPage)

		pagesVisited = append(pagesVisited, opt.Page)

		nextPage := opt.Page + 1
		if nextPage == 5 { // We've reached the end.
			nextPage = 0
		}

		resp := gitlab.Response{
			Response: &http.Response{},
			NextPage: nextPage,
		}

		return nil, &resp, nil
	}}

	cfg := &DrainCfg{
		Storage: StorageCfg{Concurrency: 1},
		Snippet: RepositoryCfg{
			Concurrency:      1,
			MoveTimeout:      3 * time.Second,
			MoveStatusUpdate: time.Second,
		},
	}

	ctx := context.Background()
	s := newSnippetRepository(cfg, nil)

	require.NoError(t, s.moveSnippets(ctx, gitLabClient, cfg, nil, "storage1"))
	require.Equal(t, []int{1, 2, 3, 4}, pagesVisited)
}

func (s *snippetRepository) assertTests(t *testing.T, tt snippetTest, err error) {
	if tt.assertError == "" {
		require.NoError(t, err)
	} else {
		assert.EqualError(t, err, tt.assertError)
	}
	require.NoError(t, testutil.CollectAndCompare(s, bytes.NewBufferString(tt.assertMetrics)))
}

func listAllSnippetsReturnsSnippet(opt *gitlab.ListAllSnippetsOptions, options ...gitlab.RequestOptionFunc) ([]*gitlab.Snippet, *gitlab.Response, error) {
	s := []*gitlab.Snippet{
		{
			ID:                1,
			WebURL:            "https://gitlab.example.com/test-group/test-group-snippet-gofedo",
			RepositoryStorage: testGitalySourceStorageName,
		},
	}
	return s, nil, nil
}

func listAllSnippetsReturnsError(opt *gitlab.ListAllSnippetsOptions, options ...gitlab.RequestOptionFunc) ([]*gitlab.Snippet, *gitlab.Response, error) {
	return []*gitlab.Snippet{}, nil, fmt.Errorf("test error")
}

func scheduleStorageMoveForSnippetIsCalled(snippet int, opts gitlab.ScheduleStorageMoveForSnippetOptions, options ...gitlab.RequestOptionFunc) (*gitlab.SnippetRepositoryStorageMove, *gitlab.Response, error) {
	scheduleSnippetRepositoryStorageMoveCalledCount++
	return nil, nil, nil
}

func scheduleStorageMoveForSnippetReturnsSuccess(snippet int, opts gitlab.ScheduleStorageMoveForSnippetOptions, options ...gitlab.RequestOptionFunc) (*gitlab.SnippetRepositoryStorageMove, *gitlab.Response, error) {
	return &gitlab.SnippetRepositoryStorageMove{
		ID:                     1,
		CreatedAt:              gitlab.Time(time.Now()),
		State:                  repositoryMoveScheduled,
		SourceStorageName:      testGitalySourceStorageName,
		DestinationStorageName: testGitalyDestinationStorageName,
	}, nil, nil
}

func scheduleStorageMoveForSnippetReturnsError(snippet int, opts gitlab.ScheduleStorageMoveForSnippetOptions, options ...gitlab.RequestOptionFunc) (*gitlab.SnippetRepositoryStorageMove, *gitlab.Response, error) {
	return nil, nil, fmt.Errorf("test error")
}

func getSnippetStorageMoveWithStateFinished(repositoryStorage int, options ...gitlab.RequestOptionFunc) (*gitlab.SnippetRepositoryStorageMove, *gitlab.Response, error) {
	return &gitlab.SnippetRepositoryStorageMove{
		ID:                     1,
		CreatedAt:              gitlab.Time(time.Now()),
		State:                  repositoryMoveFinished,
		SourceStorageName:      testGitalySourceStorageName,
		DestinationStorageName: testGitalyDestinationStorageName,
	}, nil, nil
}

func getSnippetStorageMoveWithStateFailed(repositoryStorage int, options ...gitlab.RequestOptionFunc) (*gitlab.SnippetRepositoryStorageMove, *gitlab.Response, error) {
	return &gitlab.SnippetRepositoryStorageMove{
		ID:                     1,
		CreatedAt:              gitlab.Time(time.Now()),
		State:                  repositoryMoveFailed,
		SourceStorageName:      testGitalySourceStorageName,
		DestinationStorageName: testGitalyDestinationStorageName,
	}, nil, nil
}

func getSnippetStorageMoveWithStateCleanupFailed(repositoryStorage int, options ...gitlab.RequestOptionFunc) (*gitlab.SnippetRepositoryStorageMove, *gitlab.Response, error) {
	return &gitlab.SnippetRepositoryStorageMove{
		ID:                     1,
		CreatedAt:              gitlab.Time(time.Now()),
		State:                  repositoryMoveCleanupFailed,
		SourceStorageName:      testGitalySourceStorageName,
		DestinationStorageName: testGitalyDestinationStorageName,
	}, nil, nil
}

func getSnippetStorageMoveWithStateStarted(repositoryStorage int, options ...gitlab.RequestOptionFunc) (*gitlab.SnippetRepositoryStorageMove, *gitlab.Response, error) {
	return &gitlab.SnippetRepositoryStorageMove{
		ID:                     1,
		CreatedAt:              gitlab.Time(time.Now()),
		State:                  repositoryMoveStarted,
		SourceStorageName:      testGitalySourceStorageName,
		DestinationStorageName: testGitalyDestinationStorageName,
	}, nil, nil
}

func getSnippetStorageMoveReturnsError(repositoryStorage int, options ...gitlab.RequestOptionFunc) (*gitlab.SnippetRepositoryStorageMove, *gitlab.Response, error) {
	return nil, nil, fmt.Errorf("test error")
}
