package storage

import (
	"context"
	"fmt"
	"strings"
	"time"

	"github.com/avast/retry-go"
	"github.com/fatih/semgroup"
	"github.com/go-kit/log"
	"github.com/go-kit/log/level"
	"github.com/prometheus/client_golang/prometheus"
	"github.com/xanzy/go-gitlab"
	"golang.org/x/time/rate"
)

type DrainCfg struct {
	GitLab struct {
		ApiURL string `yaml:"api_url"`
	}
	Storage StorageCfg    `yaml:"storage"`
	DryRun  bool          `yaml:"dry_run"`
	Group   RepositoryCfg `yaml:"group"`
	Snippet RepositoryCfg `yaml:"snippet"`
	Project RepositoryCfg `yaml:"project"`
}

type StorageCfg struct {
	Move        []string `yaml:"move"`
	Concurrency int64    `yaml:"concurrency"`
}

type RepositoryCfg struct {
	Enabled                bool          `yaml:"enabled"`
	Concurrency            int64         `yaml:"concurrency"`
	MoveTimeout            time.Duration `yaml:"move_timeout"`
	MoveStatusUpdate       time.Duration `yaml:"move_status_update"`
	SkipRepositories       []int         `yaml:"skip_repositories"`
	SkipPooledRepositories bool          `yaml:"skip_pooled_repositories"`
	DestinationStorage     string        `yaml:"destination_storage"`
}

type repositoryMetrics struct {
	currentPageGauge                 *prometheus.GaugeVec
	skippedRepositoryCounter         *prometheus.CounterVec
	activeRepositoryMovesGauge       *prometheus.GaugeVec
	failedRepositoryMovesCounter     *prometheus.CounterVec
	timedOutRepositoryMovesCounter   *prometheus.CounterVec
	readOnlyRepositoriesCounter      *prometheus.CounterVec
	successfulRepositoryMovesCounter *prometheus.CounterVec
	itemsPerPageGauge                prometheus.Gauge
	storageConcurrencyGauge          prometheus.Gauge
	repositoryConcurrencyGauge       prometheus.Gauge
}

type drainer interface {
	drain(ctx context.Context, c *gitlab.Client, cfg *DrainCfg, logger log.Logger, storage string) error
}

const (
	// All possible repository move states
	//
	// GitLab API docs:
	// https://docs.gitlab.com/ee/api/project_repository_storage_moves.html
	repositoryMoveInitial       = "initial"
	repositoryMoveScheduled     = "scheduled"
	repositoryMoveStarted       = "started"
	repositoryMoveReplicated    = "replicated"
	repositoryMoveFailed        = "failed"
	repositoryMoveFinished      = "finished"
	repositoryMoveCleanupFailed = "cleanup failed"

	apiResultsPerPage  = 100
	gitalyctlNamespace = "gitalyctl"
)

func Drain(ctx context.Context, cfg *DrainCfg, logger log.Logger, m prometheus.Registerer, gitlabPAT string) error {
	if cfg.DryRun {
		level.Info(logger).Log("msg", "starting dry run.")
	}

	gitlabClient, err := gitlab.NewClient(
		gitlabPAT,
		gitlab.WithBaseURL(cfg.GitLab.ApiURL),
		gitlab.WithCustomLimiter(rate.NewLimiter(5000, 5000)), // 300,000 request/minute = (5000/sec * 60)
	)
	if err != nil {
		return fmt.Errorf("create gitlab client: %w", err)
	}
	gitlabClient.UserAgent = "gitalyctl"

	g := newGroupRepository(cfg, m)
	s := newSnippetRepository(cfg, m)
	p := newProjectRepository(cfg, m)

	sg := semgroup.NewGroup(ctx, cfg.Storage.Concurrency)
	for _, storage := range cfg.Storage.Move {
		srcStorage := storage
		logger := log.With(logger, "storage", storage)

		sg.Go(func() error {
			select {
			case <-ctx.Done():
				return ctx.Err()
			default:
				if cfg.Group.Enabled {
					level.Info(logger).Log("event", "drain groups")
					err = g.drain(ctx, gitlabClient, cfg, logger, srcStorage)
					if err != nil {
						return fmt.Errorf("drain groups: %w", err)
					}
				} else {
					level.Info(logger).Log("event", "drain groups is disabled, skipping.")
				}

				if cfg.Snippet.Enabled {
					level.Info(logger).Log("event", "drain snippets")
					err = s.drain(ctx, gitlabClient, cfg, logger, srcStorage)
					if err != nil {
						return fmt.Errorf("drain snippets: %w", err)
					}
				} else {
					level.Info(logger).Log("event", "drain snippets is disabled, skipping.")
				}

				if cfg.Project.Enabled {
					level.Info(logger).Log("event", "drain projects")
					err = p.drain(ctx, gitlabClient, cfg, logger, srcStorage)
					if err != nil {
						return fmt.Errorf("drain projects: %w", err)
					}
				} else {
					level.Info(logger).Log("event", "drain projects is disabled, skipping.")
				}

				level.Info(logger).Log("event", "drain complete")
			}
			return nil
		})
	}

	if err = sg.Wait(); err != nil {
		if strings.Contains(err.Error(), "context canceled") {
			return ctx.Err()
		}
		return fmt.Errorf("move storage: %w", err)
	}

	return nil
}

func registerMetrics(namespace string) repositoryMetrics {
	return repositoryMetrics{
		currentPageGauge: prometheus.NewGaugeVec(
			prometheus.GaugeOpts{
				Namespace: namespace,
				Name:      "current_page",
				Help:      "Current page number for the API results.",
			},
			[]string{"storage"},
		),
		skippedRepositoryCounter: prometheus.NewCounterVec(
			prometheus.CounterOpts{
				Namespace: namespace,
				Name:      "skipped_repositories_total",
				Help:      "Number of repositories that have been skipped.",
			},
			[]string{"reason"},
		),
		activeRepositoryMovesGauge: prometheus.NewGaugeVec(
			prometheus.GaugeOpts{
				Namespace: namespace,
				Name:      "active_repository_moves",
				Help:      "Number of repository moves in progress.",
			},
			[]string{"storage"},
		),
		failedRepositoryMovesCounter: prometheus.NewCounterVec(
			prometheus.CounterOpts{
				Namespace: namespace,
				Name:      "failed_repository_moves_total",
				Help:      "Number of failed repository moves.",
			},
			[]string{"storage"},
		),
		timedOutRepositoryMovesCounter: prometheus.NewCounterVec(
			prometheus.CounterOpts{
				Namespace: namespace,
				Name:      "timed_out_repository_moves_total",
				Help:      "Number of timed out repository moves.",
			},
			[]string{"storage"},
		),
		readOnlyRepositoriesCounter: prometheus.NewCounterVec(
			prometheus.CounterOpts{
				Namespace: namespace,
				Name:      "read_only_repositories_total",
				Help:      "Number of read-only repositories.",
			},
			[]string{"storage"},
		),
		successfulRepositoryMovesCounter: prometheus.NewCounterVec(
			prometheus.CounterOpts{
				Namespace: namespace,
				Name:      "successful_repository_moves_total",
				Help:      "Number of successful repository moves.",
			},
			[]string{"storage"},
		),
		itemsPerPageGauge: prometheus.NewGauge(
			prometheus.GaugeOpts{
				Namespace: namespace,
				Name:      "items_per_page",
				Help:      "Number of items on each API results page.",
			},
		),
		storageConcurrencyGauge: prometheus.NewGauge(
			prometheus.GaugeOpts{
				Namespace: namespace,
				Name:      "storage_concurrency",
				Help:      "Number of Gitaly storages to drain at once.",
			},
		),
		repositoryConcurrencyGauge: prometheus.NewGauge(
			prometheus.GaugeOpts{
				Namespace: namespace,
				Name:      "repository_concurrency",
				Help:      "Number of repositories to move at once.",
			},
		),
	}
}

// retryWithBackOff retries the function passed to it 10 times with back-off delay.
func retryWithBackOff(ctx context.Context, retryableFunc func() error) error {
	return retry.Do(func() error {
		return retryableFunc()
	},
		retry.Context(ctx),
		retry.Attempts(10),
		retry.DelayType(retry.BackOffDelay),
	)
}
