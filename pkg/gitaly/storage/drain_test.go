package storage

import (
	"bytes"
	"context"
	"testing"

	"github.com/go-kit/log"
	"github.com/go-kit/log/level"
	"github.com/prometheus/client_golang/prometheus"
	"github.com/stretchr/testify/assert"
)

const (
	testGitalySourceStorageName      = "TestSourceStorage"
	testGitalyDestinationStorageName = "TestDestinationStorage"
	testSuccessfulID                 = 1
	testFailedID                     = 0
)

func setupLogger(stdout *bytes.Buffer) log.Logger {
	logger := log.NewLogfmtLogger(log.NewSyncWriter(stdout))
	logger = log.With(logger, "ts", log.DefaultTimestampUTC)
	return level.NewInjector(logger, level.InfoValue())
}

func TestDrainStorage_ContextCanceledPreventsCallingFetchProjects(t *testing.T) {
	stdout := &bytes.Buffer{}
	logger := setupLogger(stdout)

	ctx, cancel := context.WithCancel(context.Background())
	cancel()

	cfg := &DrainCfg{
		Storage: StorageCfg{
			Concurrency: 1,
			Move:        []string{testGitalySourceStorageName},
		},
	}

	err := Drain(ctx, cfg, logger, prometheus.NewRegistry(), "testPAT")
	assert.EqualError(t, err, "context canceled")
	assert.NotContains(t, stdout.String(), "event=\"drain storage\"")
}
