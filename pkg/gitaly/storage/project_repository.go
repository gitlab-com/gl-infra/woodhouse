package storage

import (
	"context"
	"fmt"
	"strings"
	"time"

	"github.com/fatih/semgroup"
	"github.com/go-kit/log"
	"github.com/go-kit/log/level"
	"github.com/prometheus/client_golang/prometheus"
	"github.com/xanzy/go-gitlab"
)

type projectClient struct {
	listProjects                      func(opt *gitlab.ListProjectsOptions, options ...gitlab.RequestOptionFunc) ([]*gitlab.Project, *gitlab.Response, error)
	scheduleStorageMoveForProject     func(project int, opts gitlab.ScheduleStorageMoveForProjectOptions, options ...gitlab.RequestOptionFunc) (*gitlab.ProjectRepositoryStorageMove, *gitlab.Response, error)
	getStorageMoveForProject          func(repositoryStorage int, options ...gitlab.RequestOptionFunc) (*gitlab.ProjectRepositoryStorageMove, *gitlab.Response, error)
	retrieveAllStorageMovesForProject func(project int, opts gitlab.RetrieveAllProjectStorageMovesOptions, options ...gitlab.RequestOptionFunc) ([]*gitlab.ProjectRepositoryStorageMove, *gitlab.Response, error)
	getSettings                       func(options ...gitlab.RequestOptionFunc) (*gitlab.Settings, *gitlab.Response, error)
}

type projectRepository struct {
	metrics *repositoryMetrics
}

const (
	projectNamespace = gitalyctlNamespace + "_projects"
)

func (p *projectRepository) drain(ctx context.Context, c *gitlab.Client, cfg *DrainCfg, logger log.Logger, storage string) error {
	gitLabClient := &projectClient{
		listProjects:                      c.Projects.ListProjects,
		scheduleStorageMoveForProject:     c.ProjectRepositoryStorageMove.ScheduleStorageMoveForProject,
		getStorageMoveForProject:          c.ProjectRepositoryStorageMove.GetStorageMove,
		retrieveAllStorageMovesForProject: c.ProjectRepositoryStorageMove.RetrieveAllStorageMovesForProject,
		getSettings:                       c.Settings.GetSettings,
	}

	err := retryWithBackOff(ctx, func() error {
		return p.moveProjects(ctx, gitLabClient, cfg, logger, storage)
	})
	if err != nil {
		return fmt.Errorf("drain project storage: %w", err)
	}

	return nil
}

func (p *projectRepository) moveProjects(ctx context.Context, gitlabClient *projectClient, cfg *DrainCfg, logger log.Logger, storage string) error {
	sg := semgroup.NewGroup(ctx, cfg.Project.Concurrency)
	p.metrics.currentPageGauge.WithLabelValues(storage).Set(1)

	opts := &gitlab.ListProjectsOptions{
		Statistics:           gitlab.Ptr(true),
		IncludeHidden:        gitlab.Ptr(true),
		IncludePendingDelete: gitlab.Ptr(true),
		ListOptions: gitlab.ListOptions{
			OrderBy:    "id",
			Sort:       "asc",
			Pagination: "keyset",
			PerPage:    apiResultsPerPage,
		},
		RepositoryStorage: gitlab.Ptr(storage),
	}
	var options gitlab.RequestOptionFunc

	for {
		projects, resp, err := gitlabClient.listProjects(opts, options, gitlab.WithContext(ctx))
		if err != nil {
			return fmt.Errorf("list projects: %w", err)
		}

		for _, proj := range projects {
			project := proj
			if p.isSkippable(project, cfg, logger) {
				continue
			}

			sg.Go(func() error {
				select {
				case <-ctx.Done():
					return ctx.Err()
				default:
					err = p.moveProjectRepository(ctx, gitlabClient, project, cfg, logger)
					if err != nil {
						return fmt.Errorf("move repository: %w", err)
					}
				}
				return nil
			})
		}

		if resp == nil || resp.NextLink == "" {
			p.metrics.currentPageGauge.WithLabelValues(storage).Set(0)
			break
		}

		p.metrics.currentPageGauge.WithLabelValues(storage).Inc()
		options = gitlab.WithKeysetPaginationParameters(resp.NextLink)
	}

	if err := sg.Wait(); err != nil {
		if strings.Contains(err.Error(), "context canceled") {
			level.Info(logger).Log("event", "context cancelled")
			return ctx.Err()
		}

		return fmt.Errorf("move project: %w", err)
	}

	return nil
}

func (p *projectRepository) isSkippable(project *gitlab.Project, cfg *DrainCfg, logger log.Logger) bool {
	logger = log.With(logger,
		"event", "skip project",
		"project", project.WebURL,
		"project_id", project.ID,
	)

	if cfg.Project.SkipPooledRepositories && (project.ForksCount > 0 || project.ForkedFromProject != nil) {
		p.metrics.skippedRepositoryCounter.WithLabelValues("has_pooled_repository").Inc()
		level.Info(logger).Log("reason", "project has pooled repository")
		return true
	}

	for _, projectID := range cfg.Project.SkipRepositories {
		if projectID == project.ID {
			p.metrics.skippedRepositoryCounter.WithLabelValues("marked_skip").Inc()
			level.Info(logger).Log("reason", "project marked for skip")
			return true
		}
	}

	return false
}

func (p *projectRepository) moveProjectRepository(ctx context.Context, gitlabClient *projectClient, project *gitlab.Project, cfg *DrainCfg, logger log.Logger) error {
	logger = log.With(logger,
		"project", project.WebURL,
		"repository_size", project.Statistics.RepositorySize,
		"timeout_duration", cfg.Project.MoveTimeout,
	)
	var repositoryMoveOpts gitlab.ScheduleStorageMoveForProjectOptions

	if cfg.DryRun {
		level.Info(logger).Log("dry_run", "true", "event", "move repository")
		return nil
	}

	dstStorage, err := destinationStorageName(ctx, logger, cfg, gitlabClient, project)
	if err != nil {
		level.Error(logger).Log("event", "get destination storage name failed", "err", err)
		return fmt.Errorf("get destination storage: %w", err)
	}

	if dstStorage != "" {
		repositoryMoveOpts.DestinationStorageName = gitlab.Ptr(dstStorage)
	}

	level.Info(logger).Log("event", "move repository")
	repositoryMove, _, err := gitlabClient.scheduleStorageMoveForProject(project.ID, repositoryMoveOpts, gitlab.WithContext(ctx))
	if err != nil {
		repositoryMove, err = p.checkProjectRepositoryMoveError(gitlabClient, project, logger, err)
		if err != nil {
			p.metrics.failedRepositoryMovesCounter.WithLabelValues(project.RepositoryStorage).Inc()
			return err
		} else if repositoryMove.ID == 0 {
			p.metrics.readOnlyRepositoriesCounter.WithLabelValues(project.RepositoryStorage).Inc()
			level.Warn(logger).Log("event", "move repository", "msg", "project is read-only")
			return nil
		}
	}

	logger = log.With(logger,
		"project_repository_move_id", repositoryMove.ID,
		"destination_storage", repositoryMove.DestinationStorageName,
	)

	level.Info(logger).Log("event", "move repository state", "state", repositoryMove.State)

	err = p.waitForProjectRepositoryMove(gitlabClient, repositoryMove, project, cfg, logger)
	if err != nil {
		level.Error(logger).Log("event", "wait for project repository move error", "err", err)
		return fmt.Errorf("wait for project repository move: %w", err)
	}

	return nil
}

// Checks if a read-only repository already has an active move.
// If one is found continue waiting on the move, if no move is found log the read-only repository.
func (p *projectRepository) checkProjectRepositoryMoveError(gitlabClient *projectClient, project *gitlab.Project, logger log.Logger, err error) (*gitlab.ProjectRepositoryStorageMove, error) {
	if !strings.Contains(err.Error(), "is read-only") {
		return &gitlab.ProjectRepositoryStorageMove{}, fmt.Errorf("retrieve project repository move: %w", err)
	}

	repositoryMove, err := p.getProjectRepositoryMoves(gitlabClient, project, logger)
	if err != nil {
		return &gitlab.ProjectRepositoryStorageMove{}, fmt.Errorf("retrieve project repository move: %w", err)
	}

	return repositoryMove, nil
}

func (p *projectRepository) waitForProjectRepositoryMove(gitlabClient *projectClient, scheduledRepositoryMove *gitlab.ProjectRepositoryStorageMove, project *gitlab.Project, cfg *DrainCfg, logger log.Logger) error {
	startTime := time.Now()

	p.metrics.activeRepositoryMovesGauge.WithLabelValues(scheduledRepositoryMove.SourceStorageName).Inc()
	defer p.metrics.activeRepositoryMovesGauge.WithLabelValues(scheduledRepositoryMove.SourceStorageName).Dec()

	for {
		repositoryMove, _, err := gitlabClient.getStorageMoveForProject(scheduledRepositoryMove.ID)
		if err != nil {
			level.Error(logger).Log("event", "retrieve project repository move", "err", err)
			p.metrics.failedRepositoryMovesCounter.WithLabelValues(scheduledRepositoryMove.SourceStorageName).Inc()
			return fmt.Errorf("retrieve project repository move: %w", err)
		}

		repoMoveLogger := log.With(logger, "state", repositoryMove.State)

		if time.Since(startTime) >= cfg.Project.MoveTimeout {
			level.Error(repoMoveLogger).Log("event", "move repository timeout")
			p.metrics.timedOutRepositoryMovesCounter.WithLabelValues(repositoryMove.SourceStorageName).Inc()
			break
		}

		if repositoryMove.State == repositoryMoveFinished {
			level.Info(repoMoveLogger).Log("event", "move repository state finished")
			p.metrics.successfulRepositoryMovesCounter.WithLabelValues(repositoryMove.SourceStorageName).Inc()
			break
		} else if repositoryMove.State == repositoryMoveFailed || repositoryMove.State == repositoryMoveCleanupFailed {
			level.Error(repoMoveLogger).Log("event", "move repository state failed")
			p.metrics.failedRepositoryMovesCounter.WithLabelValues(repositoryMove.SourceStorageName).Inc()
			break
		}

		level.Info(repoMoveLogger).Log("event", "move repository state")
		// Prevents spamming the endpoint between checks
		time.Sleep(cfg.Project.MoveStatusUpdate)
	}

	return nil
}

func (p *projectRepository) getProjectRepositoryMoves(gitlabClient *projectClient, project *gitlab.Project, logger log.Logger) (*gitlab.ProjectRepositoryStorageMove, error) {
	opts := gitlab.RetrieveAllProjectStorageMovesOptions{
		PerPage: 1,
	}

	repositoryMoves, _, err := gitlabClient.retrieveAllStorageMovesForProject(project.ID, opts)
	if err != nil {
		level.Error(logger).Log("event", "retrieve storage moves failed", "err", err)
		return &gitlab.ProjectRepositoryStorageMove{}, fmt.Errorf("check project repository move status: %w", err)
	}

	if len(repositoryMoves) == 1 {
		if repositoryMoves[0].State != repositoryMoveFailed && repositoryMoves[0].State != repositoryMoveFinished && repositoryMoves[0].State != repositoryMoveCleanupFailed {
			level.Info(logger).Log("event", "move repository state", "project", project.WebURL,
				"repository_size", project.Statistics.RepositorySize,
				"state", repositoryMoves[0].State, "destination_storage", repositoryMoves[0].DestinationStorageName)
			return repositoryMoves[0], nil
		}
	}

	return &gitlab.ProjectRepositoryStorageMove{}, nil
}

func newProjectRepository(cfg *DrainCfg, registry prometheus.Registerer) *projectRepository {
	p := projectRepository{}
	m := registerMetrics(projectNamespace)

	p.metrics = &m
	p.metrics.itemsPerPageGauge.Set(apiResultsPerPage)
	p.metrics.storageConcurrencyGauge.Set(float64(cfg.Storage.Concurrency))
	p.metrics.repositoryConcurrencyGauge.Set(float64(cfg.Project.Concurrency))

	if registry != nil {
		registry.MustRegister(
			p.metrics.currentPageGauge,
			p.metrics.skippedRepositoryCounter,
			p.metrics.activeRepositoryMovesGauge,
			p.metrics.failedRepositoryMovesCounter,
			p.metrics.timedOutRepositoryMovesCounter,
			p.metrics.readOnlyRepositoriesCounter,
			p.metrics.successfulRepositoryMovesCounter,
			p.metrics.itemsPerPageGauge,
			p.metrics.storageConcurrencyGauge,
			p.metrics.repositoryConcurrencyGauge,
		)
	}

	return &p
}

// Describe is used to describe Prometheus metrics.
func (p *projectRepository) Describe(descs chan<- *prometheus.Desc) {
	prometheus.DescribeByCollect(p, descs)
}

// Collect is used to collect Prometheus metrics.
func (p *projectRepository) Collect(metrics chan<- prometheus.Metric) {
	p.metrics.currentPageGauge.Collect(metrics)
	p.metrics.skippedRepositoryCounter.Collect(metrics)
	p.metrics.activeRepositoryMovesGauge.Collect(metrics)
	p.metrics.failedRepositoryMovesCounter.Collect(metrics)
	p.metrics.timedOutRepositoryMovesCounter.Collect(metrics)
	p.metrics.readOnlyRepositoriesCounter.Collect(metrics)
	p.metrics.successfulRepositoryMovesCounter.Collect(metrics)
}

// destinationStorageName will specify which destination storage the project
// repository should be moved to. By default, it won't pick anything so it
// will return an empty string. This leaves it up to the rails application to
// pick a destination storage. If destination_storage is configured, then its value is returned.
//
// For forked repositories we want to specify a destination storage to be the
// same as the root repository of the fork network so that we can have [object
// deduplicaton](https://docs.gitlab.com/ee/development/git_object_deduplication.html),
// but we need to make sure that the storage where the root repository lives is
// accepting new repositories because it could be out of disk space. To do this
// we check the weight of the storage, to see if it's higher than 0.
//
// We check the weight of the storage every time there is a fork, rather than
// memoization it since we'll be actively moving repositories it might get full
// during the moves.
func destinationStorageName(ctx context.Context, logger log.Logger, cfg *DrainCfg, gitlabClient *projectClient, project *gitlab.Project) (string, error) {
	if dstStorage := cfg.Project.DestinationStorage; dstStorage != "" {
		return dstStorage, nil
	}

	if project.ForkedFromProject == nil {
		return "", nil
	}

	settings, _, err := gitlabClient.getSettings(gitlab.WithContext(ctx))
	if err != nil {
		return "", fmt.Errorf("get admin settings: %w", err)
	}

	weight := settings.RepositoryStoragesWeighted[project.ForkedFromProject.RepositoryStorage]

	if weight == 0 {
		level.Info(logger).Log(
			"event", "forked repository not moved next to root repository since weight is 0",
			"fork_from_project.repsitroy_storage", project.ForkedFromProject.RepositoryStorage,
		)
		return "", nil
	}

	return project.ForkedFromProject.RepositoryStorage, nil
}
