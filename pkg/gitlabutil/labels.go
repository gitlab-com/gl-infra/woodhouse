package gitlabutil

var Labels = map[string]string{
	"IncidentDeclare":    "Source::IMA::IncidentDeclare",
	"BlocksFeatureFlags": "blocks feature-flags",
	"BlocksDeployments":  "blocks deployments",
}
