package gitlabutil

import (
	"bytes"
	"context"
	"errors"
	"os"
	"strings"
	"text/template"
	"time"

	"github.com/go-kit/log"
	"github.com/sethvargo/go-retry"
	"github.com/xanzy/go-gitlab"
)

func NotifyMergeRequestFromMirrorSource(ctx context.Context, client *gitlab.Client, logger log.Logger, mirrorProjectPath, pipelineStatus string, waitDuration time.Duration, dryRun bool) error {
	projPath := mirrorProjectPath
	if projPath == "" {
		projPath = os.Getenv("CI_PROJECT_PATH")
	}
	logger = log.With(logger, "project_path", projPath)

	commit := os.Getenv("CI_COMMIT_SHA")
	commitMrs, err := awaitNonzeroMergeRequestCount(ctx, client, projPath, commit, waitDuration)
	if err != nil {
		return err
	}
	var mrURLs []string
	for _, mr := range commitMrs {
		mrURLs = append(mrURLs, mr.WebURL)
	}
	logger.Log("msg", "found MRs for commit", "commit", commit, "merge_requests", strings.Join(mrURLs, ", "))

	noteTemplate, err := template.New("note").Parse(noteBodyTemplate)
	if err != nil {
		return err
	}
	var noteBodyBuf bytes.Buffer
	err = noteTemplate.Execute(&noteBodyBuf, struct {
		URL, Status string
	}{
		URL: os.Getenv("CI_PIPELINE_URL"), Status: pipelineStatus,
	})
	if err != nil {
		return err
	}
	noteBody := noteBodyBuf.String()

	for _, mr := range commitMrs {
		if dryRun {
			logger.Log("msg", "would link merge request to this pipeline", "merge_request", mr.WebURL)
		} else {
			logger.Log("msg", "linking merge request to this pipeline", "merge_request", mr.WebURL)
			_, _, err := client.Notes.CreateMergeRequestNote(projPath, mr.IID, &gitlab.CreateMergeRequestNoteOptions{Body: &noteBody}, gitlab.WithContext(ctx))
			if err != nil {
				return err
			}
		}
	}
	return nil
}

func awaitNonzeroMergeRequestCount(ctx context.Context, client *gitlab.Client, projectPath, commitSHA string, waitDuration time.Duration) ([]*gitlab.MergeRequest, error) {
	var cancel context.CancelFunc
	ctx, cancel = context.WithTimeout(ctx, waitDuration)
	defer cancel()

	var commitMrs []*gitlab.MergeRequest
	err := retry.Constant(ctx, time.Second*10, func(ctx context.Context) error {
		var err error
		commitMrs, _, err = client.Commits.ListMergeRequestsByCommit(projectPath, commitSHA, gitlab.WithContext(ctx))
		if err != nil {
			return err
		}
		if len(commitMrs) == 0 {
			return retry.RetryableError(errors.New("zero merge requests found"))
		}
		return nil
	})
	return commitMrs, err
}

const noteBodyTemplate = `A pipeline is running on a mirror related to this merge request.

Status: {{ .Status }}

{{ .URL }}
`
