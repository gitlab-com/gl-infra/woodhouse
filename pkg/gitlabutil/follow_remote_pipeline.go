// Based on https://gitlab.com/cross-project-pipelines/cross-project-pipelines

package gitlabutil

import (
	"context"
	"errors"
	"fmt"
	"time"

	"github.com/go-kit/log"
	"github.com/sethvargo/go-retry"
	"github.com/xanzy/go-gitlab"
	"golang.org/x/sync/errgroup"
)

func FollowRemotePipeline(ctx context.Context, logger log.Logger, client *gitlab.Client, projectPath, commitSHA string) error {
	logger = log.With(logger, "project_path", projectPath, "commit_sha", commitSHA)

	pipelines, err := awaitNonzeroPipelineCount(ctx, client, projectPath, commitSHA)
	if err != nil {
		return err
	}

	errGroup, ctx := errgroup.WithContext(ctx)

	for _, pipeline := range pipelines {
		logger.Log("msg", "monitoring pipeline", "pipeline", pipeline.WebURL)
		errGroup.Go(func() error {
			resp, err := monitorPipeline(ctx, client, projectPath, pipeline)
			if err != nil {
				logger.Log("msg", "pipeline monitoring error", "pipeline", pipeline.WebURL, "error", err)
				return err
			}
			logger.Log("msg", "pipeline finished", "pipeline", resp.WebURL, "status", pipeline.Status)
			return nil
		})
	}

	return errGroup.Wait()

}

func monitorPipeline(
	ctx context.Context, client *gitlab.Client, projectPath string,
	pipeline *gitlab.PipelineInfo,
) (*gitlab.Pipeline, error) {
	var latestPipeline *gitlab.Pipeline
	if err := retry.Constant(ctx, time.Second*10, func(ctx context.Context) error {
		var err error
		latestPipeline, _, err = client.Pipelines.GetPipeline(projectPath, pipeline.ID, gitlab.WithContext(ctx))
		if err != nil {
			return err
		}
		if !isTerminalStatus(latestPipeline.Status) {
			return retry.RetryableError(fmt.Errorf("pipeline %s status: %s", latestPipeline.WebURL, latestPipeline.Status))
		}
		return nil
	}); err != nil {
		return nil, fmt.Errorf("error monitoring pipeline %s: %w", pipeline.WebURL, err)
	}
	if !isSuccessStatus(latestPipeline.Status) {
		return nil, fmt.Errorf("pipeline %s failed with status: %s", latestPipeline.WebURL, latestPipeline.Status)
	}
	return latestPipeline, nil
}

func awaitNonzeroPipelineCount(ctx context.Context, client *gitlab.Client, projectPath, commitSHA string) ([]*gitlab.PipelineInfo, error) {
	var pipelines []*gitlab.PipelineInfo
	err := retry.Constant(ctx, time.Second*10, func(ctx context.Context) error {
		var err error
		pipelines, _, err = client.Pipelines.ListProjectPipelines(projectPath, &gitlab.ListProjectPipelinesOptions{SHA: &commitSHA})
		if err != nil {
			return err
		}
		if len(pipelines) == 0 {
			return retry.RetryableError(errors.New("zero pipelines found"))
		}
		return nil
	})
	return pipelines, err
}

// https://docs.gitlab.com/ee/api/pipelines.html#list-project-pipelines
func isTerminalStatus(status string) bool {
	terminalStatuses := map[string]bool{
		"success":  true,
		"failed":   true,
		"canceled": true,
		"skipped":  true,
		"manual":   true,
	}
	return terminalStatuses[status]
}

func isSuccessStatus(status string) bool {
	return status == "success" || status == "manual"
}
