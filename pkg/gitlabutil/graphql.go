package gitlabutil

import (
	"bytes"
	"context"
	"encoding/json"
	"fmt"
	"io"
	"strings"

	httpretryable "github.com/hashicorp/go-retryablehttp"
)

const OpsEventHost string = "ops.gitlab.net"

type GraphQLClient struct {
	client     *httpretryable.Client
	token, url string
}

func NewGraphQLClient(client *httpretryable.Client, token, url string) *GraphQLClient {
	return &GraphQLClient{
		client: client,
		token:  token,
		url:    url,
	}
}

type SetSeverityResponse struct {
	Data struct {
		IssueSetSeverity struct{} `json:"issueSetSeverity"`
	} `json:"data"`
	Errors []struct {
		Message string `json:"message"`
	} `json:"errors"`
}

type CreateDiscussionResponse struct {
	// {"data":{"createDiscussion":{"errors":[]}}}
	// {"errors":[{"message":"..."}]}
	Data struct {
		CreateDiscussion struct{} `json:"createDiscussion"`
	} `json:"data"`
	Errors []struct {
		Message string `json:"message"`
	} `json:"errors"`
}

func (c *GraphQLClient) CreateInternalDiscussion(ctx context.Context, id int, body string) error {
	postResp, err := c.postGraphQL(ctx, createInternalDiscussion(id, body))
	if err != nil {
		return fmt.Errorf("CreateInternalDiscussion postGraphQL: %w", err)
	}
	decoder := json.NewDecoder(bytes.NewReader(postResp))
	var createDiscussionResponse CreateDiscussionResponse
	if err := decoder.Decode(&createDiscussionResponse); err != nil {
		return fmt.Errorf("CreateInternalDiscussion JSON Decode resp: %w", err)
	}

	if len(createDiscussionResponse.Errors) > 0 {
		return fmt.Errorf("CreateInternalDiscussion error, got: %v", createDiscussionResponse.Errors)
	}
	return nil
}

func (c *GraphQLClient) CreateLinkedResource(ctx context.Context, id int, link, linkText string) error {
	postResp, err := c.postGraphQL(ctx, issuableResourceLinkCreate(id, link, linkText, "general"))
	if err != nil {
		return fmt.Errorf("CreateLinkedResource postGraphQL: %w", err)
	}

	decoder := json.NewDecoder(bytes.NewReader(postResp))
	var setSeverityResponse SetSeverityResponse
	if err := decoder.Decode(&setSeverityResponse); err != nil {
		return fmt.Errorf("CreateLinkedResource JSON decode resp: %w", err)
	}

	if len(setSeverityResponse.Errors) > 0 {
		return fmt.Errorf("CreateLinkedResource error adding resource link, got: %v", setSeverityResponse.Errors)
	}

	return nil
}

func (c *GraphQLClient) SetIssueSeverity(ctx context.Context, iid int, projectPath, severity string) error {
	postResp, err := c.postGraphQL(ctx, issueSetSeverity(iid, projectPath, severity))
	if err != nil {
		return err
	}

	decoder := json.NewDecoder(bytes.NewReader(postResp))
	var setSeverityResponse SetSeverityResponse
	if err := decoder.Decode(&setSeverityResponse); err != nil {
		return err
	}

	if len(setSeverityResponse.Errors) > 0 {
		return fmt.Errorf("Error setting severity, got: %v", setSeverityResponse.Errors)
	}

	return nil
}

func (c *GraphQLClient) postGraphQL(ctx context.Context, body string) ([]byte, error) {
	req, err := httpretryable.NewRequestWithContext(
		ctx,
		"POST", c.url,
		strings.NewReader(body),
	)
	if err != nil {
		return nil, err
	}

	req.Header.Set("Authorization", fmt.Sprintf("Bearer %s", c.token))
	req.Header.Set("Content-Type", "application/json")

	resp, err := c.client.Do(req)
	if err != nil {
		return nil, err
	}
	defer resp.Body.Close()

	if resp.StatusCode != 200 {
		return nil, fmt.Errorf("Unable to post %s to %s, received %d response", body, c.url, resp.StatusCode)
	}

	postResp, err := io.ReadAll(resp.Body)
	if err != nil {
		return nil, err
	}

	return postResp, nil
}

func issueSetSeverity(iid int, projectPath, severity string) string {
	// https://docs.gitlab.com/ee/api/graphql/reference/#mutationissuesetseverity
	return fmt.Sprintf(`{ "query": "mutation { issueSetSeverity(input: { iid: \"%d\", projectPath: \"%s\", severity: %s }) { errors } }" }`, iid, projectPath, severity)
}

func issuableResourceLinkCreate(id int, link, linkText, linkType string) string {
	// https://docs.gitlab.com/ee/api/graphql/reference/#mutationissuableresourcelinkcreate
	return fmt.Sprintf(`{ "query": "mutation { issuableResourceLinkCreate(input: { id: \"gid://gitlab/Issue/%d\", link: \"%s\", linkText: \"%s\", linkType: %s }) { errors } }" }`, id, link, linkText, linkType)
}

func createInternalDiscussion(id int, body string) string {
	// https://docs.gitlab.com/ee/api/graphql/reference/index.html#mutationcreatediscussion
	return fmt.Sprintf(`{ "query": "mutation { createDiscussion(input: { body: \"%s\", internal: true, noteableId: \"gid://gitlab/Issue/%d\" }) { errors } }" }`, strings.ReplaceAll(body, "\n", "\\n"), id)
}
