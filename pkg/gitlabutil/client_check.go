package gitlabutil

import (
	"fmt"

	"github.com/xanzy/go-gitlab"
)

func ClientCheck(c *gitlab.Client) error {
	_, resp, err := c.Users.CurrentUser()
	if err != nil {
		return err
	}

	if resp.StatusCode != 200 {
		return fmt.Errorf("Invalid response from API: %d", resp.StatusCode)
	}

	return nil
}
