package gitlabutil

import (
	"context"
	"fmt"
	"io"
	"net/http"
	"net/http/httptest"
	"testing"

	httpretryable "github.com/hashicorp/go-retryablehttp"
	"github.com/stretchr/testify/assert"
	"github.com/stretchr/testify/require"
)

var ctx = context.Background()

func TestCreateInternalDiscussion(t *testing.T) {
	expected := `{ "query": "mutation { createDiscussion(input: { body: \"some reply from slack\", internal: true, noteableId: \"gid://gitlab/Issue/1\" }) { errors } }" }`
	ts := httptest.NewServer(http.HandlerFunc(func(w http.ResponseWriter, r *http.Request) {
		w.WriteHeader(http.StatusOK)

		assert.Equal(t, "POST", r.Method)

		reqBody, _ := io.ReadAll(r.Body)
		assert.Equal(t, expected, string(reqBody))

		fmt.Fprint(w, `{"data":{"createDiscussion":{"errors":[]}}}`)
	}))

	defer ts.Close()

	retryClient := httpretryable.NewClient()
	graphQLClient := NewGraphQLClient(
		retryClient,
		"some-token",
		ts.URL,
	)

	err := graphQLClient.CreateInternalDiscussion(ctx, 1, "some reply from slack")
	require.NoError(t, err)
}

func TestSetIssueSeverity(t *testing.T) {
	expected := `{ "query": "mutation { issueSetSeverity(input: { iid: \"100\", projectPath: \"some/project/path\", severity: SOME_SEVERITY }) { errors } }" }`
	ts := httptest.NewServer(http.HandlerFunc(func(w http.ResponseWriter, r *http.Request) {
		w.WriteHeader(http.StatusOK)

		assert.Equal(t, r.Method, "POST")

		reqBody, err := io.ReadAll(r.Body)
		require.Nil(t, err)
		assert.Equal(t, string(reqBody), expected)

		fmt.Fprint(w, `{"data":{"issueSetSeverity":{"errors":[]}}}`)
	}))

	defer ts.Close()

	retryClient := httpretryable.NewClient()
	graphQLClient := NewGraphQLClient(
		retryClient,
		"some-token",
		ts.URL,
	)

	err := graphQLClient.SetIssueSeverity(ctx, 100, "some/project/path", "SOME_SEVERITY")
	require.Nil(t, err)
}

func TestCreateLinkedResource(t *testing.T) {
	expected := `{ "query": "mutation { issuableResourceLinkCreate(input: { id: \"gid://gitlab/Issue/100\", link: \"https://example.com\", linkText: \"some title\", linkType: general }) { errors } }" }`
	ts := httptest.NewServer(http.HandlerFunc(func(w http.ResponseWriter, r *http.Request) {
		w.WriteHeader(http.StatusOK)

		assert.Equal(t, r.Method, "POST")

		reqBody, err := io.ReadAll(r.Body)
		require.Nil(t, err)
		assert.Equal(t, string(reqBody), expected)

		fmt.Fprint(w, `{"data":{"issuableResourceLinkCreate":{"errors":[]}}}`)
	}))

	defer ts.Close()

	retryClient := httpretryable.NewClient()
	graphQLClient := NewGraphQLClient(
		retryClient,
		"some-token",
		ts.URL,
	)

	err := graphQLClient.CreateLinkedResource(ctx, 100, "https://example.com", "some title")
	require.Nil(t, err)
}
