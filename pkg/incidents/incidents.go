package incidents

import (
	"fmt"
	"strconv"
	"strings"
)

const (
	ActiveIncidentLabel = "Incident::Active"
	SeverityCritical    = "CRITICAL"
	SeverityHigh        = "HIGH"
	SeverityMedium      = "MEDIUM"
	SeverityLow         = "LOW"
	SeverityUnknown     = "Unknown"
)

type Severity int

func ParseSeverityFromIssueLabel(label string) (Severity, error) {
	parts := strings.Split(label, "::")
	if len(parts) != 2 {
		return 0, fmt.Errorf("invalid severity label: %s", label)
	}
	if parts[0] != "severity" {
		return 0, fmt.Errorf("invalid severity label: %s", label)
	}
	sev, err := strconv.Atoi(parts[1])
	if err != nil {
		return 0, err
	}
	return Severity(sev), nil
}

func (s Severity) IssueLabel() string {
	return fmt.Sprintf("severity::%d", s)
}

func (s Severity) SlackEmoji() string {
	return fmt.Sprintf(":s%d-incident-severity:", s)
}

func (s Severity) IncidentSeverity() string {
	switch s {
	case 1:
		return SeverityCritical
	case 2:
		return SeverityHigh
	case 3:
		return SeverityMedium
	case 4:
		return SeverityLow
	default:
		return SeverityUnknown
	}
}

func (s Severity) IsS1S2() bool {
	return s.IncidentSeverity() == SeverityCritical || s.IncidentSeverity() == SeverityHigh
}
