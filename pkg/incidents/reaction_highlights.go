package incidents

import (
	"fmt"
	gogitlab "github.com/xanzy/go-gitlab"
	"github.com/zekroTJA/timedmap/v2"
	"log/slog"
	"strconv"
	"strings"
	"time"
)

type ReactionHighlights struct {
	GitlabClient                *gogitlab.Client
	GitlabProductionProjectPath string
	IssueIIDsForChannelNames    *timedmap.TimedMap[string, int]
	DiscussionIDsForIssueIIDs   *timedmap.TimedMap[int, string]
	Timestamps                  *timedmap.TimedMap[float64, bool]
}

type GLClientInterface struct {
}

func NewReactionHighlights(gitlabClient *gogitlab.Client, gitlabProductionProjectPath string) *ReactionHighlights {
	const scanTime = 10 * time.Second

	return &ReactionHighlights{
		GitlabClient:                gitlabClient,
		GitlabProductionProjectPath: gitlabProductionProjectPath,
		IssueIIDsForChannelNames:    timedmap.New[string, int](scanTime),
		DiscussionIDsForIssueIIDs:   timedmap.New[int, string](scanTime),
		Timestamps:                  timedmap.New[float64, bool](scanTime),
	}
}

func (h *ReactionHighlights) Handle(msg, channelName, name string, ts float64) {
	// Prevent duplications by dedupping based on timestamp
	if _, exists := h.Timestamps.GetValue(ts); exists {
		slog.Info("Received duplicate reaction on the same message", "msg", msg, "channelName", channelName)
		return
	}

	h.Timestamps.Set(ts, true, 30*time.Minute)
	tsDisplay := time.Unix(int64(ts), 0).UTC().Format("2006-01-02 15:04:05 UTC")
	// Get the issue ID from the channel Name
	issueIID, err := h.IssueIIDFromSlackChannel(channelName)
	if err != nil {
		slog.Error("Unable to get issue ID from the Slack Channel name for highlights", "err", err)
		return
	}

	// Find the discussion note with the text <!-- highlight -->
	discussionID, err := h.HighlightDiscussionID(issueIID)
	if err != nil {
		slog.Error("Unable to find a discussion ID for highlights", "err", err, "discussionID", discussionID)
		return
	}

	// Append to the discussion with the msg
	highlightMsg := h.SlackToGitLabMarkdown(tsDisplay, name, msg)
	if err := h.AddHighlight(issueIID, discussionID, highlightMsg); err != nil {
		slog.Error("Unable to add a new note to highlights discussion", "err", err, "discussionID", discussionID)
	}
}

func (h *ReactionHighlights) IssueIIDFromSlackChannel(channelName string) (int, error) {
	if val, exists := h.IssueIIDsForChannelNames.GetValue(channelName); exists {
		slog.Info("Using cached issue ID", "issueID", val)
		return val, nil
	}

	lastDashIndex := strings.LastIndex(channelName, "-")
	if lastDashIndex == -1 {
		return 0, fmt.Errorf("no dash found in channel name")
	}

	issueIID, err := strconv.Atoi(channelName[lastDashIndex+1:])
	if err != nil {
		return 0, fmt.Errorf("invalid issue IID after the last dash in channel name: %s", channelName)
	}

	h.IssueIIDsForChannelNames.Set(channelName, issueIID, 2*24*time.Hour)
	return issueIID, nil
}

func (h *ReactionHighlights) HighlightDiscussionID(issueIID int) (string, error) {
	if val, exists := h.DiscussionIDsForIssueIIDs.GetValue(issueIID); exists {
		slog.Info("Using cached discussion ID", "discussionID", val)
		return val, nil
	}

	opts := &gogitlab.ListIssueDiscussionsOptions{
		PerPage: 100,
		Page:    1,
	}

	for {
		discussions, resp, err := h.GitlabClient.Discussions.ListIssueDiscussions(h.GitlabProductionProjectPath, issueIID, opts)
		if err != nil {
			return "", fmt.Errorf("Unable to list discussions: %w", err)
		}

		for _, disc := range discussions {
			if len(disc.Notes) > 0 {
				if strings.Contains(disc.Notes[0].Body, "<!-- highlight -->") {
					h.DiscussionIDsForIssueIIDs.Set(issueIID, disc.ID, 2*24*time.Hour)
					return disc.ID, nil
				}
			}
		}

		if resp.NextPage == 0 {
			break
		}

		opts.Page = resp.NextPage
	}

	return "", fmt.Errorf("Unable to find any highlight discussion on issue")
}

func (h *ReactionHighlights) AddHighlight(issueIID int, discussionID, highlightMsg string) error {
	_, _, err := h.GitlabClient.Discussions.AddIssueDiscussionNote(h.GitlabProductionProjectPath, issueIID, discussionID, &gogitlab.AddIssueDiscussionNoteOptions{
		Body: &highlightMsg,
	})
	if err != nil {
		return fmt.Errorf("Unable to add msg highlight to discussion thread: %w", err)
	}
	return nil
}

func (h *ReactionHighlights) SlackToGitLabMarkdown(tsDisplay, name, msg string) string {
	// wrap in an table td block and add header
	return fmt.Sprintf("_%s **%s** comments on Slack_:\n\n<table><td>\n%s\n</td></table>\n", tsDisplay, name, msg)
}
