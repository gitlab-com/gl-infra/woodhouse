package incidents_test

import (
	"testing"

	"github.com/stretchr/testify/assert"
	"gitlab.com/gitlab-com/gl-infra/woodhouse/pkg/incidents"
	"gitlab.com/gitlab-com/gl-infra/woodhouse/pkg/slackutil"
)

func TestSeverity(t *testing.T) {
	sev := incidents.Severity(7)
	assert.Equal(t, ":s7-incident-severity:", sev.SlackEmoji())
	assert.Equal(t, "severity::7", sev.IssueLabel())
}

func TestParseSeverity(t *testing.T) {
	sev, err := incidents.ParseSeverityFromIssueLabel("severity::42")
	assert.Nil(t, err)
	assert.Equal(t, ":s42-incident-severity:", sev.SlackEmoji())
}

func TestParseSeverity_returnsErrorWhenWrongLabelPassed(t *testing.T) {
	_, err := incidents.ParseSeverityFromIssueLabel("foo::2")
	assert.NotNil(t, err)
	_, err = incidents.ParseSeverityFromIssueLabel("bar")
	assert.NotNil(t, err)
	_, err = incidents.ParseSeverityFromIssueLabel("foo::1::2")
	assert.NotNil(t, err)
}

func TestParseGitLabHandleFromURL(t *testing.T) {
	for _, tc := range []struct {
		name     string
		input    string
		expected string
	}{
		{
			name:     "returns username when input is a valid GitLab profile URL",
			input:    "https://gitlab.com/a_profile-with.all_valid_chars123",
			expected: "@a_profile-with.all_valid_chars123",
		},
		{
			name:     "returns empty string when input is not a valid GitLab profile URL",
			input:    "https://github.com/craigfurman",
			expected: "",
		},
		{
			name:     "returns empty string when input is not a valid URL",
			input:    "foo",
			expected: "",
		},
	} {
		t.Run(tc.name, func(t *testing.T) {
			handle := slackutil.ParseGitLabHandleFromURL(tc.input)
			assert.Equal(t, tc.expected, handle)
		})
	}
}
