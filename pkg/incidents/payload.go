package incidents

import (
	"errors"
	"strings"

	"github.com/slack-go/slack"
)

const (
	EOCSelection    = "pageEOC"
	IMSelection     = "pageIM"
	CMOCSelection   = "pageCMOC"
	GitalySelection = "pageGitaly"
	UseOpsSelection = "useOps"

	S1S2Tasks    = "s1s2Tasks"
	ExtraOptions = "extraOptions"
)

type IncidentPayload struct {
	incidentPayload *slack.InteractionCallback
}

func NewIncidentPayload(payload *slack.InteractionCallback) (*IncidentPayload, error) {
	if payload == nil {
		return nil, errors.New("NewIncidentPayload: payload is nil!")
	}
	return &IncidentPayload{incidentPayload: payload}, nil
}

func (p *IncidentPayload) Title() string {
	return p.incidentPayload.View.State.Values["title"]["title"].Value
}

func (p *IncidentPayload) SeverityLabel() string {
	return p.incidentPayload.View.State.Values["severity"]["severity"].SelectedOption.Value
}

func (p *IncidentPayload) PrivateMetadata() string {
	return p.incidentPayload.View.PrivateMetadata
}

func (p *IncidentPayload) IsInternallyFacing() bool {
	for _, labelObject := range p.incidentPayload.View.State.Values["extralabels"]["extralabels"].SelectedOptions {
		if labelObject.Value == "backstage" {
			return true
		}
	}
	return false
}

func (p *IncidentPayload) ServiceLabel() string {
	return p.incidentPayload.View.State.Values["service"]["service"].SelectedOption.Value
}

func (p *IncidentPayload) SelectedS1S2Tasks() []string {
	tasks := []string{}
	for _, selectedTask := range p.incidentPayload.View.State.Values[S1S2Tasks][S1S2Tasks].SelectedOptions {
		tasks = append(tasks, selectedTask.Value)
	}

	return tasks
}

func (p *IncidentPayload) IMSelected() bool {
	for _, s := range p.SelectedS1S2Tasks() {
		if s == IMSelection {
			return true
		}
	}
	return false
}

func (p *IncidentPayload) IsConfidential() bool {
	for _, option := range p.incidentPayload.View.State.Values[ExtraOptions][ExtraOptions].SelectedOptions {
		if option.Value == "confidential" {
			return true
		}
	}

	return false
}

func (p *IncidentPayload) UseOpsSelected() bool {
	for _, option := range p.incidentPayload.View.State.Values[ExtraOptions][ExtraOptions].SelectedOptions {
		if option.Value == UseOpsSelection {
			return true
		}
	}

	return false
}

func (p *IncidentPayload) PageGitalySelected() bool {
	for _, option := range p.incidentPayload.View.State.Values[ExtraOptions][ExtraOptions].SelectedOptions {
		if option.Value == GitalySelection {
			return true
		}
	}

	return false
}

func (p *IncidentPayload) ExtraLabels() []string {
	extraLabels := []string{}
	for _, option := range p.incidentPayload.View.State.Values[ExtraOptions][ExtraOptions].SelectedOptions {
		if option.Text != nil && strings.Contains(option.Text.Text, "label") {
			extraLabels = append(extraLabels, option.Value)
		}
	}

	return extraLabels
}
