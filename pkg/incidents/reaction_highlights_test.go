package incidents_test

import (
	"fmt"
	"github.com/stretchr/testify/assert"
	"github.com/stretchr/testify/require"
	"gitlab.com/gitlab-com/gl-infra/woodhouse/pkg/incidents"
	"testing"
	"time"
)

func TestIssueIIDFromSlackChannel(t *testing.T) {
	tests := []struct {
		channelName string
		expectedIID int
		expectedErr error
		prepare     func(highlight *incidents.ReactionHighlights)
	}{
		{
			channelName: "some-channel-123",
			expectedIID: 123,
			expectedErr: nil,
			prepare:     func(_ *incidents.ReactionHighlights) {},
		},
		{
			channelName: "another-channel-456",
			expectedIID: 123,
			expectedErr: nil,
			prepare: func(highlight *incidents.ReactionHighlights) {
				highlight.IssueIIDsForChannelNames.Set("another-channel-456", 123, 2*time.Hour)
			},
		},
		{
			channelName: "invalidchannelname",
			expectedIID: 0,
			expectedErr: fmt.Errorf("no dash found in channel name"),
			prepare:     func(_ *incidents.ReactionHighlights) {},
		},
		{
			channelName: "no-issue-id-",
			expectedIID: 0,
			expectedErr: fmt.Errorf("invalid issue IID after the last dash in channel name: no-issue-id-"),
			prepare:     func(_ *incidents.ReactionHighlights) {},
		},
	}

	for _, tt := range tests {
		t.Run(tt.channelName, func(t *testing.T) {
			highlight := incidents.NewReactionHighlights(nil, "some/project/path")
			tt.prepare(highlight)

			iid, err := highlight.IssueIIDFromSlackChannel(tt.channelName)

			if tt.expectedErr != nil {
				require.EqualError(t, err, tt.expectedErr.Error(), "error message should match")
			} else {
				require.NoError(t, err)
			}

			assert.Equal(t, tt.expectedIID, iid, "expected IID should match the returned IID")
		})
	}
}

func TestSlackToGitLabMarkdown(t *testing.T) {
	highlight := incidents.NewReactionHighlights(nil, "some/project/path")
	output := highlight.SlackToGitLabMarkdown("2024-01-01", "user", "some msg")
	want := `_2024-01-01 **user** comments on Slack_:

<table><td>
some msg
</td></table>
`
	assert.Equal(t, want, output, "expected output should match")
}
