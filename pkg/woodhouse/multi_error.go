package woodhouse

import (
	"errors"
)

func MultiError(errs []error) error {
	if len(errs) == 0 {
		return nil
	}

	return errors.Join(errs...)
}
