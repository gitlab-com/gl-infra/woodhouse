package web

import (
	"net/http"

	"github.com/gorilla/mux"
	"gitlab.com/gitlab-com/gl-infra/woodhouse/pkg/instrumentation"
)

type Router struct {
	mux *mux.Router
}

func NewRouter() *Router {
	return &Router{mux: mux.NewRouter()}
}

func (r *Router) Handle(pathPattern string, handler http.Handler) *mux.Route {
	return r.mux.Handle(pathPattern, instrumentWithRoute(pathPattern, handler))
}

func (r *Router) HandleFunc(pathPattern string, handler func(w http.ResponseWriter, req *http.Request)) *mux.Route {
	return r.Handle(pathPattern, http.HandlerFunc(handler))
}

func (r *Router) ServeHTTP(w http.ResponseWriter, req *http.Request) {
	r.mux.ServeHTTP(w, req)
}

func instrumentWithRoute(pathPattern string, next http.Handler) http.Handler {
	return http.HandlerFunc(func(w http.ResponseWriter, req *http.Request) {
		event := req.Context().Value(instrumentation.EventContextKey{}).(*instrumentation.Event)
		event.With("route", pathPattern)
		next.ServeHTTP(w, req)
	})
}
