package statusio

import (
	"encoding/json"
	"fmt"
	"reflect"
	"testing"

	"github.com/golang/mock/gomock"
	gostatusio "github.com/statusio/statusio-go"
	"gitlab.com/gitlab-com/gl-infra/woodhouse/pkg/statusio/mocks"
)

func componentsJsonToStruct(jsonStr string) (gostatusio.ComponentListResponse, error) {
	var clr gostatusio.ComponentListResponse
	jsonData := []byte(jsonStr)

	if err := json.Unmarshal(jsonData, &clr); err != nil {
		return clr, fmt.Errorf("unable to parse JSON: %s", err)
	}

	return clr, nil
}

func incidentResponseJsonToStruct(jsonStr string) (gostatusio.IncidentCreateResponse, error) {
	var icr gostatusio.IncidentCreateResponse
	jsonData := []byte(jsonStr)

	if err := json.Unmarshal(jsonData, &icr); err != nil {
		return icr, fmt.Errorf("unable to parse JSON: %s", err)
	}

	return icr, nil
}

func TestClient_GetInfrastructureListFilterByFunction(t *testing.T) {
	type args struct {
		description string
		matcher     func(name string) bool
	}
	tests := []struct {
		name    string
		prepare func(h *mocks.MockStatusioHandler)
		args    args
		want    []Component
		wantErr bool
	}{
		{
			name: "Successful exact and regex match",
			prepare: func(h *mocks.MockStatusioHandler) {
				clr, err := componentsJsonToStruct(`{"result": [ {"_id": "C1", "name": "API" }, {"_id": "C2", "name": "CI/CD Test Component"} ]}`)
				if err != nil {
					t.Error(err)
				}
				h.EXPECT().ComponentList(gomock.Any()).Return(clr, nil).Times(1)
			},
			args: args{
				description: "API and CI/CD",
				matcher: func(name string) bool {
					return true
				},
			},
			want: []Component{
				{ID: "C1", Name: "API"},
				{ID: "C2", Name: "CI/CD Test Component"},
			},
			wantErr: false,
		},
		{
			name: "Filter using matcher",
			prepare: func(h *mocks.MockStatusioHandler) {
				clr, err := componentsJsonToStruct(`{"result": [ {"_id": "C1", "name": "API" }, {"_id": "C2", "name": "CI/CD Test Component"} ]}`)
				if err != nil {
					t.Error(err)
				}
				h.EXPECT().ComponentList(gomock.Any()).Return(clr, nil).Times(1)
			},
			args: args{
				description: "API",
				matcher: func(name string) bool {
					return name == "API"
				},
			},
			want: []Component{
				{ID: "C1", Name: "API"},
			},
			wantErr: false,
		},
		{
			name: "No components matched",
			prepare: func(h *mocks.MockStatusioHandler) {
				clr, err := componentsJsonToStruct(`{"result": [ {"_id": "C1", "name": "Foo" }, {"_id": "C2", "name": "Bar"} ]}`)
				if err != nil {
					t.Error(err)
				}
				h.EXPECT().ComponentList(gomock.Any()).Return(clr, nil).Times(1)
			},
			args: args{
				description: "no matches",
				matcher: func(name string) bool {
					return true
				},
			},
			want:    nil,
			wantErr: true,
		},
		{
			name: "More than 10 components matched",
			prepare: func(h *mocks.MockStatusioHandler) {
				clr, err := componentsJsonToStruct(`{"result": [
					{"_id": "C1", "name": "API"},
					{"_id": "C2", "name": "Container Registry"},
					{"_id": "C3", "name": "CI/CD 1"},
					{"_id": "C4", "name": "CI/CD 2"},
					{"_id": "C5", "name": "CI/CD 3"},
					{"_id": "C6", "name": "CI/CD 4"},
					{"_id": "C7", "name": "CI/CD 5"},
					{"_id": "C8", "name": "CI/CD 6"},
					{"_id": "C9", "name": "CI/CD 7"},
					{"_id": "C10", "name": "CI/CD 8"},
					{"_id": "C11", "name": "CI/CD 9"}
				]}`)
				if err != nil {
					t.Error(err)
				}
				h.EXPECT().ComponentList(gomock.Any()).Return(clr, nil).Times(1)
			},
			args: args{
				description: "more than 10",
				matcher: func(name string) bool {
					return true
				},
			},
			want:    []Component{},
			wantErr: true,
		},
		{
			name: "Unable to fetch components from API",
			prepare: func(h *mocks.MockStatusioHandler) {
				h.EXPECT().ComponentList(gomock.Any()).Return(gostatusio.ComponentListResponse{}, fmt.Errorf("unable to fetch components from API")).Times(1)
			},
			args: args{
				description: "more than 10",
				matcher: func(name string) bool {
					return true
				},
			},
			want:    nil,
			wantErr: true,
		},
	}
	for _, tt := range tests {
		t.Run(tt.name, func(t *testing.T) {
			ctrl := gomock.NewController(t)
			handler := mocks.NewMockStatusioHandler(ctrl)

			if tt.prepare != nil {
				tt.prepare(handler)
			}

			c := NewClient(handler, "page123", "")

			got, err := c.GetInfrastructureListFilterByFunction(tt.args.description, tt.args.matcher)
			if (err != nil) != tt.wantErr {
				t.Errorf("Client.GetInfrastructureListFilterByFunction() error = %v, wantErr %v", err, tt.wantErr)
				return
			}
			if !reflect.DeepEqual(got, tt.want) {
				t.Errorf("Client.GetInfrastructureListFilterByFunction() = %v, want %v", got, tt.want)
			}
		})
	}
}

func TestClient_getComponentsAndContainers(t *testing.T) {
	type args struct {
		components []string
	}
	tests := []struct {
		name    string
		prepare func(h *mocks.MockStatusioHandler)
		args    args
		want    []string
		wantErr bool
	}{
		{
			name: "Unable to fetch components list",
			prepare: func(h *mocks.MockStatusioHandler) {
				h.EXPECT().ComponentList(gomock.Any()).Return(gostatusio.ComponentListResponse{}, fmt.Errorf("unable to fetch components from API")).Times(1)
			},
			args:    args{},
			want:    nil,
			wantErr: true,
		},
		{
			name: "Successfully create array of components to containers",
			prepare: func(h *mocks.MockStatusioHandler) {
				clr, err := componentsJsonToStruct(`{"result": [ {"_id": "C1", "name": "API", "containers": [{"_id": "CT1", "name": "GCP"}, {"_id": "CT2", "name": "AWS"}]} ]}`)
				if err != nil {
					t.Error(err)
				}
				h.EXPECT().ComponentList(gomock.Any()).Return(clr, nil).Times(1)
			},
			args: args{
				components: []string{"C1", "C2"},
			},
			want: []string{
				"C1-CT1",
				"C1-CT2",
			},
			wantErr: false,
		},
	}
	for _, tt := range tests {
		t.Run(tt.name, func(t *testing.T) {
			ctrl := gomock.NewController(t)
			handler := mocks.NewMockStatusioHandler(ctrl)

			if tt.prepare != nil {
				tt.prepare(handler)
			}

			c := NewClient(handler, "page123", "")

			got, err := c.getComponentsAndContainers(tt.args.components)
			if (err != nil) != tt.wantErr {
				t.Errorf("Client.getComponentsAndContainers() error = %v, wantErr %v", err, tt.wantErr)
				return
			}
			if !reflect.DeepEqual(got, tt.want) {
				t.Errorf("Client.getComponentsAndContainers() = %v, want %v", got, tt.want)
			}
		})
	}
}

func TestClient_CreateIncident(t *testing.T) {
	type args struct {
		incident           gostatusio.Incident
		components         []string
		incidentStatusName string
	}
	tests := []struct {
		name    string
		prepare func(h *mocks.MockStatusioHandler)
		args    args
		want    gostatusio.IncidentCreateResponse
		wantErr bool
	}{
		{
			name: "Successfully create an incident",
			prepare: func(h *mocks.MockStatusioHandler) {
				clr, err := componentsJsonToStruct(`{"result": [ {"_id": "C1", "name": "API", "containers": [{"_id": "CT1", "name": "GCP"}, {"_id": "CT2", "name": "AWS"}]} ]}`)
				if err != nil {
					t.Error(err)
				}

				icr, err := incidentResponseJsonToStruct(`{"result": "abc123"}`)
				if err != nil {
					t.Error(err)
				}

				gomock.InOrder(
					h.EXPECT().ComponentList(gomock.Any()).Return(clr, nil).Times(1),
					h.EXPECT().IncidentCreate(gomock.Any()).Return(icr, nil).Times(1),
				)
			},
			args: args{
				incident: gostatusio.Incident{
					IncidentName:    "name",
					IncidentDetails: "details",
					MessageSubject:  "message subject",
				},
				components: []string{
					"C1",
				},
				incidentStatusName: "300",
			},
			want: gostatusio.IncidentCreateResponse{
				Result: "abc123",
			},
			wantErr: false,
		},
		{
			name: "Invalid incident status",
			prepare: func(h *mocks.MockStatusioHandler) {
				clr, err := componentsJsonToStruct(`{"result": [ {"_id": "C1", "name": "API"} ]}`)
				if err != nil {
					t.Error(err)
				}

				h.EXPECT().ComponentList(gomock.Any()).Return(clr, nil).Times(1)
				h.EXPECT().IncidentCreate(gomock.Any()).Return(gostatusio.IncidentCreateResponse{}, nil).Times(0)
			},
			args: args{
				incident: gostatusio.Incident{
					IncidentName:    "name",
					IncidentDetails: "details",
					MessageSubject:  "message subject",
				},
				components: []string{
					"C1",
				},
				incidentStatusName: "abc123",
			},
			want:    gostatusio.IncidentCreateResponse{},
			wantErr: true,
		},
	}
	for _, tt := range tests {
		t.Run(tt.name, func(t *testing.T) {
			ctrl := gomock.NewController(t)
			handler := mocks.NewMockStatusioHandler(ctrl)

			if tt.prepare != nil {
				tt.prepare(handler)
			}

			c := NewClient(handler, "page123", "")

			got, err := c.CreateIncident(tt.args.incident, tt.args.components, tt.args.incidentStatusName)
			if (err != nil) != tt.wantErr {
				t.Errorf("Client.CreateIncident() error = %v, wantErr %v", err, tt.wantErr)
				return
			}
			if !reflect.DeepEqual(got, tt.want) {
				t.Errorf("Client.CreateIncident() = %v, want %v", got, tt.want)
			}
		})
	}
}

func TestClient_GetIncidentsBaseURL(t *testing.T) {
	type fields struct {
		baseURL string
		pageID  string
	}
	tests := []struct {
		name   string
		fields fields
		want   string
	}{
		{
			name: "Ensure internal_view URL is returned when private mode is enabled",
			fields: fields{
				pageID: "abc123",
			},
			want: "https://app.status.io/pages/incident/abc123",
		},
		{
			name: "Ensure correct URL is returned when private mode is disabled",
			fields: fields{
				pageID:  "abc123",
				baseURL: "https://status.foo.com",
			},
			want: "https://status.foo.com/pages/incident/abc123",
		},
	}
	for _, tt := range tests {
		t.Run(tt.name, func(t *testing.T) {
			ctrl := gomock.NewController(t)
			handler := mocks.NewMockStatusioHandler(ctrl)

			c := NewClient(handler, tt.fields.pageID, tt.fields.baseURL)

			if got := c.GetIncidentsBaseURL(); got != tt.want {
				t.Errorf("Client.GetIncidentsBaseURL() = %v, want %v", got, tt.want)
			}
		})
	}
}
