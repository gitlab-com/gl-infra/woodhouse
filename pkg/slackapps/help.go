package slackapps

import (
	"encoding/json"
	"fmt"
	"net/http"
	"strings"

	"github.com/slack-go/slack"
	"gitlab.com/gitlab-com/gl-infra/woodhouse/pkg/instrumentation"
)

type HelpSlashCommand struct {
	GlobalCommandName string
	Subcommands       []string
}

func (s *HelpSlashCommand) Handle(
	event *instrumentation.Event, commandName string, command slack.SlashCommand,
	w http.ResponseWriter, req *http.Request,
) {
	if err := json.NewEncoder(w).Encode(slack.Msg{
		ResponseType: slack.ResponseTypeEphemeral,
		Text:         fmt.Sprintf("Usage: /%s <%s>", s.GlobalCommandName, strings.Join(s.Subcommands, " | ")),
	}); err != nil {
		event.With("error", err, "message", "error writing HTTP response")
	}
}
