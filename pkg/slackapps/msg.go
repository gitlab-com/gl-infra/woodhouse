package slackapps

import "github.com/slack-go/slack"

func SendMsg(client *slack.Client, channelID, msg string) error {
	_, _, _, err := client.SendMessage(
		channelID,
		slack.MsgOptionText(msg, false),
	)
	return err
}
