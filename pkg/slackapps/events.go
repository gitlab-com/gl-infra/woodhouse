package slackapps

import (
	"encoding/json"
	"fmt"
	"github.com/slack-go/slack"
	"github.com/slack-go/slack/slackevents"
	"github.com/zekroTJA/timedmap/v2"
	"io"
	"net/http"
	"regexp"
	"strconv"
	"strings"
	"time"
)

type EventsHandler struct {
	SlackClient             SlackClientInterface
	reactionHandlers        map[string]ReactionHandler
	slackUserIDsToRealNames *timedmap.TimedMap[string, string]
}

type ReactionHandler interface {
	Handle(msg, channelName, user string, ts float64)
}

type SlackClientInterface interface {
	GetConversationInfo(params *slack.GetConversationInfoInput) (*slack.Channel, error)
	GetUserInfo(user string) (*slack.User, error)
	GetConversationHistory(params *slack.GetConversationHistoryParameters) (*slack.GetConversationHistoryResponse, error)
	GetConversationReplies(params *slack.GetConversationRepliesParameters) ([]slack.Message, bool, string, error)
}

func NewEventsHandler(slackClient SlackClientInterface) *EventsHandler {
	const scanTime = 10 * time.Second

	return &EventsHandler{
		SlackClient:             slackClient,
		reactionHandlers:        make(map[string]ReactionHandler),
		slackUserIDsToRealNames: timedmap.New[string, string](scanTime),
	}
}

func (e *EventsHandler) AddReactionHandler(reaction string, handler ReactionHandler) {
	e.reactionHandlers[reaction] = handler
}

func (e *EventsHandler) ServeHTTP(w http.ResponseWriter, req *http.Request) {
	body, err := io.ReadAll(req.Body)
	if err != nil {
		http.Error(w, "Failed to read request body", http.StatusInternalServerError)
		return
	}
	eventsAPIEvent, err := slackevents.ParseEvent(json.RawMessage(body), slackevents.OptionNoVerifyToken())
	if err != nil {
		http.Error(w, "Failed to parse event", http.StatusBadRequest)
		return
	}

	switch eventsAPIEvent.Type {
	case slackevents.URLVerification:
		var r *slackevents.ChallengeResponse
		err := json.Unmarshal(body, &r)
		if err != nil {
			http.Error(w, "Failed to parse challenge", http.StatusInternalServerError)
			return
		}
		w.Header().Set("Content-Type", "text")
		_, _ = w.Write([]byte(r.Challenge))
	case slackevents.CallbackEvent:
		innerEvent := eventsAPIEvent.InnerEvent
		if ev, ok := innerEvent.Data.(*slackevents.ReactionAddedEvent); ok {
			handler, exists := e.reactionHandlers[ev.Reaction]
			if exists {
				channelInfo, err := e.SlackClient.GetConversationInfo(&slack.GetConversationInfoInput{
					ChannelID:         ev.Item.Channel,
					IncludeLocale:     false,
					IncludeNumMembers: false})
				if err != nil {
					http.Error(w, "Failed to get channel info", http.StatusInternalServerError)
					return
				}

				msg, u, err := e.fetchMessage(ev.Item.Channel, ev.Item.Timestamp)
				if err != nil {
					http.Error(w, "Failed to fetch message", http.StatusInternalServerError)
					return
				}

				ts, err := strconv.ParseFloat(ev.Item.Timestamp, 64)
				if err != nil {
					http.Error(w, "Error parsing event timestamp", http.StatusInternalServerError)
					return
				}

				go handler.Handle(
					e.msgToGitLabMarkdown(msg),
					channelInfo.Name,
					e.realNameForSlackUsername(u),
					ts,
				)
			}
		}
	}
}

func (e *EventsHandler) msgToGitLabMarkdown(msg string) string {
	var msgGitLabMarkdown string

	msgGitLabMarkdown = convertSlackLinksToMarkdown(msg)
	// escape all words beginning with a `/`
	msgGitLabMarkdown = escapeQuickAction(msgGitLabMarkdown)
	// replace newlines with "<br>"
	msgGitLabMarkdown = strings.ReplaceAll(msgGitLabMarkdown, "\n", "<br>")
	// replace slack usernames with real names
	return e.convertSlackUsersToRealNames(msgGitLabMarkdown)
}

func (e *EventsHandler) fetchMessage(channelID, timestamp string) (string, string, error) {
	params := &slack.GetConversationHistoryParameters{
		ChannelID: channelID,
		Inclusive: true,
		Latest:    timestamp,
		Limit:     1,
	}

	history, err := e.SlackClient.GetConversationHistory(params)
	if err != nil {
		return "", "", err
	}

	// If the message is not found in the main history (i.e., it could be a thread), check for replies
	if len(history.Messages) > 0 && history.Messages[0].Timestamp == timestamp {
		return history.Messages[0].Text, history.Messages[0].User, nil
	}

	// If the message was not found as a standalone, it might be part of a thread.
	replies, _, _, err := e.SlackClient.GetConversationReplies(&slack.GetConversationRepliesParameters{
		ChannelID: channelID,
		Timestamp: timestamp,
		Limit:     1,
	})
	if err != nil {
		return "", "", err
	}

	// Return the first reply that matches the timestamp
	if len(replies) > 0 && replies[0].Timestamp == timestamp {
		return replies[0].Text, replies[0].User, nil
	}

	return "", "", fmt.Errorf("no message found at timestamp %s", timestamp)
}

func (e *EventsHandler) realNameForSlackUsername(u string) string {
	fmt.Println("looking up", u)
	if val, exists := e.slackUserIDsToRealNames.GetValue(u); exists {
		return val
	}

	user, err := e.SlackClient.GetUserInfo(u)
	if err != nil {
		return "<i>user lookup failure</i>"
	}

	realName := user.RealName
	e.slackUserIDsToRealNames.Set(u, realName, 2*24*time.Hour)
	return realName
}

func (e *EventsHandler) convertSlackUsersToRealNames(msg string) string {
	fmt.Println("converting slack IDs for msg", msg)

	re := regexp.MustCompile(`<@(U[A-Z0-9]{8,10})>`)
	result := re.ReplaceAllStringFunc(msg, func(m string) string {
		matches := re.FindStringSubmatch(m)
		return fmt.Sprintf("<b><i>%s</i></b>", e.realNameForSlackUsername(matches[1]))
	})

	return result
}

func convertSlackLinksToMarkdown(msg string) string {
	re := regexp.MustCompile(`<([^|>@]+)\|?([^>@]*)>`)

	result := re.ReplaceAllStringFunc(msg, func(m string) string {
		matches := re.FindStringSubmatch(m)
		url := matches[1]
		text := matches[2]
		if text == "" {
			return fmt.Sprintf("<a href=\"%s\">%s</a>", url, url)
		}
		return fmt.Sprintf("[%s](%s)", text, url)
	})

	return result
}

func escapeQuickAction(msg string) string {
	// escapes "/" when it starts a new line
	re := regexp.MustCompile(`(?m)^/(\w+)`)
	result := re.ReplaceAllStringFunc(msg, func(m string) string {
		matches := re.FindStringSubmatch(m)
		action := matches[1]
		return fmt.Sprintf("<pre>/%s</pre>", action)
	})

	return result
}
