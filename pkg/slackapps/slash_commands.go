package slackapps

import (
	"fmt"
	"net/http"
	"regexp"
	"strings"

	"github.com/slack-go/slack"
	"gitlab.com/gitlab-com/gl-infra/woodhouse/pkg/instrumentation"
)

var whitespace = regexp.MustCompile(`\s+`)

type SlashCommandHandler struct {
	monoCommandNamespace string
	helpHandler          *HelpSlashCommand
	commands             map[string]SlashCommand
}

func NewSlashCommandHandler(monoCommandNamespace string) *SlashCommandHandler {
	handler := &SlashCommandHandler{
		monoCommandNamespace: monoCommandNamespace, commands: map[string]SlashCommand{},
		helpHandler: &HelpSlashCommand{GlobalCommandName: monoCommandNamespace},
	}

	// Always be helpful!
	handler.HandleSlashCommand("help", handler.helpHandler)
	return handler
}

type SlashCommand interface {
	Handle(
		event *instrumentation.Event, commandName string, command slack.SlashCommand,
		w http.ResponseWriter, req *http.Request,
	)
}

func (s *SlashCommandHandler) HandleSlashCommand(command string, handler SlashCommand) {
	s.commands[command] = handler
	s.helpHandler.Subcommands = append(s.helpHandler.Subcommands, command)
}

func (s *SlashCommandHandler) ServeHTTP(w http.ResponseWriter, req *http.Request) {
	event := req.Context().Value(instrumentation.EventContextKey{}).(*instrumentation.Event)

	slash, err := slack.SlashCommandParse(req)
	if err != nil {
		event.With("error", err, "message", "error parsing slack command")
		w.WriteHeader(http.StatusUnprocessableEntity)
		return
	}

	commandName := slash.Command
	command := strings.TrimPrefix(slash.Command, "/")
	slashCommandArgs := whitespace.Split(slash.Text, -1)

	if command == s.monoCommandNamespace {
		command = slashCommandArgs[0]
		commandName = fmt.Sprintf("%s %s", commandName, command)
		slash.Command = command
		slash.Text = strings.Join(slashCommandArgs[1:], " ")
	}

	handler := s.commands[command]
	if handler == nil {
		// This should never be nil because the "help" subcommand is always
		// registered.
		handler = s.commands["help"]
	}

	event.With("slash_command", command)

	// If we can parse the command at all, send an HTTP 200, even if we will
	// indicate that an error has occurred:
	// https://api.slack.com/interactivity/slash-commands#responding_with_errors
	w.Header().Set("Content-type", "application/json")
	w.WriteHeader(http.StatusOK)

	handler.Handle(event, commandName, slash, w, req)
}
