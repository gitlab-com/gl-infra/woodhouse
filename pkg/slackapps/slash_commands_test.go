package slackapps_test

import (
	"fmt"
	"io"
	"net/http"
	"net/http/httptest"
	"strings"
	"testing"

	"github.com/slack-go/slack"
	"github.com/stretchr/testify/assert"
	"github.com/stretchr/testify/require"
	"gitlab.com/gitlab-com/gl-infra/woodhouse/pkg/instrumentation"
	"gitlab.com/gitlab-com/gl-infra/woodhouse/pkg/slackapps"
)

func TestSlashCommandHandler(t *testing.T) {
	for _, tc := range []struct {
		name                 string
		command              string
		text                 string
		expectedResponse     string
		expectedHandlerCalls int
	}{
		{
			name:                 "calls registered command handler",
			command:              "test",
			text:                 "this%20is%20a%20test",
			expectedResponse:     "this is a test",
			expectedHandlerCalls: 1,
		},
		{
			name:                 "returns error text when slash command not found",
			command:              "idontexist",
			text:                 "this%20is%20a%20test",
			expectedHandlerCalls: 0,
		},
		{
			name:                 "calls handler for first word of space-separated slash command text when unified command is invoked",
			command:              "unified",
			text:                 "test%20this%20is%20a%20test",
			expectedResponse:     "this is a test",
			expectedHandlerCalls: 1,
		},
		{
			name:                 "calls handler for first word of any-whitespace-separated slash command text when unified command is invoked",
			command:              "unified",
			text:                 "test%20%20%20%0Athis%20is%20a%20test",
			expectedResponse:     "this is a test",
			expectedHandlerCalls: 1,
		},
		{
			name:                 "returns error text when slash command not found when unified command is invoked",
			command:              "unified",
			text:                 "wrong%20this%20is%20a%20test",
			expectedHandlerCalls: 0,
		},
	} {
		t.Run(tc.name, func(t *testing.T) {
			handler := slackapps.NewSlashCommandHandler("unified")

			handlerCallCount := 0
			handlerFunc := func(
				event *instrumentation.Event, commandName string, command slack.SlashCommand,
				w http.ResponseWriter, req *http.Request,
			) {
				expectedCommandName := command.Command
				if tc.command == "unified" {
					expectedCommandName = fmt.Sprintf("/%s %s", "unified", command.Command)
				}

				assert.Equal(t, expectedCommandName, commandName)
				handlerCallCount++
				fmt.Fprint(w, command.Text)
			}

			handler.HandleSlashCommand("test", &fakeSlashCommandHandler{handler: handlerFunc})

			server := httptest.NewServer(instrumentation.NewDummyInstrumentationMiddleware(handler))
			defer server.Close()

			slashCommandBody := strings.ReplaceAll(fmt.Sprintf(slashCommandBodyTemplate, tc.command, tc.text), "\n", "")
			req, err := http.NewRequest(http.MethodPost, server.URL, strings.NewReader(slashCommandBody))
			require.Nil(t, err)
			req.Header.Set("Content-type", "application/x-www-form-urlencoded")
			resp, err := server.Client().Do(req)
			require.Nil(t, err)
			defer resp.Body.Close()
			require.Equal(t, http.StatusOK, resp.StatusCode)
			respBody, err := io.ReadAll(resp.Body)
			require.Nil(t, err)

			if tc.expectedResponse != "" {
				assert.Equal(t, tc.expectedResponse, strings.TrimSpace(string(respBody)))
			}
			assert.Equal(t, tc.expectedHandlerCalls, handlerCallCount)
		})
	}
}

const (
	slashCommandBodyTemplate = `token=gIkuvaNzQIHg97ATvDxqgjtO
&team_id=T0001
&team_domain=example
&enterprise_id=E0001
&enterprise_name=Globular%%20Construct%%20Inc
&channel_id=C2147483705
&channel_name=test
&user_id=U2147483697
&user_name=Steve
&command=/%s
&text=%s
&response_url=https://hooks.slack.com/commands/1234/5678
&trigger_id=13345224609.738474920.8088930838d88f008e0
&api_app_id=A123456
`
)

type fakeSlashCommandHandler struct {
	handler func(
		event *instrumentation.Event, commandName string, command slack.SlashCommand,
		w http.ResponseWriter, req *http.Request,
	)
}

func (s *fakeSlashCommandHandler) Handle(
	event *instrumentation.Event, commandName string, command slack.SlashCommand,
	w http.ResponseWriter, req *http.Request,
) {
	s.handler(event, commandName, command, w, req)
}
