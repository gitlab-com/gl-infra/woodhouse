package slackapps

import (
	"testing"

	"github.com/slack-go/slack"
	"github.com/stretchr/testify/assert"
	"github.com/stretchr/testify/mock"
	"github.com/stretchr/testify/require"
)

type MockSlackClient struct {
	mock.Mock
}

func (m *MockSlackClient) GetConversationInfo(params *slack.GetConversationInfoInput) (*slack.Channel, error) {
	args := m.Called(params)
	return args.Get(0).(*slack.Channel), args.Error(1)
}

func (m *MockSlackClient) GetUserInfo(user string) (*slack.User, error) {
	args := m.Called(user)
	return args.Get(0).(*slack.User), args.Error(1)
}

func (m *MockSlackClient) GetConversationHistory(params *slack.GetConversationHistoryParameters) (*slack.GetConversationHistoryResponse, error) {
	args := m.Called(params)
	return args.Get(0).(*slack.GetConversationHistoryResponse), args.Error(1)
}

func (m *MockSlackClient) GetConversationReplies(params *slack.GetConversationRepliesParameters) ([]slack.Message, bool, string, error) {
	args := m.Called(params)
	return args.Get(0).([]slack.Message), args.Bool(1), args.String(2), args.Error(3)
}

func TestMsgToGitLabMarkdown(t *testing.T) {
	tests := []struct {
		name       string
		slackMsg   string
		glMarkdown string
	}{
		{
			name:       "with line breaks",
			slackMsg:   "text\nwith\n\nline\nbreaks",
			glMarkdown: "text<br>with<br><br>line<br>breaks",
		},
		{
			name:       "with bare link",
			slackMsg:   "text\nwith <https://example.com>\n<https://example.com>\nbare link",
			glMarkdown: `text<br>with <a href="https://example.com">https://example.com</a><br><a href="https://example.com">https://example.com</a><br>bare link`,
		},
		{
			name:       "with formatted link",
			slackMsg:   "text\nwith <https://example.com|example>\n<https://example.com|example>\nformatted link",
			glMarkdown: `text<br>with [example](https://example.com)<br>[example](https://example.com)<br>formatted link`,
		},
		{
			name:       "with quick action",
			slackMsg:   "text\nwith /notaction\n/close herpderp\n\n/foo",
			glMarkdown: `text<br>with /notaction<br><pre>/close</pre> herpderp<br><br><pre>/foo</pre>`,
		},
	}
	for _, tt := range tests {
		t.Run(tt.name, func(t *testing.T) {
			mockSlackClient := new(MockSlackClient)
			handler := NewEventsHandler(mockSlackClient)

			glMarkdown := handler.msgToGitLabMarkdown(tt.slackMsg)
			assert.Equal(t, tt.glMarkdown, glMarkdown, "expected output should match")
		})
	}
}

func TestEventsHandler_fetchMessage(t *testing.T) {
	mockSlackClient := new(MockSlackClient)
	handler := NewEventsHandler(mockSlackClient)

	channelID := "C123456"
	timestamp := "1234567890.123456"

	mockHistoryResponse := &slack.GetConversationHistoryResponse{
		Messages: []slack.Message{
			{Msg: slack.Msg{Timestamp: timestamp, Text: "incident slack message"}},
		},
	}
	mockSlackClient.On("GetConversationHistory", &slack.GetConversationHistoryParameters{
		ChannelID: channelID,
		Inclusive: true,
		Latest:    timestamp,
		Limit:     1,
	}).Return(mockHistoryResponse, nil)

	msg, _, err := handler.fetchMessage(channelID, timestamp)
	require.NoError(t, err)
	assert.Equal(t, "incident slack message", msg)
}
